import unittest
import math
import numpy as np
from SkinModel import NDGaussianPDF

class NDGaussianPDF_test(unittest.TestCase):
	def test1DDensity(self):
		mean = 2.0
		var  = 0.3
		pdf = NDGaussianPDF([mean], [[var]])
		density = lambda x: math.exp(-(x - mean)**2 / (2 * var)) / (math.sqrt(2 * math.pi * var))

		self.assertAlmostEqual(pdf.density([1.0]), density(1.0))
		self.assertAlmostEqual(pdf.density([2.0]), density(2.0))
		self.assertAlmostEqual(pdf.density([0.5]), density(0.5))

	def test2DOrthogonalDensity(self):
		mean = (mx , my ) = [2.0, 0.4]
		var  = (sx2, sy2) = [0.3, 1.0]
		pdf = NDGaussianPDF(mean, var)

		denom   = 2 * math.pi * math.sqrt(sx2 * sy2)
		score   = lambda  x, y: -((x - mx)**2 / sx2 + (y - my)**2 / sy2) / 2
		density = lambda (x, y): math.exp(score(x,y)) / denom

		points    = [[1.0, 0.3], [2.0, 0.5], [0.4, -2.0]]
		densities = pdf.density(points)
		for i, p in enumerate(points):
			self.assertAlmostEqual(densities[i], density(p))

	def test2DEntangledDensity(self):
		mean = np.array([2.0, 0.4])[:,np.newaxis]
		cov  = np.matrix([[1.0, -0.5], [-0.5, 3.0]])
		pdf  = NDGaussianPDF(mean.flatten(), cov)

		denom   = 2 * math.pi * math.sqrt(np.linalg.det(cov))
		score   = lambda x: - (x - mean).T * cov.I * (x - mean) / 2
		density = lambda x: math.exp(score(x)) / denom

		points    = np.array([[1.0, 0.3], [2.0, 0.5], [0.4, -2.0]])
		densities = pdf.density(points)
		for i, p in enumerate(points):
			self.assertAlmostEqual(densities[i], density(p[:,np.newaxis]))

if __name__ == '__main__':
	unittest.main()
