#!/usr/bin/python
import sys
import cv2 as cv
import freenect as fk
import numpy as np
from SkinDetector import SkinDetector
from term import *

detector = None

welcomeMsg = """Bayesian Color-based Skin Detector Preview for Kinect (November, 2014), by 
	Pedro Asad -- pasad@cos.ufrj.br -- www.lcg.ufrj.br/Members/pasad"""

def atStart(dbfile, skinfile, colorfile):
	global detector
	detector = SkinDetector(skinfile, colorfile, maxThr=0.3)
	print "Skin detector initialized"

def onFrame(video_frame, depth_frame):
	video_frame_yuv = cv.cvtColor(video_frame, cv.COLOR_RGB2YUV)
	video_frame     = cv.cvtColor(video_frame, cv.COLOR_RGB2BGR)

	#video_mask = detector.skinMask(video_frame_yuv)
	high_mask, low_mask = detector.skinMask(video_frame_yuv)
	#video_frame[video_mask,1] = 255
	video_frame[low_mask , 0] = 255
	video_frame[high_mask, 1] = 255

	cv.imshow('Video', video_frame)
	cv.imshow('Depth', ~(depth_frame * 64))

	key = chr(cv.waitKey(int(100 / 3.0)) & 255)

	if key == 'q':
		return False

CommandLineKinectApplication(welcomeMsg, atStart, onFrame).run()
