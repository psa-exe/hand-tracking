from SkinModel import *
import cv2
import numpy

class SkinDetector:
	def __init__(self, skinFile='', colorFile='', minThr=0.15, maxThr=0.4, offlineModel='', window=5, gamma=0.8):
		self._skinModel = GaussianSkinModel()
		self._minThr = minThr
		self._maxThr = maxThr
		self._window = window
		self._gamma  = gamma

	def skinProbMap(self, image):
		"""Returns a map of pixel-wise skin likelihood."""
		return self._skinModel.posterior(image[:,:,[2,1]])

	def skinMask(self, image):
		#image = cv2.GaussianBlur(image, (7, 7), 0)

		probmap = self.skinProbMap(image)
		mask_high = probmap > self._maxThr
		mask_low  = numpy.logical_and(self._minThr < probmap, probmap <= self._maxThr)

		return (mask_high, mask_low)

		#h, w = probmap.shape
		#changed = True
		#while changed:
		#	changed = False
		#	for i in range(h):
		#		for j in range(w):
		#			if not mask[i, j] and probmap[i, j] > self._minThr and np.any(mask[i - 1 : i + 2, j - 1 : j + 2]):
		#				mask[i, j] = True
		#				changed = True

		#return cv2.morphologyEx(mask, cv2.MORPH_ERODE, (7, 7)) == 255
