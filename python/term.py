import sys
import freenect

def writeLine(line):
	sys.stdout.write(''.join(['\r'] + [' '] * 80 + ['\r']))
	sys.stdout.write(line)
	sys.stdout.flush()

def doNothing(*args):
	pass

class CommandLineApplication:
	def __init__(self, welcomeMsg='', atStart=doNothing, onFrame=doNothing, atEnd=doNothing):
		self.welcomeMsg = welcomeMsg
		self.atStart    = atStart
		self.onFrame    = onFrame
		self.atEnd      = atEnd

	def run(self):
		print self.welcomeMsg

		if len(sys.argv) < 4:
			print 'Usage: ' + sys.argv[0] + ' <database-file> <skin-file> <non-skin-file>'
			return

		(dbfile, skinfile, colorfile) = sys.argv[1:4]

		self.atStart(dbfile, skinfile, colorfile)
		lastframe = self.core()
		self.atEnd(lastframe)

		writeLine('Bye...\n')

class CommandLineDatasetApplication(CommandLineApplication):
	def __init__(self, welcomeMsg='', atStart=doNothing, onFrame=doNothing, atEnd=doNothing):
		CommandLineApplication.__init__(self, welcomeMsg, atStart, onFrame, atEnd)

	def core(self):
		frame  = 0
		dbfile = open(sys.argv[1])
		lines  = dbfile.readlines()
		while 0 <= frame < len(lines):
			writeLine('Frame: %5d / %5d' % (frame + 1, len(lines)))
			(file_orig, file_skin) = lines[frame].rstrip().split('\t')

			nextframe = self.onFrame(frame, file_orig, file_skin)

			if nextframe is None:
				frame += 1
			elif 0 <= nextframe < len(lines):
				frame = nextframe
			else:
				writeLine('Jump request out of range or last frame reached\n')
				break
		dbfile.close()
		return len(lines)

class CommandLineKinectApplication(CommandLineApplication):
	def __init__(self, welcomeMsg='', atStart=doNothing, onFrame=doNothing, atEnd=doNothing):
		CommandLineApplication.__init__(self, welcomeMsg, atStart, onFrame, atEnd)

	def core(self):
		keep   = True
		frames = 0
		while keep:
			(video_frame, _) = freenect.sync_get_video()
			(depth_frame, _) = freenect.sync_get_depth()

			keep = self.onFrame(video_frame, depth_frame)
			if keep is None:
				keep = True

			frames += 1

		freenect.sync_stop()
		return frames
