#!/usr/bin/python
from SkinModel import SkinModel
import numpy
import cv2
import sys
from term import *

skindump  = None
colordump = None

model = SkinModel()

welcomeMsg = """
Bayesian Skin Model Trainer (October, 2014)
	by Pedro Asad (pasad@cos.ufrj.br)
	www.lcg.ufrj.br/Members/pasad"""

def dumpModel(model, skinfile, colorfile):
	writeLine("Printing to files '" + skinfile + "', '" + colorfile + "'")

	model.dumpSkin	(skinfile )
	model.dumpColors(colorfile)

def atStart(dbfile, skinfile, colorfile):
	global skindump
	global colordump

	skindump  = skinfile
	colordump = colorfile

def onFrame(frame, path_orig, path_skin):
	frame_orig = cv2.imread(path_orig) 
	frame_skin = cv2.imread(path_skin, 0)

	yuv_orig = cv2.GaussianBlur(frame_orig, (3,3), 0)
	yuv_orig = cv2.cvtColor(yuv_orig, cv2.COLOR_BGR2YUV)

	(height, width, _) = frame_orig.shape

	skin = (frame_skin == 255)
	model.addSkin   (yuv_orig[ skin,1:3])
	model.addNonSkin(yuv_orig[~skin,1:3])

def atEnd(*args):
	global model
	dumpModel(model, skindump, colordump)

CommandLineDatasetApplication(welcomeMsg, atStart, onFrame, atEnd).run()

#class SkinModelTrainer:
#	 """
#	 TODO:
#	 def __init__(self, skinModel):
#	 or, perhaps
#	 def __init__(self, minthr=0.15, maxthr=0.5):
#	 def useDataset(self, dataset):
#	 def addImage(self, image, skinMask):
#	 def getModel(self):
#
#	 Check this out:
#	 http://en.wikipedia.org/wiki/Naive_Bayes_classifier
#	 """
#	 pass
