import numpy
import math

class SkinModel:
	"""Bayesian skin color model for skin segmentation.

	Partial implementation of the Bayesian color-based skin segmentation
	framework proposed in 

		Argyros, Antonis A., and Manolis IA Lourakis. "Real-time tracking of
		multiple skin-colored objects with a possibly moving camera." Computer
		Vision-ECCV 2004. Springer Berlin Heidelberg, 2004. 368-379.

	Only adding skin/non-skin samples into the model and checking for
	probabilities is supported. Refactoring is needed in order to implement
		* Seed blobs
		* Hysteresis thresholding
		* Region growing for seed blobs
		* Offline and online models
		* Offline training

	TODO: This model can be considered an specialization of a more general one.
	Given:
		dimensionality 'd' and value range for each dimension
		set of classes 'C'
		build the histogram of every class in 'C' and calculate conditional and
		absolute probabilities from observations
	"""

	def __init__(self, sfile='', cfile=''):
		if sfile:
			self._skinData = numpy.loadtxt(sfile)
			self._skinSamples = numpy.sum(self._skinData)
		else:
			self._skinData = numpy.zeros((256, 256))
			self._skinSamples = 0

		if cfile:
			self._colorData = numpy.loadtxt(cfile)
			self._colorSamples = numpy.sum(self._colorData)
		else:
			self._colorData = numpy.zeros((256, 256))
			self._colorSamples = 0

	def prior(self):
		"""Prior probability of getting a skin pixel -- P(S)."""
		if self._skinSamples:
			return float(self._skinSamples) / self._colorSamples
		else:
			return 0.0

	def likelihood(self, uvcolors):
		"""Likelihood of given UV colors among skin colors -- P(C|S)."""
		uvcolors = numpy.array(uvcolors)
		shape    = uvcolors.shape
		if shape[-1] == 2:
			if self._skinSamples:
				return self._skinData[uvcolors[...,0], uvcolors[...,1]] / float(self._skinSamples)
			elif shape == (2,):
				return 0.0
			else:
				return numpy.zeros(shape)
		else:
			raise ValueError(uvcolors)

	def evidence(self, uvcolors):
		"""Probability of evidence UV colors -- P(C)."""
		uvcolors = numpy.array(uvcolors)
		shape    = uvcolors.shape
		if shape[-1] == 2:
			if self._colorSamples:
				return self._colorData[uvcolors[...,0], uvcolors[...,1]] / float(self._colorSamples)
			elif shape == (2,):
				return 0.0
			else:
				return numpy.zeros(shape)
		else:
			raise ValueError(uvcolors)

	def posterior(self, uvcolors):
		"""Posterior probability that observed UV colors are skin colors (You probably want this method, not the others) -- P(S|C)."""
		uvcolors = numpy.array(uvcolors)
		shape    = uvcolors.shape
		if shape[-1] == 2:
			if self._colorSamples:
				skinColorProb = self.likelihood(uvcolors)
				colorProb     = self.evidence    (uvcolors)
				return self.prior() * numpy.divide(skinColorProb, colorProb)
			elif shape == (2,):
				return 0.0
			else:
				return numpy.zeros(shape)
		else:
			raise ValueError(uvcolors)

	def addSkin(self, uvcolors):
		"""Add UV skin color samples (1 or more) to the model."""
		uvcolors = numpy.array(uvcolors)
		shape    = uvcolors.shape
		size     = uvcolors.size
		if shape[-1] == 2:
			u = uvcolors[...,0].flatten()
			v = uvcolors[...,1].flatten()
			bins = numpy.linspace(0, 256, 257)

			hist = numpy.histogram2d(u, v, bins)[0].astype(numpy.int64)

			self._skinData  += hist
			self._colorData += hist

			self._skinSamples  += size
			self._colorSamples += size
		else:
			raise ValueError(uvcolors)

	def addNonSkin(self, uvcolors):
		"""Add a non-skin color sample to the model."""
		uvcolors = numpy.array(uvcolors)
		shape = uvcolors.shape
		size  = uvcolors.size
		if shape[-1] == 2:
			u = uvcolors[...,0].flatten()
			v = uvcolors[...,1].flatten()
			bins = numpy.linspace(0, 256, 257)

			hist = numpy.histogram2d(u, v, bins)[0].astype(numpy.int64)

			self._colorData    += hist
			self._colorSamples += size
		else:
			raise ValueError(uvcolors)

	def dumpSkin(self, fname):
		numpy.savetxt(fname, self._skinData)

	def dumpColors(self, fname):
		numpy.savetxt(fname, self._colorData)

class UniformNonSkinBayesianSkinModel(SkinModel):

	def __init__(self, sfile='', cfile=''):
		BayesianSkinModel.__init__(self, sfile, cfile)

	def evidence(self, uvcolors):
		"""Probability of evidence UV colors -- P(C)."""
		uvcolors = numpy.array(uvcolors)
		shape    = uvcolors.shape
		if shape[-1] == 2:
			if self._skinSamples:
				pS   = self.prior()
				pC_S = self._skinData[uvcolors[...,0], uvcolors[...,1]] / float(self._skinSamples)
				return pC_S * pS + 1.0 / 256**2 * (1 - pS)
			elif shape == (2,):
				return 1.0 / 256**2
			else:
				return numpy.ones(shape) / 256**2
		else:
			raise ValueError(uvcolors)

class NDGaussianPDF:
	def __init__(self, mean=[0.0], cov=[1.0]):
		# We could simply make 
		# self.ndim = numpy.array(mean).size
		# self.mean = numpy.array(mean).reshape(self.ndim, 1)
		# self.cov  = numpy.matrix(cov).reshape(self.ndim, self.ndim)
		# and forward any exceptions
		mean = numpy.array(mean)
		cov  = numpy.array(cov)

		self.ndim = len(mean)
		self.mean = mean

		if mean.ndim > 1:
			raise ValueError('Mean vector should be 1D')
		elif cov.shape == mean.shape:
			self.cov = numpy.diag(cov)
		elif cov.shape == mean[numpy.newaxis].shape:
			self.cov = numpy.diag(cov[0,:])
		elif cov.shape == mean.shape * 2:
			self.cov = cov
		else:
			raise ValueError('Mean vector %s incompatible with covariance matrix %s' % (mean, cov))

	def density(self, points):
		points = numpy.array(points)
		shape  = points.shape

		if shape[-1] != self.ndim:
			raise ValueError('Point array must be a %d-vector or a Nx%d array' % (self.ndim, self.ndim))

		flatshape = numpy.r_[numpy.prod(shape[:-1]), shape[-1]]

		points = points.reshape(flatshape) - self.mean
		scores = points
		scores = numpy.dot(scores, numpy.linalg.inv(self.cov))
		scores *= points
		scores = - numpy.sum(scores, axis=1)/2

		denom = math.sqrt((2 * math.pi)**self.ndim * numpy.linalg.det(self.cov))

		return numpy.exp(scores).reshape(shape[:-1]) / denom

class GaussianSkinModel:
	def __init__(self, 
			prior=0.1885,
			smean=[112.35,  149.29], 
			scov=[[108.86, - 67.26], [- 67.26,  87.83]], 
			cmean=[121.14,  135.69], 
			ccov=[[331.29, -169.96], [-169.96, 289.32]]):

		self._prior = prior
		self._skinPdf  = NDGaussianPDF(smean, scov)
		self._colorPdf = NDGaussianPDF(cmean, ccov)

	def prior(self):
		"""Prior probability of getting a skin pixel -- P(S)."""
		return self._prior

	def likelihood(self, uvcolors):
		"""Likelihood of given UV colors among skin colors -- P(C|S)."""
		return self._skinPdf.density(uvcolors)

	def evidence(self, uvcolors):
		"""Probability of evidence UV colors -- P(C)."""
		return self._colorPdf.density(uvcolors)

	def posterior(self, uvcolors):
		"""Posterior probability that observed UV colors are skin colors (You probably want this method, not the others) -- P(S|C)."""
		post = self.likelihood(uvcolors) * self.prior() / self.evidence(uvcolors)
		post[numpy.isnan(post)] = 0.0
		return post
