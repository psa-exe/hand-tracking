#!/usr/bin/python
import cv2
import sys
import time
import numpy
from SkinDetector import SkinDetector
from term import *

detector = None
wait = 0

welcomeMsg = """Bayesian Color-based Skin Detector Inspection Tool (October, 2014), by
	Pedro Asad -- pasad@cos.ufrj.br -- www.lcg.ufrj.br/Members/pasad

Commands:
	n: Next frame
	p: Previous frame
	q: Quit
	s: Switch between normal mode and slide show mode"""

def atStart(dbfile, skinfile, colorfile):
	global detector
	detector = SkinDetector(skinfile, colorfile)

def onFrame(frame, file_orig, file_skin):
	global wait

	bgr_orig = cv2.imread(file_orig)
	bgr_skin = cv2.imread(file_skin, 0)

	bgr_orig = cv2.GaussianBlur(bgr_orig, (3,3), 0)
	yuv_orig = cv2.cvtColor(bgr_orig, cv2.COLOR_BGR2YUV)

	mask_skin = detector.skinMask(yuv_orig)
	bgr_orig[mask_skin,0] = 255

	cv2.imshow('Original', bgr_orig)
	cv2.imshow('Skin'	 , bgr_skin)

	key = chr(cv2.waitKey(wait) & 255)
	if key == 'q':
		return -1
	elif key == 'n':
		return frame + 1
	elif key == 'p':
		return frame - 1
	elif key == 'j':
		frame = ""
		char  = ""
		while char != '\n':
			frame += char
			writeLine('Jump to frame: ' + frame)
			char = chr(cv2.waitKey() & 255)
		return eval(frame) - 1
	elif key == 's':
		if wait == 0:
			wait = 2000
		else:
			wait = 0

CommandLineDatasetApplication(welcomeMsg, atStart, onFrame).run()
