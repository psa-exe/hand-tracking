# Highest priorities

## Investigate performance holes

* Upload/download times
* Rendering times
* GPU/CPU operations

## Fixing of CCL and depth removal

* Apply dilating to skin mask
* Remove discarded blobs, necause they interfere in the energy function
* Fix CCL with sequential relabeling
* Remove irrelevant depth values
* Consider the placement of the hand model where the hand is located

## Constraints

* Check DIP FE angle calcuation

## Disturbance system

* Check xy disturbance
* Study Quaternion disturbance

## Limit tracking by score

* Tracking should only begin when the best score is at a minimum level
* Whenever tracking is restarted, disturbed poses should be calculated again
* Tracking should stop whenever the best score grows beyond a maximum level
* These levels should be determined empirically

## Initialization from a single pose

* Instead of initializing the swarm with a pose dir, initialize from a single one by disturbing

## Fix Kinect recorder

* Depth and video framerates do not match
* Or do they? It seems that the RGB camera in the sync mode starts with a certain delay
* But removing the first 10 depth frames is an acceptable workaround

# Important fixes and investigations

* I think energy calculation is being performed at several steps, instead of a single one

# Secondary architectural modifications

* PSO parameters are set in a single namespace, but we have different algorithms, so why not provide different namespaces with different default values?

# Porting the whole system to the GPU
