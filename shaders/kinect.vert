#version 430

layout(location=0) in vec4 in_Position;

out vec4 color;
out vec3 normal;
out vec4 vert;

uniform int yMultiplier;
uniform float kinectNear;
uniform float kinectFar;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform vec4 in_Color;

void main(void) {
	mat4 modelViewMatrix = viewMatrix * modelMatrix;
	mat4 normalMatrix = transpose(inverse(modelViewMatrix));

	normal = normalize(vec3(normalMatrix * vec4(in_Position.xyz,0.0)).xyz);
	vert = modelViewMatrix * in_Position;

	gl_Position = projectionMatrix * modelViewMatrix * in_Position;
	
	gl_Position.y *= yMultiplier;
	gl_Position.z = gl_Position.w * (2 * (gl_Position.w - kinectNear) / (kinectFar - kinectNear) - 1);

	color = in_Color;
}
