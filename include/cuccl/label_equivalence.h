#ifndef LCG_CUCCL_LABEL_EQUIVALENCE_H_
#define LCG_CUCCL_LABEL_EQUIVALENCE_H_

#define LCG_CUCCL_MAX_INNER_ITERATIONS 100
#define LCG_CUCCL_MAX_OUTER_ITERATIONS 100

#include <cuccl/cuccl.h>

namespace lcg {
namespace cuccl {

struct label_equivalence {

	typedef std::pair<
		cutils::device_ref<full_label_image>,
		cutils::device_ref<properties>
	> labels_and_props;

	const dim3 grid, block;
	cutils::pinned<bool> changed;
	cutils::device<full_label_image> labels;
	cutils::device<properties> props;

	static void init();

	label_equivalence(const dim3 &grid = meta_cuccl<>::grid, const dim3 &block = meta_cuccl<>::block);

	cutils::device_ref<full_label_image>
	cuccl(const cutils::device_ref<segment_image> &binary);

	labels_and_props
	cucclprops(const cutils::device_ref<segment_image> &binary);
};

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_LABEL_EQUIVALENCE_H_ */
