#ifndef LCG_CUCCL_ERROR_ELLIPSE_H_
#define LCG_CUCCL_ERROR_ELLIPSE_H_

namespace lcg {
namespace cuccl {

struct error_ellipse {
	float covmatrix[2][2];
	float  eigenval[2];
	float2 eigenvec[2];

	__host__ __device__
	error_ellipse(const ulonglong3 &variances, const ulonglong2 centroid, const float area);

	__host__ __device__
	float angle() const;

	__host__ __device__
	float area() const;
};

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_ERROR_ELLIPSE_H_ */
