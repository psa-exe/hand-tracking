#ifndef LCG_CUCCL_CUCCL_H_
#define LCG_CUCCL_CUCCL_H_

#include <limits>

#include <boost/type_traits.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/transition.hpp>
#include <boost/mpl/list.hpp>

#include <cuimg/image.h>

#include <cutils/fit_threads.h>
#include <cutils/types/cuda_vector.h>
#include <cutils/types/vector.h>
#include <cutils/wrappers.h>

#include <t3/bits.h>
#include <t3/make_int_with.h>

//TODO: These constants belong to the problem domain and should be removed from this file
#ifndef LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_WIDTH
#define LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_WIDTH 640
#endif

#ifndef LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_HEIGHT
#define LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_HEIGHT 480
#endif

namespace lcg {
namespace cuccl {

using cutils::fit_threads;

void init();

//TODO: Turn all image types into templates and use the meta_cuccl structure to obtain typedefs. This meta-structure should be highly type-parameterized and have templated-typedefs to add or modify features from the standard set of parameters
template <size_t W = LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_WIDTH, size_t H = LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_HEIGHT>
struct meta_cuccl : public fit_threads<W, H, 16, 16> {

	//TODO: binary_image is the special case of segments = 1
	static const int segments = 3;
	static const int segment_flag_bits = 0;
	static const int segment_bits = t3::bits<segments>::n;
	static const int segment_bits_total = segment_bits + segment_flag_bits;

	static const int label_flag_bits = 0;
	static const int label_bits = t3::bits<W * H - 1>::n;
	static const int label_bits_total = label_bits + label_flag_bits;

	static const int full_label_bits_total = label_bits_total + segment_bits_total;

	static const int centroidx_bits = t3::bits<W * (W - 1) / 2>::n;
	static const int centroidy_bits = t3::bits<H * (H - 1) / 2>::n;

	//TODO: Review these calculations
	static const int variancex_bits = t3::bits<(2 * W * W * W + 3 * W * W + W) / 6>::n;
	static const int variancey_bits = t3::bits<(2 * H * H * H + 3 * H * H + H) / 6>::n;

	static const int centroid_bits = (centroidx_bits > centroidy_bits) ? centroidx_bits : centroidy_bits;
	static const int variance_bits = (variancex_bits > variancey_bits) ? variancex_bits : variancey_bits;

	static const int covariance_bits = t3::bits<(W * (W - 1) / 2) * (H * (H - 1) / 2)>::n;

	typedef typename boost::make_unsigned<
		typename t3::make_int_with<1>::or_more_bits
	>::type binary_type;

	typedef typename boost::make_unsigned<
		typename t3::make_int_with<segment_bits_total>::or_more_bits
	>::type segment_type;

	typedef typename boost::make_unsigned<
		typename t3::make_int_with<label_bits_total>::or_more_bits
	>::type label_type;

	typedef typename boost::make_unsigned<
		typename t3::make_int_with<full_label_bits_total>::or_more_bits
	>::type full_label_type;

	typedef typename t3::make_int_with<label_bits>::or_more_bits area_type;

	//TODO: Nice typedefs that do not work with current type strict implementation.
//	typedef typename cuda_vector<
//		typename boost::make_unsigned<typename t3::make_int_with<centroid_bits>::type>::type,
//		2
//	>::type centroid_type;
//
//	typedef typename cuda_vector<
//		typename boost::make_unsigned<
//			typename t3::make_int_with<variance_bits>::type
//		>::type,
//		3

	//TODO: Arbitrary typedefs that work with current type strict implementation.
	typedef ulonglong2 centroid_type;
	typedef ulonglong3 variance_type;

//	typedef uint8_t channel_type;
//	typedef typename boost::make_unsigned<
//		typename t3::make_int_with<
//			t3::bits<W * H * std::numeric_limits<channel_type>::max()>::n
//		>::or_more_bits
//	>::type mean_color_type;

	typedef cuimg::array_2d<      area_type, W, H>       area_table;
	typedef cuimg::array_2d<  centroid_type, W, H>   centroid_table;
	typedef cuimg::array_2d<  variance_type, W, H>   variance_table;
//	typedef cuimg::array_2d<mean_color_type, W, H> mean_color_table;

//	typedef cuimg::image<    binary_type, W, H>     binary_image;
//	typedef cuimg::image<   segment_type, W, H>    segment_image;
//	typedef cuimg::image<     label_type, W, H> full_label_image;
//	typedef cuimg::image<full_label_type, W, H>      label_image;
//	//TODO: Abstract out properties so that we may specify only desired properties using tags
//	typedef cuimg::array_2d<area_type, centroid_type, variance_type> properties;
};

//TODO: Consider specializing array_2d pixel access operators to treat indices of type full_label differently: flags and segment should always be stripped when using this type for pixel access. This would make the code less error prone
class full_label {
public:
	typedef meta_cuccl<>::full_label_type base_type;

	__host__ __device__ inline
	static full_label max() {
		return std::numeric_limits<base_type>::max();
	}

	__host__ __device__ inline
	static full_label min() {
		return std::numeric_limits<base_type>::min();
	}

	__host__ __device__ inline
	static full_label background() {
		return max();
	}

	__host__ __device__ inline
	full_label(const base_type &seg, const base_type &val) :
			number(0) {

		segment() = seg;
		value() = val;
	}

	__host__ __device__ inline
	full_label(const base_type &some_number = 0) :
			number(some_number) {
	}

	__host__ __device__ inline
	full_label(const full_label &other_label) :
			number(other_label.number) {
	}

	__host__ __device__ inline
	full_label& operator=(const full_label &other_label) {
		number = other_label.number;
		return *this;
	}

	__host__ __device__ inline
	operator base_type() const {
		return number;
	}

	__host__ __device__ inline
	bool is_background() const {
		return number == background();
	}

	__host__ __device__ inline
	bool is_foreground() const {
		return not is_background();
	}

	struct segment_accessor {
		full_label &label_obj;

		__host__ __device__
		segment_accessor(full_label &label_obj) :
				label_obj(label_obj) {
		}

		__host__ __device__
		base_type operator=(const base_type &value) {
			label_obj.number = ((value << SEGMENT_OFFSET) & SEGMENT_MASK) | (label_obj.number & LABEL_MASK);
			return (label_obj.number & SEGMENT_MASK) >> SEGMENT_OFFSET;
		}

		__host__ __device__
		operator base_type() const {
			return (label_obj.number & SEGMENT_MASK) >> SEGMENT_OFFSET;
		}
	};

	__host__ __device__ inline
	base_type segment() const {
		return (number & SEGMENT_MASK) >> SEGMENT_OFFSET;
	}

	__host__ __device__ inline
	segment_accessor segment() {
		return segment_accessor(*this);
	}

	struct value_accessor {
		full_label &label_obj;

		__host__ __device__
		value_accessor(full_label &label_obj) :
				label_obj(label_obj) {
		}

		__host__ __device__
		base_type operator=(const base_type &value) {
			label_obj.number = (label_obj.number & SEGMENT_MASK) | (value & LABEL_MASK);
			return value & LABEL_MASK;
		}

		__host__ __device__
		operator base_type() const {
			return label_obj.number & LABEL_MASK;
		}
	};

	__host__ __device__ inline
	base_type value() const {
		return number & LABEL_MASK;
	}

	__host__ __device__ inline
	value_accessor value() {
		return value_accessor(*this);
	}

//private:
	static const base_type USABLE_MASK = (1 << meta_cuccl<>::full_label_bits_total) - 1;
	static const base_type LABEL_MASK  = (1 << meta_cuccl<>::label_bits) - 1;

	static const base_type SEGMENT_OFFSET = meta_cuccl<>::label_bits_total;
	static const base_type SEGMENT_MASK = USABLE_MASK ^ LABEL_MASK;

	base_type number;
};

template<class PixelType>
class base_image:
		public cuimg::image<PixelType, LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_WIDTH, LCG_CUCCL_LCG_CUCCL_MAX_SCREEN_HEIGHT> {
};

class segment_image :
		public base_image<meta_cuccl<>::segment_type> {

public:
	__host__ __device__ inline
	static element_type background() {
		return std::numeric_limits<element_type>::max();
	}

	__host__ __device__ inline
	static element_type foreground() {
		return std::numeric_limits<element_type>::min();
	}
};

//TODO: Missing image types: binary_image, label_image

class full_label_image :
		public meta_cuccl<>,
		public base_image<full_label> {

public:

	//TODO: Should indices be unsigned?
	__host__ __device__ inline
	bool is_root(int x, int y = 0) const {
		return (*this)(x, y).value() == y * width + x;
	}
};

struct properties {
	typedef meta_cuccl<>::      area_type       area_type;
	typedef meta_cuccl<>::  centroid_type   centroid_type;
	typedef meta_cuccl<>::  variance_type   variance_type;
//	typedef meta_cuccl<>::mean_color_type mean_color_type;

	meta_cuccl<>::      area_table area;
	meta_cuccl<>::  centroid_table centroid;
	meta_cuccl<>::  variance_table variance;
//	meta_cuccl<>::mean_color_table mean_color;
};

} /* namespace tlce*/
} /* namespace lcg */

#endif /* LCG_CUCCL_CUCCL_H_ */
