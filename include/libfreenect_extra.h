#ifndef LIBFREENECT_EXTRA_H_
#define LIBFREENECT_EXTRA_H_

#include <libfreenect.h>

#ifdef __cplusplus
extern "C" {
#endif

char*
freenect_resolution_name(freenect_resolution resolution, char *resolution_name);

char*
freenect_depth_mode_name(freenect_frame_mode depth_mode, char *depth_mode_name);

char*
freenect_video_mode_name(freenect_frame_mode video_mode, char *video_mode_name);

#ifdef __cplusplus
}
#endif

#endif /* LIBFREENECT_EXTRA_H_ */
