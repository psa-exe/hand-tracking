#ifndef LCG_CUIMG_PREFIX_SUM_H_
#define LCG_CUIMG_PREFIX_SUM_H_

#include <algorithm>

#include <cutils/cuda_exception.h>
#include <cutils/wrappers/device.h>
#include <cutils/wrappers/device_ref.h>

#include <thrust/scan.h>

namespace lcg {
namespace cuimg {

//TODO: Once a variadic base array class is provided, allow such operations only on flattened versions of the arrays
namespace kernels {

/**
 * \brief Performs prefix sum on thread block level.
 *
 * If the input array contains BLOCK_SIZE or less elements, the prefix sum of
 * all elements is computed. Otherwise, the prefix sum of every group of
 * consecutive 'BLOCK_SIZE' elements is computed. Either way, results are
 * stored in the input array itself.
 */
template<class T>
__global__
void sequential_scan(T *array_global, size_t length, T *array_blocks = NULL, T *last = NULL) {
	extern __shared__ T array_local[];

	const int index_global = blockDim.x * blockIdx.x + threadIdx.x;
	const int index_local  = threadIdx.x;

	if (index_global >= length)
		return;

	array_local[index_local] = array_global[index_global];
	__syncthreads();

	for (int off = 1; off < blockDim.x; off <<= 1) {
		T temp = (index_local >= off) ? array_local[index_local - off] : 0;
		__syncthreads();
		array_local[index_local] += temp;
		__syncthreads();
	}

	array_global[index_global] = array_local[index_local];

	if (array_blocks != NULL and index_local == blockDim.x - 1)
		array_blocks[blockIdx.x] = array_local[index_local];

	if (last != NULL and index_global == length - 1)
		*last = array_local[index_local];
}

template <class Type>
__global__
void tree_scan(Type *array, size_t n, Type *blocks = NULL) {
	const int block_threads  =     blockDim.x;
	const int block_elements = 2 * blockDim.x;
	const int last_element = block_elements - 1;
	const int last_thread  = block_threads  - 1;

	const int b = blockIdx.x;
	const int t = threadIdx.x;

	extern __shared__ Type data[];
	Type * const inout = data;
	Type * const up    = data + block_elements;
	Type * const down  = data + block_elements;
	array = array + b * block_elements;

	int leap = 2;

	//TODO: Is there a more efficient loading strategy?
	// Load values from global memory into input/output and up-sweep shared memory arrays
	up[t] = inout[t] = array[t];
	up[block_threads + t] = inout[block_threads + t] = array[block_threads + t];
	__syncthreads();

	// Up-sweep (log_2(n) passes)
	while (leap <= block_elements) {
		const int vertex = leap * (t + 1) - 1;
		const int left = vertex - leap / 2, right = vertex;

		if (vertex < block_elements) {
			up[vertex] = up[left] + up[right];
		}

		leap <<= 1;
		__syncthreads();
	}

	// Prepare for down-sweep: place identity on root node
	if (t == 0)
		down[last_element] = 0;

	// Down-sweep (log_2(n) - 1 passes)
	leap = block_elements;
	while (leap > 1) {
		const int vertex = leap * (t + 1) - 1;
		const int left = vertex - leap / 2, right = vertex;

		if (vertex < block_elements) {
			const Type vertex_value = down[vertex];
			down[right] = vertex_value + up[left];
			down[left ] = vertex_value;
		}

		leap >>= 1;
		__syncthreads();
	}

	// Add partial sums from last down-sweep pass to obtain the inclusive scan
	inout[t] += down[t];
	inout[block_threads + t] += down[block_threads + t];

	// Save results to global array
	array[t] = inout[t];
	array[block_threads + t] = inout[block_threads + t];

	// If last thread of block, store result in block array
	if (t == last_thread and blocks != NULL)
		blocks[b] = inout[last_element];
}

/**
 * \brief Adds entries on a given vector to a longer one.
 * Given two input vectors 'a' and 'b', of BLOCK_SIZE² and BLOCK_SIZE elements,
 * respectively, adds the ith entry of 'b' to the ith group of consecutive
 * BLOCK_SIZE elements in 'a'. It is like summing a constant to a vector, but
 * every block gets a different constant.
 */
template<class T>
__global__
void add_blocks_sequential(T *array, size_t length, T *blocks, T *last = NULL) {
	const int index = blockDim.x * (blockIdx.x + 1) + threadIdx.x;

	if (index >= length)
		return;

	array[index] += blocks[blockIdx.x];

	if (last != NULL and index == length - 1)
		*last = array[index];
}

template <class Type>
__global__
void add_blocks_tree(Type *array, size_t n, Type *blocks) {
	const int i = blockIdx.x * blockDim.x + threadIdx.x;

	array[i] += blocks[blockIdx.x / 2];
}

} /* namespace kernels */

using cutils::device;
using cutils::device_ref;
using cutils::hosted;

//TODO: Add parameter for inclusive X exclusive scan and a cudaStream_t parameter
//TODO: Function signature was previously specified in terms of array_2d, but device<image> to device<array_2d> conversion must be thought over (easiest way is to allow constructing an arbitrary wrapper from any possible type, which would fail for incompatible types). This relaxation may be more easily applied on reference wrappers, since they are initialized by pointer
//TODO: Memory wrappers should have a base type to allow passing both uploaded (device) and downloaded (host) parameters
/**
 * \brief Calculate in-place inclusive prefix sum of array elements.
 * @param some_array
 */
template <class T>
device_ref<T> prefix_sum(T *array, size_t length) {

	cudaDeviceProp device_props;
	CUDA_CHECK( cudaGetDeviceProperties(&device_props, 0) );

	{
	const size_t block_max = std::min({
		size_t(device_props.maxThreadsPerBlock),
		size_t(device_props.sharedMemPerBlock / sizeof(T))
	});
	const size_t block = std::min({	block_max, length });
	const size_t groups = (length - 1) / block + 1;
	const size_t shmem = block * sizeof(T);

	device<T> last_dev;

	if (block >= length) {
		kernels::sequential_scan<<<groups, block, shmem>>>(array, length, (T*) NULL, &last_dev);
	} else {
		device<T*> group_array_dev(groups);

		kernels::sequential_scan<<<groups, block, shmem>>>(
			array, length, (T*) group_array_dev);

		prefix_sum((T*) group_array_dev, groups);

		kernels::add_blocks_sequential<<<groups - 1, block>>>(
			array, length, (T*) group_array_dev, &last_dev
		);
	}
	}

	{
	const size_t block_max = std::min({
		size_t(device_props.maxThreadsPerBlock),
		size_t(device_props.sharedMemPerBlock / (4 * sizeof(T)))
	});
	const size_t block = std::min({	block_max, length / 2 });
	const size_t groups = (length - 1) / (2 * block) + 1;
	const size_t shmem = 4 * block * sizeof(T);

	device<T> last_dev;

	if (2 * block >= length) {
		kernels::tree_scan<<<groups, block, 4 * block * sizeof(T)>>>(array, length, (T*) NULL/*, &last_dev*/);
	} else {
		device<T*> group_array_dev(groups);

		kernels::tree_scan<<<groups, block, shmem>>>(
			array, length, (T*) group_array_dev);

		prefix_sum((T*) group_array_dev, groups);

		//TODO: Blocks are coming in 1024 threads, for some reason
		kernels::add_blocks_tree<<<2 * (groups - 1), block>>>(
			array + 2 * block, length - 2 * block, (T*) group_array_dev/*, &last_dev*/
		);
	}

	return last_dev;
	}
}

} /* namespace cuimg */
} /* namespace lcg */

#endif /* LCG_CUIMG_PREFIX_SUM_H_ */
