#ifndef LCG_CUIMG_FUNCTIONS_THRESHOLD_H_
#define LCG_CUIMG_FUNCTIONS_THRESHOLD_H_

#include <limits>
#include <cutils/thread_index.h>

namespace lcg {
namespace cuimg {

using cutils::thread_index;
using cutils::vec2i;

/**
 * \brief Applies a threshold on image values, clamping values beyond specified range.
 * @param some_image Target image. Must have a scalar (\class std::numeric_limits<> specialized) `Type`
 * @param lower_bound Lower bound
 * @param higher_bound Higher bound
 * @param low_value Value applied to elements below the `low` param. Defaults to `Type`'s minimum
 * @param high_value Value applied to elements above the `high` param. Defaults to `Type`'s maximum
 */
template <class Type, int W, int H>
__global__
void threshold(
		image<Type, W, H> &some_image,
		Type  lower_bound,
		Type higher_bound,
		Type  low_value = std::numeric_limits<Type>::min(),
		Type high_value = std::numeric_limits<Type>::max()) {

	const vec2i p = thread_index().xy();
	const int   i = thread_index().plain();

	Type value = some_image(p.x(), p.y());

	if (value < lower_bound and low_value != value)
		some_image(p.x(), p.y()) = low_value;
	else if (value >= higher_bound and high_value != value)
		some_image(p.x(), p.y()) = high_value;
}

} /* namespace cuimg */
} /* namespace lcg */

#endif /* LCG_CUIMG_FUNCTIONS_THRESHOLD_H_ */
