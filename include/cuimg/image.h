#ifndef LCG_CUCCL_CORE_IMAGE_H_
#define LCG_CUCCL_CORE_IMAGE_H_

#include <cutils/types.h>

#include <functional>
#include <iomanip>
#include <iostream>
#include <initializer_list>
#include <sstream>
#include <stdexcept>

#include <vector_types.h>

#ifdef CUIMG_OPENCV_SUPPORT
#include <opencv2/core/core.hpp>
#endif /* CUIMG_OPENCV_SUPPORT */

namespace lcg {
namespace cuimg {

using cutils::vec2i;

//TODO: Add a derived class of array_2d that works as an array_2d reference. It may be used for inline casts (interpreting the contents as having a different data type), such as array_2d<RGB8888>.as<type>

//TODO: Change from int2 to vec2i
struct rectangle {
	int width, height;
	int x, y;

	__host__ __device__
	rectangle(size_t width, size_t height, int x = 0, int y = 0);

	__host__ __device__
	rectangle(size_t width, size_t height, const int2 &center);

	__host__ __device__
	int area() const;

	//TODO: Change types
	__host__ __device__
	ulonglong2 centroid() const;

	__host__ __device__
	ulonglong2 variance() const;

	__host__ __device__
	float covariance() const;

	__host__ __device__
	bool contains(const int2 &point) const;
};

std::ostream& operator<<(std::ostream &os, const rectangle &rect);
//TODO: Class should be moved to cutils and could benefit from variadic templates to provide multidimensional arrays (and perhaps, with dynamic versions?), and even serve as base class for matrices
template<class PixelType, int Width, int Height = 1>
class array_2d {
public:
	static const int width  = Width;
	static const int height = Height;
	static const int dims   = width * height;
	static const int size   = dims  * sizeof(PixelType);
	static const int pitch  = width * sizeof(PixelType);
	static const rectangle domain;

	typedef PixelType element_type;

	#ifdef CUIMG_OPENCV_SUPPORT
	static const cv::Size cvSize;
	static const int cvType;
	#endif /* CUIMG_OPENCV_SUPPORT */

	PixelType data[height * width];

	__host__ __device__
	array_2d() {
	}

	//TODO: Provide a two-dimensional version of initializer_list-based constructor, as in cutils::matrix
	__host__ __device__
	array_2d(const std::initializer_list<PixelType> &values) {
		*this = values;
	}

	//TODO: Check requirements to make this class actually compatible with STL iterators
	PixelType* begin() {
		return data;
	}

	const PixelType* begin() const {
		return data;
	}

	PixelType* end() {
		return data + dims;
	}

	const PixelType* end() const {
		return data + dims;
	}

	void for_each(std::function<void(int,int)> procedure) {
		for (int y = 0; y < Height; y++)
			for (int x = 0; x < Width; x++)
				procedure(x, y);
	}

	__host__ __device__ inline
	PixelType& operator()(int x, int y = 0) {
		return data[y * width + x];
	}

	__host__ __device__ inline
	PixelType& operator[](const vec2i &point) {
		return this->operator()(point.x(), point.y());
	}

	__host__ __device__ inline
	PixelType& operator[](const vec2i &point) const {
		return this->operator()(point.x(), point.y());
	}

	__host__ __device__ inline
	const PixelType& operator()(int x, int y = 0) const {
		return data[y * width + x];
	}

	template<class OtherType>
	__host__ __device__
	const OtherType& at(int x, int y = 0) const {
		return reinterpret_cast<OtherType&>(*this)(x, y);
	}

	template<class OtherType>
	__host__ __device__
	const OtherType& at(const vec2i &point) const {
		return at(point.x(), point.y());
	}

	template<class OtherType>
	__host__ __device__
	OtherType& at(int x, int y = 0) {
		return reinterpret_cast<OtherType&>(*this)(x, y);
	}

	template<class OtherType>
	__host__ __device__
	OtherType& at(const vec2i &point) {
		return at(point.x(), point.y());
	}

	__host__ __device__ inline
	void fill(const PixelType &value) {
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				(*this)(x, y) = value;
	}

	__host__ __device__ inline
	array_2d& operator=(const PixelType &value) {
		fill(value);
		return *this;
	}

	__host__ __device__ inline
	array_2d& operator=(const std::initializer_list<PixelType> &values) {
		#ifndef __CUDA_ARCH__
		if (values.size() != dims)
			throw std::logic_error("array_2d initialized with incorrect number of values");
		#endif

		int i = 0;
		for (const PixelType &value : values)
			data[i++] = value;

		return *this;
	}

	#ifdef CUIMG_OPENCV_SUPPORT
	operator cv::Mat() {
		return cv::Mat(cvSize, cvType, data);
	}

	operator const cv::Mat() const {
		return cv::Mat(cvSize, cvType, data);
	}
	#endif /* CUIMG_OPENCV_SUPPORT */

	operator std::string() const {
		std::stringstream string;

		for (int i = 0; i < Height; i++) {
			for (int j = 0; j < Width; j++) {
				if (sizeof(PixelType) == sizeof(char))
					string << std::setw(8) << std::setfill(' ') << int((*this)(j, i));
				else
					string << std::setw(8) << std::setfill(' ') << (*this)(j, i);

				if (j < Width - 1)
					string << ",";
			}
			string << std::endl;
		}

		return string.str();
	}
};

template<class T, int W, int H>
const int array_2d<T, W, H>::width;

template<class T, int W, int H>
const int array_2d<T, W, H>::height;

template<class T, int W, int H>
const int array_2d<T, W, H>::dims;

template<class T, int W, int H>
const int array_2d<T, W, H>::size;

template<class T, int W, int H>
const int array_2d<T, W, H>::pitch;

template<class T, int W, int H>
const rectangle array_2d<T,W,H>::domain(W,H);

#ifdef CUIMG_OPENCV_SUPPORT
template<class T, int W, int H>
const cv::Size array_2d<T,W,H>::cvSize(W, H);

template<class T, int W, int H>
//TODO: This is actually broken: uint32_t is not mapped into a valid type and int32_t is mapped into 32SC1 and not 8UC4. The solution: insert a number of channels into image and create ways to interpret an image as a merging of channels?
const int array_2d<T,W,H>::cvType = cv::DataType<T>::type;
#endif /* CUIMG_OPENCV_SUPPORT */

//TODO: Images could be parameterized by number of channels, allowing vector-based pixel access or element-based component access and iteration
//TODO: Publish typedefs for corresponding texture types. This library might provide some default textures and binding functions for each texturizable type
template<class PixelType, int Width, int Height>
class image: public array_2d<PixelType, Width, Height> {
public:
	using array_2d<PixelType, Width, Height>::array_2d;

	__host__ __device__
	static bool inside(const int2 &p) {
		return p.x >= 0 and p.y >= 0 and p.x < Width and p.y < Height;
	}
};

template <class T, int W, int H>
void draw(image<T, W, H> &img, const rectangle &rect, const T &color) {
	for (int y = rect.y; y < rect.x + rect.height and y < H; y++)
		for (int x = rect.x; x < rect.x + rect.width and x < W; x++)
			img(x, y) = color;
}

} /* namespace cuimg */
} /* namespace lcg */

#endif /* LCG_CUCCL_CORE_IMAGE_H_ */
