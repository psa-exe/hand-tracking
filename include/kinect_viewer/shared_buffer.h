#ifndef LCG_MSC_SHARED_BUFFER_H_
#define LCG_MSC_SHARED_BUFFER_H_

#include <atomic>
#include <functional>
#include <stdx/semaphore.h>

namespace lcg {
namespace msc {

struct shared_buffer {
	shared_buffer(size_t slot_size, int slots = 1);

	~shared_buffer();

	int consume(std::function<void(void*)> consume_function);

	int produce(std::function<void(void*)> produce_function);

	void* read_address();

	const void* read_address() const;

	void* write_address();

	const void* write_address() const;

private:
	const size_t slot_size;
	const int slots;
	void * const buffer;

	std::atomic<int> pos_read, pos_write;

	stdx::semaphore sem_read;
	stdx::semaphore sem_write;
};

} /* namespace msc */
} /* namespace lcg */
#endif /* LCG_MSC_SHARED_BUFFER_H_ */
