#ifndef IDEAS_H_
#define IDEAS_H_

/**
 * Variadic array class with indefinite number of dimensions. Must include a variadic permutation object describing the memory layout (row-major, column-major, etc.)
 * It must be possible to instantiate arrays either by allocating them on the heap or by using shared pointers. This could be achieved by providing two compatible subclasses of array_base, in which case copying would be naturally restricted by memory spaces (I think).
 */

/**
 * Template utility to convert array of one size into another:
 * void process_rgb(int (&array)[3]);
 * .
 * .
 * .
 * int array[6];
 * process_rgb(subarray<3>(array));
 * process_rgb(subarray<1, 3>(array));
 */

//Create a template facility to wrap enum classes so that converting to and from string is as easy as in Java. The idea is to use a specialization to declare a static map<string, Type> to lookup correspondencies, but the difficult part is that the mapping should be unique.
//template<typename Type>
//struct EnumType {
//	EnumType(const Type &value);
//	EnumType(const EnumType &other);
//	EnumType(const stdx::string &name);
//
//	stdx::string name() const;
//	operator stdx::string() const;
//};

#endif /* IDEAS_H_ */
