#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <GL/glew.h> //TODO: Just here to prevent that "gl.h" included before "glew.h" bullshit
#include <cutils/wrappers.h>
#include <msc/types.h>
#include <msc/Module.h>
#include <opencv2/core/core.hpp>
#include <t3/SingletonClass.h>
#include <thrust/host_vector.h>
#include <tucano/framebuffer.hpp>

namespace lcg {
namespace msc {

class CameraDisplay :
		public Module,
		public t3::SingletonClass<CameraDisplay> {

	friend SingletonBase;

	CameraDisplay();

	void displayImages();
	void processEvents();
	int waitKey();

	int min_depth, max_depth;

public:
	cv::Mat montageImageOutput;

	virtual void run();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* DISPLAY_H_ */
