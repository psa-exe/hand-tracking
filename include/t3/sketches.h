#ifndef SKETCHES_H_
#define SKETCHES_H_

/**
 * Variadic function to convert a sequence of numbers of any types into a sequence of the strongest type. E.g.
 * list(3, 1234124354365, 9.5f, 0.4d) -> { 3.0d, 3.0d, 1234124354365.0d, 9.5d, 0.4d } (std::initializer_list<double>)
 */

#endif /* SKETCHES_H_ */
