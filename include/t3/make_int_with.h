#ifndef LCG_TEMPLATRICKS_MAKE_INT_H_
#define LCG_TEMPLATRICKS_MAKE_INT_H_

#include <stdint.h>

namespace lcg {
namespace t3 {

//TODO: Once we have a bignum library, requiring more than 64 bits may trigger the instantiation of a bignum type
template <class Type>
struct make_int_with_base {
	typedef Type or_more_bits;
};

template <int Bits>
struct make_int_with :
		make_int_with_base<int64_t> {

	static_assert(Bits >   0, "make_int_with cannot make a type with Bits <= 0");
	static_assert(Bits <= 64, "make_int_with cannot make a type with Bits > 64");
};

template<>
struct make_int_with<1> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<2> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<3> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<4> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<5> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<6> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<7> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<8> :
		make_int_with_base<int8_t> {
};

template<>
struct make_int_with<9> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<10> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<11> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<12> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<13> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<14> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<15> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<16> :
		make_int_with_base<int16_t> {
};

template<>
struct make_int_with<17> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<18> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<19> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<20> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<21> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<22> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<23> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<24> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<25> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<26> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<27> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<28> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<29> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<30> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<31> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<32> :
		make_int_with_base<int32_t> {
};

template<>
struct make_int_with<33> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<34> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<35> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<36> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<37> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<38> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<39> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<40> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<41> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<42> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<43> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<44> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<45> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<46> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<47> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<48> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<49> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<50> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<51> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<52> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<53> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<54> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<55> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<56> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<57> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<58> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<59> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<60> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<61> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<62> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<63> :
		make_int_with_base<int64_t> {
};

template<>
struct make_int_with<64> :
		make_int_with_base<int64_t> {
};

} /* namespace t3 */
} /* namespace lcg */

#endif /* LCG_TEMPLATRICKS_MAKE_INT_H_ */
