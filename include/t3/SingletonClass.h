#ifndef SINGLETONCLASS_H_
#define SINGLETONCLASS_H_

#include <iostream>

namespace lcg {
namespace t3 {

/**
 * \brief Support template for creating classes that follow the singleton design pattern.
 *
 * \note Currently only single instance singletons are supported.
 */
template <class ClassType>
class SingletonClass {
public:
	typedef SingletonClass SingletonBase;

	friend class InstanceCleaner;

	static ClassType& instance() {
		if (the_instance == nullptr)
			the_instance = new ClassType;
		return *the_instance;
	}

protected:
	SingletonClass() {
		instance_cleaner.forceSpecialization();
	}

	virtual ~SingletonClass() {}

private:

	/**
	 * \brief Helper class to enforce Singleton destruction upon normal application termination.
	 */
	struct InstanceCleaner {
		~InstanceCleaner() {
// TODO: Instance cleaning causes a segfault; investigate why (probably beacause of a module that keeps running after quit has been issued).
//			delete the_instance;
		}

		void forceSpecialization() {}
	};

	static InstanceCleaner instance_cleaner;
	static ClassType* the_instance;
};

template <class ClassType>
ClassType*
SingletonClass<ClassType>::the_instance = nullptr;

template <class ClassType>
typename SingletonClass<ClassType>::InstanceCleaner
SingletonClass<ClassType>::instance_cleaner;

} /* namespace t3 */
} /* namespace lcg */

#endif /* SINGLETONCLASS_H_ */
