#ifndef T3_FIELD_H_
#define T3_FIELD_H_

#include <functional>

namespace t3 {

/**
 * \brief Wraps class members with a custom setter function.
 *
 * This template class provides an alternative idiom to private members with
 * custom getters and setters. A setter function (a lambda, possibly wrapping
 * another member function) is provided during attribute construction and is
 * used whenever the attribute is to be modified. Direct conversion is supported
 * when reading and member access is possible through the -> operator.
 */
template <class FieldType>
class field {
public:
	typedef std::function<FieldType(const FieldType&)> SetterFunction;

	field(SetterFunction setter, const FieldType &value = FieldType()) :
		value(value), setter(setter) {
	}

	FieldType& operator->() {
		return value;
	}

	operator const FieldType&() const {
		return value;
	}

	FieldType operator=(const FieldType &newvalue) {
		value = setter(newvalue);
		return value;
	}

private:
	SetterFunction setter;
	FieldType value;
};

}

#endif /* T3_FIELD_H_ */
