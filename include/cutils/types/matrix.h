#ifndef LCG_CUDAUTILS_TYPES_MATRIX_CUH_
#define LCG_CUDAUTILS_TYPES_MATRIX_CUH_

#include <initializer_list>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <stdio.h>

#include "cuda_vector.h"
#include "dimensionality.h"

#include <t3/supertype.h>

namespace lcg {
namespace cutils {

using t3::supertype;

template <class Scalar, int M, int N>
struct matrix;

//TODO: Add support for initializer_list in constructors. Suggestion: add dimension<> specializations for initializer_list<>
//TODO: Add some operations to matrix and specialized vector types, such as: translation, scaling, normalization, magnitude, rotation (various forms), static constructors, remaining arithmetic operators and so.
//TODO: Implement inverse calculation
//TODO: Implement matrices with dynamic sizing? Use CRTP?

/**
 * \brief Base class for \class matrix. Should not be used directly in most cases.
 */
template <class Scalar, int M, int N>
struct matrix_base {
	Scalar data[M][N];

	//TODO: Make a variadic template constructor accepting all elements? Such as in vector?

	__host__ __device__
	matrix_base() {
	}

	__host__ __device__
	matrix_base(const std::initializer_list<std::initializer_list<Scalar>> &elements) {

		#ifndef __CUDA_ARCH__
		if (elements.size() != M) {
			printf("%d, %d\n", M, elements.size()); fflush(stdout);
			throw std::logic_error("The number of rows in the initializer_list passed to matrix constructor does not match the matrix's");
		}
		#endif

		int i = 0, j;
		for (auto &row : elements) {
			#ifndef __CUDA_ARCH__
			if (row.size() != N) {
				throw std::logic_error("The number of columns in the initializer_list passed to matrix constructor does not match the matrix's");
			}
			#endif

			j = 0;
			for (auto &coef : row) {
				(*this)(i, j) = coef;
				j++;
			}

			i++;
		}
	}

	__host__ __device__
	matrix_base(const matrix_base &other) {
		(*this) = other;
	}

//	__host__ __device__
//	matrix_base(const Scalar (&other)[M][N]) {
//	}

	__host__ __device__
	matrix_base(const Scalar *data) {
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				(*this)(i, j) = data[i * N + j];
	}

	//TODO: Default template arguments cannot be used for function templates. This makes it uglier to call block, since all parameters (including I and J) must be specified. Study a workaround -> C++11 allows this
	//TODO: This should return a referencing object, not a new instance. Perhaps, all matrix operations should all be implemented with lazy evaluation, just as Eigen does.
	template <int Rows, int Cols, int I = 0, int J = 0>
	__host__ __device__
	matrix<Scalar, Rows, Cols>
	block() const {
		matrix<Scalar, Rows, Cols> submatrix_base;

		for (int i = I; i < I + Rows; i++)
			for (int j = J; j < J + Cols; j++)
				submatrix_base(i, j) = (*this)(i, j);

		return submatrix_base;
	}

	template <class OtherScalar>
	__host__ __device__
	matrix<OtherScalar, M, N>
	cast() const {
		matrix<OtherScalar, M, N> cast_matrix_base;

		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				cast_matrix_base(i, j) = (*this)(i, j);

		return cast_matrix_base;
	}

	template <class OtherScalar, int C>
	__host__ __device__
	matrix<typename supertype<Scalar, OtherScalar>::type, N, C>
	dot(const matrix<OtherScalar, M, C> &right_matrix) const {
		return transposed() * right_matrix;
	}

	template <class OtherScalar>
	__host__ __device__
	void fill(OtherScalar value) {
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				(*this)(i, j) = value;
	}

	__host__ __device__
	matrix<Scalar, N, M>
	transposed() const {
		matrix<Scalar, N, M> transposed_matrix_base;

		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				transposed_matrix_base(j, i) = (*this)(i, j);

		return transposed_matrix_base;
	}

	__host__ __device__
	matrix_base&
	operator=(const matrix_base &other) {
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				(*this)(i, j) = other(i, j);

		return *this;
	}

	__host__ __device__
	const Scalar&
	operator[](int i) const {
		return ((Scalar*) data)[i];
	}

	__host__ __device__
	Scalar&
	operator[](int i) {
		return ((Scalar*) data)[i];
	}

	__host__ __device__
	const Scalar&
	operator()(int i, int j = 0) const {
		return data[i][j];
	}

	__host__ __device__
	Scalar&
	operator()(int i, int j = 0) {
		return data[i][j];
	}

	operator std::string() const {
		std::stringstream string;

		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				if (sizeof(Scalar) == sizeof(char))
					string << std::setw(6) << std::setfill(' ') << int((*this)(i, j));
				else
					string << std::setw(6) << std::setfill(' ') << (*this)(i, j);

				if (j < N - 1)
					string << ",";
			}
			string << std::endl;
		}

		return string.str();
	}
};

template <class Scalar, int M, int N>
struct matrix : matrix_base<Scalar, M, N> {

	using matrix_base<Scalar, M, N>::matrix_base;
};

template <class Scalar, int N>
struct matrix<Scalar, N, N> : matrix_base<Scalar, N, N> {

	using matrix_base<Scalar, N, N>::matrix_base;

	//TODO: Implement decent determinant calculation
	__host__ __device__
	typename supertype<Scalar, float>::type
	determinant() const {
		return (*this)(0, 0) * (*this)(1, 1) - (*this)(0, 1) * (*this)(1, 0);
	}

	//TODO: Implement decent inverse calculation
	__host__ __device__
	matrix<typename supertype<Scalar, float>::type, N, N>
	inverse() const {
		static_assert(N == 2, "Inverse operation has only been implemented for 2 x 2 matrices");

		matrix<typename supertype<Scalar, float>::type, N, N> result = {
			{  (*this)(1, 1), -(*this)(0, 1) },
			{ -(*this)(1, 0),  (*this)(0, 0) }
		};

		return result / determinant();
	}
};

template <class Scalar>
struct matrix<Scalar, 4, 1> : matrix_base<Scalar, 4, 1> {

	__host__ __device__
	operator typename cuda_vector<Scalar, 4>::type() const {
		return { (*this)(0), (*this)(1), (*this)(2), (*this)(3) };
	}
};

template <class Scalar>
struct matrix<Scalar, 3, 1> : matrix_base<Scalar, 3, 1> {

	__host__ __device__
	operator typename cuda_vector<Scalar, 3>::type() const {
		return { (*this)(0), (*this)(1), (*this)(2) };
	}
};

template <class Scalar>
struct matrix<Scalar, 2, 1> : matrix_base<Scalar, 2, 1> {

	__host__ __device__
	operator typename cuda_vector<Scalar, 2>::type() const {
		return { (*this)(0), (*this)(1) };
	}
};

template <class Scalar>
struct matrix<Scalar, 1, 1> : matrix_base<Scalar, 1, 1> {

	using matrix_base<Scalar, 1, 1>::matrix_base;

	//TODO: Removing this constructor generates a compilation error when instantiating vec1*() objects.
	__host__ __device__
	matrix() {
		(*this)(0, 0) = 0;
	}

	//TODO: May be erased if matrix_base implements a constructor with an indefinite number of parameters.
	__host__ __device__
	matrix(Scalar element) {
		(*this)(0, 0) = element;
	}

	__host__ __device__
	typename supertype<Scalar, float>::type
	determinant() const {
		return Scalar(*this);
	}

	__host__ __device__
	typename supertype<Scalar, float>::type
	inverse() const {
		return 1.0f / determinant();
	}

	__host__ __device__
	operator Scalar() const {
		return (*this)(0, 0);
	}

	__host__ __device__
	operator typename cuda_vector<Scalar, 1>::type() const {
		return { (*this)(0) };
	}
};

template <class Scalar, int M, int N>
struct dimensionality<matrix_base<Scalar, M, N>> {
	static const int value = 2;
};

template <class Scalar, int M>
struct dimensionality<matrix_base<Scalar, M, 1>> {
	static const int value = 1;
};

template <class Scalar>
struct dimensionality<matrix_base<Scalar, 1, 1>> {
	static const int value = 0;
};

template <class Scalar, int M, int N>
struct dimensionality<matrix<Scalar, M, N>> {
	static const int value = 2;
};

template <class Scalar, int M>
struct dimensionality<matrix<Scalar, M, 1>> {
	static const int value = 1;
};

template <class Scalar>
struct dimensionality<matrix<Scalar, 1, 1>> {
	static const int value = 0;
};

template <class Scalar, int M, int N>
__host__ __device__
matrix<Scalar, M, N>
operator-(const matrix<Scalar, M, N> &mat) {

	matrix<Scalar, M, N> neg_matrix;

	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
			neg_matrix(i, j) = -mat(i, j);

	return neg_matrix;
}

template <class LeftScalar, class RightScalar, int M, int N>
__host__ __device__
matrix<typename supertype<LeftScalar, RightScalar>::type, M, N>
operator+(const matrix<LeftScalar, M, N> &left_matrix, const matrix<RightScalar, M, N> &right_matrix) {

	typedef typename supertype<LeftScalar, RightScalar>::type ResultingScalar;
	matrix<ResultingScalar, M, N> sum_matrix;

	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
			sum_matrix(i, j) = left_matrix(i, j) + right_matrix(i, j);

	return sum_matrix;
}

//TODO: Arithmetic operators should go outside class definition
//TODO: supertype<...>-based type detuction could be even better, by taking operation into account. For instance, subtracting unsigneds should yeld a signed.
template <class LeftScalar, class RightScalar, int M, int N>
__host__ __device__
matrix<typename supertype<LeftScalar, RightScalar>::type, M, N>
operator-(const matrix<LeftScalar, M, N> &left_matrix, const matrix<RightScalar, M, N> &right_matrix) {

	typedef typename supertype<LeftScalar, RightScalar>::type ResultingScalar;
	matrix<ResultingScalar, M, N> diff_matrix;

	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
			diff_matrix(i, j) = left_matrix(i, j) - right_matrix(i, j);

	return diff_matrix;
}

template <class LeftScalar, class RightScalar, int M, int K, int N>
__host__ __device__
matrix<typename supertype<LeftScalar, RightScalar>::type, M, N>
operator*(const matrix<LeftScalar, M, K> &left_matrix, const matrix<RightScalar, K, N> &right_matrix) {

	typedef typename supertype<LeftScalar, RightScalar>::type ResultingScalar;
	matrix<ResultingScalar, M, N> product_matrix;

	for (int i = 0; i < M; i++) {
		for (int j = 0; j < N; j++) {
			ResultingScalar value = 0;
			for (int k = 0; k < K; k++) {
				value += left_matrix(i, k) * right_matrix(k, j);
			}
			product_matrix(i, j) = value;
		}
	}

	return product_matrix;
}

template <class LeftScalar, class RightScalar, int K>
__host__ __device__
typename supertype<LeftScalar, RightScalar>::type
operator*(const matrix<LeftScalar, 1, K> &left_matrix, const matrix<RightScalar, K, 1> &right_matrix) {

	typedef typename supertype<LeftScalar, RightScalar>::type ResultingScalar;

	ResultingScalar value = 0;
	for (int k = 0; k < K; k++) {
		value += left_matrix(0, k) * right_matrix(k, 0);
	}

	return value;
}

template <class LeftScalar, class RightScalar, int M, int N>
__host__ __device__
matrix<typename supertype<LeftScalar, RightScalar>::type, M, N>
operator/(const matrix<LeftScalar, M, N> &mat, RightScalar k) {

	typedef typename supertype<LeftScalar, RightScalar>::type ResultingLeftScalar;
	matrix<ResultingLeftScalar, M, N> resulting_matrix;

	for (int i = 0; i < M; i++)
		for (int j = 0; j < N; j++)
			resulting_matrix(i, j) = mat(i, j) / k;

	return resulting_matrix;
}

//TODO: Refactor matrix initializer code into separate header file
template <class Scalar, int M, int N, int I = 1, int J = 1>
struct matrix_initializer {

	typedef matrix_initializer<Scalar, M, N, I, J + 1> next_element_initializer;

	matrix_base<Scalar, M, N> &target_matrix;

	template <class OtherScalar>
	__host__ __device__
	matrix_initializer(matrix_base<Scalar, M, N> &mat, OtherScalar value) :
			target_matrix(mat) {

		target_matrix(I - 1, J - 1) = value;
	}
};

template <class Scalar, int M, int N, int I>
struct matrix_initializer<Scalar, M, N, I, N> {

	typedef matrix_initializer<Scalar, M, N, I + 1, 1> next_element_initializer;

	matrix_base<Scalar, M, N> &target_matrix;

	template <class OtherScalar>
	__host__ __device__
	matrix_initializer(matrix_base<Scalar, M, N> &mat, OtherScalar value) :
			target_matrix(mat) {

		target_matrix(I - 1, N - 1) = value;
	}
};

template <class Scalar, int M, int N>
struct matrix_initializer<Scalar, M, N, M, N> {

	typedef matrix_initializer<Scalar, M, N, 0, 0> next_element_initializer;

	matrix_base<Scalar, M, N> &target_matrix;

	template <class OtherScalar>
	__host__ __device__
	matrix_initializer(matrix_base<Scalar, M, N> &mat, OtherScalar value) :
			target_matrix(mat) {

		target_matrix(M - 1, N - 1) = value;
	}
};

template <class Scalar, int M, int N>
struct matrix_initializer<Scalar, M, N, 0, 0> {

	typedef matrix_initializer<Scalar, M, N, 0, 0> next_element_initializer;

	matrix_base<Scalar, M, N> &target_matrix;

	template <class OtherScalar>
	__host__ __device__
	matrix_initializer(matrix_base<Scalar, M, N> &mat, OtherScalar value) :
			target_matrix(mat) {
	}
};

template <class Scalar, class OtherScalar, int M, int N>
__host__ __device__
matrix_initializer<Scalar, M, N>
operator<<(matrix_base<Scalar, M, N> &target_matrix, OtherScalar first_value) {
	return matrix_initializer<Scalar, M, N>(target_matrix, first_value);
}

template <class Scalar, class OtherScalar, int M, int N, int I, int J>
__host__ __device__
typename matrix_initializer<Scalar, M, N, I, J>::next_element_initializer
operator<<(matrix_initializer<Scalar, M, N, I, J> initializer, OtherScalar next_value) {
	typedef typename matrix_initializer<Scalar, M, N, I, J>::next_element_initializer next_initializer;
	return next_initializer(initializer.target_matrix, next_value);
}

template <class Scalar, class OtherScalar, int M, int N, int I, int J>
__host__ __device__
typename matrix_initializer<Scalar, M, N, I, J>::next_element_initializer
operator,(matrix_initializer<Scalar, M, N, I, J> initializer, OtherScalar next_value) {
	return initializer << next_value;
}

//TODO: Publish more useful typedefs such as these
typedef matrix<unsigned, 2, 2> mat2u;
typedef matrix<unsigned, 3, 3> mat3u;
typedef matrix<unsigned, 4, 4> mat4u;

typedef matrix<int, 2, 2> mat2i;
typedef matrix<int, 3, 3> mat3i;
typedef matrix<int, 4, 4> mat4i;

typedef matrix<unsigned long, 2, 2> mat2ul;
typedef matrix<unsigned long, 3, 3> mat3ul;
typedef matrix<unsigned long, 4, 4> mat4ul;

typedef matrix<long, 2, 2> mat2l;
typedef matrix<long, 3, 3> mat3l;
typedef matrix<long, 4, 4> mat4l;

typedef matrix<float, 2, 2> mat2f;
typedef matrix<float, 3, 3> mat3f;
typedef matrix<float, 4, 4> mat4f;

typedef matrix<double, 2, 2> mat2d;
typedef matrix<double, 3, 3> mat3d;
typedef matrix<double, 4, 4> mat4d;

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_TYPES_MATRIX_CUH_ */
