#ifndef LCG_CUCCL_META_MAKE_VECTOR_H_
#define LCG_CUCCL_META_MAKE_VECTOR_H_

#include <vector_functions.h>
#include <vector_types.h>

#include "dimensionality.h"

#include <iostream>

namespace lcg {
namespace cutils {

// Builds a CUDA vector type from a base Type and a number of Dimensions.
// It has specializations for all CUDA vector types.
template <class Type, int Dims>
struct cuda_vector {
};

template <class Type>
struct cuda_vector_traits {
};

//TODO: Include vector traits for dim3

__host__ __device__
inline unsigned flatten(const dim3 &vec) {
	return vec.x * vec.y * vec.z;
}

__host__ __device__
inline int off(const int2 &v, const int2 &d) {
	return v.x + v.y * d.x;
}

__host__ __device__
inline int off(const dim3 &v, const dim3 &d) {
	return v.x + v.y * d.x + v.z * d.x * d.y;
}

__host__ __device__
inline int2 toint2(const ulonglong2 &v) {
	return make_int2(v.x, v.y);
}

template <>
struct cuda_vector <char, 1> {
	typedef char1 type;
};

template <>
struct cuda_vector <char, 2> {
	typedef char2 type;
};

template <>
struct cuda_vector <char, 3> {
	typedef char3 type;
};

template <>
struct cuda_vector <char, 4> {
	typedef char4 type;
};

template <>
struct cuda_vector <unsigned char, 1> {
	typedef uchar1 type;
};

template <>
struct cuda_vector <unsigned char, 2> {
	typedef uchar2 type;
};

template <>
struct cuda_vector <unsigned char, 3> {
	typedef uchar3 type;
};

template <>
struct cuda_vector <unsigned char, 4> {
	typedef uchar4 type;
};

template <>
struct cuda_vector <short, 1> {
	typedef short1 type;
};

template <>
struct cuda_vector <short, 2> {
	typedef short2 type;
};

template <>
struct cuda_vector <short, 3> {
	typedef short3 type;
};

template <>
struct cuda_vector <short, 4> {
	typedef short4 type;
};

template <>
struct cuda_vector <unsigned short, 1> {
	typedef ushort1 type;
};

template <>
struct cuda_vector <unsigned short, 2> {
	typedef ushort2 type;
};

template <>
struct cuda_vector <unsigned short, 3> {
	typedef ushort3 type;
};

template <>
struct cuda_vector <unsigned short, 4> {
	typedef ushort4 type;
};

template <>
struct cuda_vector <int, 1> {
	typedef int1 type;
};

template <>
struct cuda_vector <int, 2> {
	typedef int2 type;
};

template <>
struct cuda_vector <int, 3> {
	typedef int3 type;
};

template <>
struct cuda_vector <int, 4> {
	typedef int4 type;
};

template <>
struct cuda_vector <unsigned int, 1> {
	typedef uint1 type;
};

template <>
struct cuda_vector <unsigned int, 2> {
	typedef uint2 type;
};

template <>
struct cuda_vector <unsigned int, 3> {
	typedef uint3 type;
};

template <>
struct cuda_vector <unsigned int, 4> {
	typedef uint4 type;
};

template <>
struct cuda_vector <long int, 1> {
	typedef long1 type;
};

template <>
struct cuda_vector <long int, 2> {
	typedef long2 type;
};

template <>
struct cuda_vector <long int, 3> {
	typedef long3 type;
};

template <>
struct cuda_vector <long int, 4> {
	typedef long4 type;
};

template <>
struct cuda_vector <unsigned long int, 1> {
	typedef ulong1 type;
};

template <>
struct cuda_vector <unsigned long int, 2> {
	typedef ulong2 type;
};

template <>
struct cuda_vector <unsigned long int, 3> {
	typedef ulong3 type;
};

template <>
struct cuda_vector <unsigned long int, 4> {
	typedef ulong4 type;
};

template <>
struct cuda_vector <long long int, 1> {
	typedef longlong1 type;
};

template <>
struct cuda_vector <long long int, 2> {
	typedef longlong2 type;
};

template <>
struct cuda_vector <long long int, 3> {
	typedef longlong3 type;
};


template <>
struct cuda_vector <long long int, 4> {
	typedef longlong4 type;
};

template <>
struct cuda_vector <unsigned long long int, 1> {
	typedef ulonglong1 type;
};

template <>
struct cuda_vector <unsigned long long int, 2> {
	typedef ulonglong2 type;
};

template <>
struct cuda_vector <unsigned long long int, 3> {
	typedef ulonglong3 type;
};

template <>
struct cuda_vector <unsigned long long int, 4> {
	typedef ulonglong4 type;
};

template <>
struct cuda_vector <float, 1> {
	typedef float1 type;
};

template <>
struct cuda_vector <float, 2> {
	typedef float2 type;
};

template <>
struct cuda_vector <float, 3> {
	typedef float3 type;
};

template <>
struct cuda_vector <float, 4> {
	typedef float4 type;
};

template <>
struct cuda_vector <double, 1> {
	typedef double1 type;
};

template <>
struct cuda_vector <double, 2> {
	typedef double2 type;
};

template <>
struct cuda_vector <double, 3> {
	typedef double3 type;
};

template <>
struct cuda_vector <double, 4> {
	typedef double4 type;
};

template <>
struct cuda_vector_traits<char1> {
	typedef char base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<char2> {
	typedef char base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<char3> {
	typedef char base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<char4> {
	typedef char base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<uchar1> {
	typedef unsigned char base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<uchar2> {
	typedef unsigned char base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<uchar3> {
	typedef unsigned char base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<uchar4> {
	typedef unsigned char base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<short1> {
	typedef short base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<short2> {
	typedef short base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<short3> {
	typedef short base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<short4> {
	typedef short base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<ushort1> {
	typedef unsigned short base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<ushort2> {
	typedef unsigned short base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<ushort3> {
	typedef unsigned short base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<ushort4> {
	typedef unsigned short base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<int1> {
	typedef int base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<int2> {
	typedef int base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<int3> {
	typedef int base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<int4> {
	typedef int base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<uint1> {
	typedef unsigned int base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<uint2> {
	typedef unsigned int base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<uint3> {
	typedef unsigned int base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<uint4> {
	typedef unsigned int base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<long1> {
	typedef long base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<long2> {
	typedef long base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<long3> {
	typedef long base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<long4> {
	typedef long base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<ulong1> {
	typedef unsigned long base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<ulong2> {
	typedef unsigned long base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<ulong3> {
	typedef unsigned long base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<ulong4> {
	typedef unsigned long base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<longlong1> {
	typedef long long base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<longlong2> {
	typedef long long base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<longlong3> {
	typedef long long base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<longlong4> {
	typedef long long base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<ulonglong1> {
	typedef unsigned long long base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<ulonglong2> {
	typedef unsigned long long base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<ulonglong3> {
	typedef unsigned long long base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<ulonglong4> {
	typedef unsigned long long base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<float1> {
	typedef float base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<float2> {
	typedef float base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<float3> {
	typedef float base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<float4> {
	typedef float base_type;
	static const int dims = 4;
};

template <>
struct cuda_vector_traits<double1> {
	typedef double base_type;
	static const int dims = 1;
};

template <>
struct cuda_vector_traits<double2> {
	typedef double base_type;
	static const int dims = 2;
};

template <>
struct cuda_vector_traits<double3> {
	typedef double base_type;
	static const int dims = 3;
};

template <>
struct cuda_vector_traits<double4> {
	typedef double base_type;
	static const int dims = 4;
};

template <>
struct dimensionality <char1> {
	static const int value = 1;
};

template <>
struct dimensionality <char2> {
	static const int value = 1;
};

template <>
struct dimensionality <char3> {
	static const int value = 1;
};

template <>
struct dimensionality <char4> {
	static const int value = 1;
};

template <>
struct dimensionality <uchar1> {
	static const int value = 1;
};

template <>
struct dimensionality <uchar2> {
	static const int value = 1;
};

template <>
struct dimensionality <uchar3> {
	static const int value = 1;
};

template <>
struct dimensionality <uchar4> {
	static const int value = 1;
};

template <>
struct dimensionality <short1> {
	static const int value = 1;
};

template <>
struct dimensionality <short2> {
	static const int value = 1;
};

template <>
struct dimensionality <short3> {
	static const int value = 1;
};

template <>
struct dimensionality <short4> {
	static const int value = 1;
};

template <>
struct dimensionality <ushort1> {
	static const int value = 1;
};

template <>
struct dimensionality <ushort2> {
	static const int value = 1;
};

template <>
struct dimensionality <ushort3> {
	static const int value = 1;
};

template <>
struct dimensionality <ushort4> {
	static const int value = 1;
};

template <>
struct dimensionality <int1> {
	static const int value = 1;
};

template <>
struct dimensionality <int2> {
	static const int value = 1;
};

template <>
struct dimensionality <int3> {
	static const int value = 1;
};

template <>
struct dimensionality <int4> {
	static const int value = 1;
};

template <>
struct dimensionality <uint1> {
	static const int value = 1;
};

template <>
struct dimensionality <uint2> {
	static const int value = 1;
};

template <>
struct dimensionality <uint3> {
	static const int value = 1;
};

template <>
struct dimensionality <uint4> {
	static const int value = 1;
};

template <>
struct dimensionality <long1> {
	static const int value = 1;
};

template <>
struct dimensionality <long2> {
	static const int value = 1;
};

template <>
struct dimensionality <long3> {
	static const int value = 1;
};

template <>
struct dimensionality <long4> {
	static const int value = 1;
};

template <>
struct dimensionality <ulong1> {
	static const int value = 1;
};

template <>
struct dimensionality <ulong2> {
	static const int value = 1;
};

template <>
struct dimensionality <ulong3> {
	static const int value = 1;
};

template <>
struct dimensionality <ulong4> {
	static const int value = 1;
};

template <>
struct dimensionality <longlong1> {
	static const int value = 1;
};

template <>
struct dimensionality <longlong2> {
	static const int value = 1;
};

template <>
struct dimensionality <longlong3> {
	static const int value = 1;
};

template <>
struct dimensionality <longlong4> {
	static const int value = 1;
};

template <>
struct dimensionality <ulonglong1> {
	static const int value = 1;
};

template <>
struct dimensionality <ulonglong2> {
	static const int value = 1;
};

template <>
struct dimensionality <ulonglong3> {
	static const int value = 1;
};

template <>
struct dimensionality <ulonglong4> {
	static const int value = 1;
};

template <>
struct dimensionality <float1> {
	static const int value = 1;
};

template <>
struct dimensionality <float2> {
	static const int value = 1;
};

template <>
struct dimensionality <float3> {
	static const int value = 1;
};

template <>
struct dimensionality <float4> {
	static const int value = 1;
};

template <>
struct dimensionality <double1> {
	static const int value = 1;
};

template <>
struct dimensionality <double2> {
	static const int value = 1;
};

template <>
struct dimensionality <double3> {
	static const int value = 1;
};

template <>
struct dimensionality <double4> {
	static const int value = 1;
};

} /* namespace cutils */
} /* namespace lcg */

__host__ __device__
inline dim3 operator+(const dim3 &u, const dim3 &v) {
	return dim3(u.x + v.x, u.y + v.y, u.z + v.z);
}

__host__ __device__
inline int2 operator+(const int2 &u, const int2 &v) {
	return make_int2(u.x + v.x, u.y + v.y);
}

__host__ __device__
inline ulonglong2 operator+(const ulonglong2 &u, const int2 &v) {
	return make_ulonglong2(u.x + v.x, u.y + v.y);
}

__host__ __device__
inline ulonglong2 operator+(const int2 &u, const ulonglong2 &v) {
	return v + u;
}

__host__ __device__
inline ulonglong2 operator+(const ulonglong2 &u, const ulonglong2 &v) {
	return make_ulonglong2(u.x + v.x, u.y + v.y);
}

__host__ __device__
inline ulonglong3 operator+(const ulonglong3 &u, const ulonglong3 &v) {
	return make_ulonglong3(u.x + v.x, u.y + v.y, u.z + v.z);
}

__host__ __device__
inline dim3 operator-(const dim3 &u, const dim3 &v) {
	return dim3(u.x - v.x, u.y - v.y, u.z - v.z);
}

__host__ __device__
inline int2 operator-(const int2 &u, const int2 &v) {
	return make_int2(u.x - v.x, u.y - v.y);
}

__host__ __device__
inline ulonglong2 operator-(const ulonglong2 &u, const ulonglong2 &v) {
	return make_ulonglong2(u.x - v.x, u.y - v.y);
}

__host__ __device__
inline ulonglong3 operator-(const ulonglong3 &u, const ulonglong3 &v) {
	return make_ulonglong3(u.x - v.x, u.y - v.y, u.z - v.z);
}

__host__ __device__
inline int2 operator*(const int2 &v, int k) {
	return make_int2(k * v.x, k * v.y);
}

__host__ __device__
inline int2 operator*(int k , const int2 &v) {
	return v * k;
}

__host__ __device__
inline int operator*(const int2 &u, const int2 &v) {
	return u.x * v.x + u.y * v.y;
}

__host__ __device__
inline ulonglong2 operator*(const ulonglong2 &v, const float &k) {
	return make_ulonglong2(k * v.x, k * v.y);
}

__host__ __device__
inline ulonglong2 operator*(const float &k, const ulonglong2 &v) {
	return v * k;
}

__host__ __device__
inline unsigned long long operator*(const ulonglong2 &u, const ulonglong2 &v) {
	return u.x * v.x + u.y * v.y;
}

__host__ __device__
inline float operator*(const float2 &u, const float2 &v) {
	return u.x * v.x + u.y * v.y;
}

__host__ __device__
inline ulonglong3 operator*(const ulonglong3 &v, int k) {
	return make_ulonglong3(k * v.x, k * v.y, k * v.z);
}

__host__ __device__
inline ulonglong3 operator*(int k , const ulonglong3 &v) {
	return v * k;
}

__host__ __device__
inline ulonglong3 operator*(const ulonglong3 &v, const float &k) {
	return make_ulonglong3(k * v.x, k * v.y, k * v.z);
}

__host__ __device__
inline ulonglong3 operator*(const float &k, const ulonglong3 &v) {
	return v * k;
}

__host__ __device__
inline float operator*(const ulonglong3 &u, const ulonglong3 &v) {
	return u.x * v.x + u.y * v.y + u.z * v.z;
}

__host__ __device__
inline dim3 operator/(const dim3 &v, const int &k) {
	return dim3(v.x / k , v.y / k, v.z / k);
}

__host__ __device__
inline ulonglong2 operator/(const int2 &u, const float &k) {
	return make_ulonglong2(u.x / k, u.y / k);
}

__host__ __device__
inline ulonglong2 operator/(const ulonglong2 &v, const float &k) {
	return make_ulonglong2(v.x / k, v.y / k);
}

__host__ __device__
inline float2 operator/(const float2 &v, const float &k) {
	return make_float2(v.x / k, v.y / k);
}

__host__ __device__
inline ulonglong3 operator/(const ulonglong3 &u, const float &k) {
	return make_ulonglong3(u.x / k, u.y / k, u.z / k);
}

__host__ __device__
inline bool operator==(const ulonglong2 &u, const ulonglong2 &v) {
	return u.x == v.x and u.y == v.y;
}

__host__ __device__
inline bool operator==(const ulonglong3 &u, const ulonglong3 &v) {
	return u.x == v.x and u.y == v.y and u.z == v.z;
}

__host__ __device__
inline bool operator!=(const int2 &u, const int2 &v) {
	return u.x != v.x and u.y != v.y;
}

__host__ __device__
inline bool operator!=(const ulonglong3 &u, const ulonglong3 &v) {
	return u.x != v.x and u.y != v.y and u.z != v.z;
}

inline std::ostream& operator<<(std::ostream &os, const ulonglong2 &vec) {
	return os << "(" << vec.x << ", " << vec.y << ")";
}

inline std::ostream& operator<<(std::ostream &os, const ulonglong3 &vec) {
	return os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
}

inline std::ostream& operator<<(std::ostream &os, const int4 &vec) {
	return os << "(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
}

#endif /* LCG_CUCCL_META_MAKE_VECTOR_H_ */
