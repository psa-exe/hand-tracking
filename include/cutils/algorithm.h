#ifndef LCG_CUTILS_ALGORITHM_H_
#define LCG_CUTILS_ALGORITHM_H_

namespace lcg {
namespace cutils {

template <class Type>
__device__ inline
void swap(Type &a, Type &b) {
	Type c(a);
	a = b;
	b = c;
}

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_ALGORITHM_H_ */
