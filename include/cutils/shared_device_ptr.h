#ifndef LCG_CUDAUTILS_SHARED_PTR_H_
#define LCG_CUDAUTILS_SHARED_PTR_H_

#include <cutils/cuda_exception.h>

namespace lcg {
namespace cutils {

enum MemoryType {
	MEMORY_DEVICE,
	MEMORY_PINNED
};

template <class Type>
class shared_kernel_ptr;

template<typename Type>
class shared_device_ptr {
	friend class shared_kernel_ptr<Type>;

private:
	struct reference_count {
		size_t count;

		__host__ __device__
		reference_count() :
				count(1) {
		}
	};

	Type *data;
	reference_count *reference;
	MemoryType type;

public:
	shared_device_ptr(MemoryType type, size_t length = 1) :
			data(NULL), reference(new reference_count), type(type) {
		switch(type) {
		case MEMORY_DEVICE:
			CUDA_CHECK( cudaMalloc((void**)&data, length * sizeof(Type)) );
			break;

		case MEMORY_PINNED:
			CUDA_CHECK( cudaMallocHost((void**)&data, length * sizeof(Type)) );
			break;
		}
	}

	shared_device_ptr(MemoryType type, Type* data) :
			data(data), reference(new reference_count), type(type) {
		reference->count++;
	}

	shared_device_ptr(const shared_device_ptr<Type>& other) :
			data(other.data), reference(other.reference), type(other.type) {
		reference->count++;
	}

	~shared_device_ptr() {
		erase();
	}

	shared_device_ptr<Type>& operator=(const shared_device_ptr<Type>& other) {
		if (this != &other) {
			erase();
			data = other.data;
			reference = other.reference;
			type = other.type;
			reference->count++;
		}
		return *this;
	}

	Type& operator*() {
		return *data;
	}

	const Type& operator*() const {
		return *data;
	}

	Type* operator->() {
		return data;
	}

	const Type* operator->() const {
		return data;
	}

	operator Type*() {
		return data;
	}

	operator const Type*() const {
		return data;
	}

private:
	void erase() {
		if (--reference->count == 0) {
			switch (type) {
			case MEMORY_DEVICE:
				CUDA_CHECK( cudaFree(data) );
				break;

			case MEMORY_PINNED:
				CUDA_CHECK( cudaFreeHost(data) );
				break;
			}

			delete reference;
		}
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_SHARED_PTR_H_ */
