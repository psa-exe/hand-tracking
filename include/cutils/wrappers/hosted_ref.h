#ifndef LCG_CUDAUTILS_WRAPPERS_HOSTED_REF_H_
#define LCG_CUDAUTILS_WRAPPERS_HOSTED_REF_H_

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template <class Type>
class kernel_ref;

template<class Type>
class hosted_ref {
	Type *pointer;

public:
	hosted_ref(Type &data) :
			pointer(&data) {
	}

	hosted_ref(Type *data) :
			pointer(data) {
	}

	hosted_ref(hosted<Type> &other) :
			pointer(&other) {
	}

	hosted_ref(const hosted_ref &other) :
			pointer(&other) {
	}

	hosted_ref(pinned<Type> &other) :
			pointer(&other) {
	}

	hosted_ref& operator= (const Type &other) {
		*pointer = other;
		return *this;
	}

	hosted_ref& operator= (const hosted_ref &other) {
		*pointer = other;
		return *this;
	}

	hosted_ref& operator= (const hosted<Type> &other) {
		*pointer = other;
		return *this;
	}

	hosted_ref& operator= (const pinned<Type> &other) {
		*pointer = other;
		return *this;
	}

	hosted_ref& operator= (const device<Type> &other) {
		CUDA_CHECK( cudaMemcpy(pointer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	hosted_ref& operator= (const device_ref<Type> &other) {
		CUDA_CHECK( cudaMemcpy(pointer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	operator Type&() {
		return *pointer;
	}

	operator const Type&() const {
		return *pointer;
	}

	Type& operator*() {
		return *pointer;
	}

	const Type& operator*() const {
		return *pointer;
	}

	Type* operator->() {
		return pointer;
	}

	const Type* operator->() const {
		return pointer;
	}

	Type* operator&() {
		return pointer;
	}

	const Type* operator&() const {
		return pointer;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_HOSTED_REF_H_ */
