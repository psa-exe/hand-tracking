#ifndef LCG_CUDAUTILS_WRAPPERS_KERNEL_REF_H_
#define LCG_CUDAUTILS_WRAPPERS_KERNEL_REF_H_

#include <cutils/shared_kernel_ptr.h>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template <class Type>
class kernel_ref {
	shared_kernel_ptr<Type> gpupointer;

public:
	__host__ __device__
	kernel_ref(Type *data) :
			gpupointer(data) {
	}

	__host__
	kernel_ref(const pinned<Type> &other) :
			gpupointer(other.buffer) {
	}

	__host__
	kernel_ref(const device<Type> &other) :
			gpupointer(other.gpupointer) {
	}

	__host__
	kernel_ref(const device_ref<Type> &other) :
			gpupointer(other.gpupointer) {
	}

	__host__ __device__
	kernel_ref(const kernel_ref &other) :
			gpupointer(other.gpupointer) {
	}

	__device__
	kernel_ref& operator=(const Type &other) {
		*gpupointer = other;
		return *this;
	}

	__device__
	kernel_ref& operator=(const kernel_ref<Type> &other) {
		*gpupointer = other;
		return *this;
	}

	__device__
	operator Type&() {
		return *gpupointer;
	}

	__device__
	operator const Type&() const {
		return *gpupointer;
	}

	__device__
	Type* operator->() {
		return gpupointer;
	}

	__device__
	const Type* operator->() const {
		return gpupointer;
	}

	__device__
	Type* operator&() {
		return gpupointer;
	}

	__device__
	const Type* operator&() const {
		return gpupointer;
	}
};

template <class Type>
class kernel_ref<Type*> {
	size_t length;
	shared_kernel_ptr<Type> gpupointer;

public:
//	template <int N>
//	kernel_ref(Type (&data)[N]) :
//			length(N), gpupointer(data) {
//	}
//
//	kernel_ref(Type *data, size_t length) :
//			length(length), gpupointer(data) {
//	}
//
	__host__
	kernel_ref(const pinned<Type*> &other) :
			gpupointer(other.buffer) {
	}

	__host__
	kernel_ref(const device<Type*> &other) :
			length(other.length), gpupointer(other.gpupointer) {
	}

	__host__
	kernel_ref(const device_ref<Type*> &other) :
			length(other.length), gpupointer(other.gpupointer) {
	}


	__device__
	kernel_ref(const kernel_ref &other) :
			length(other.length), gpupointer(other.gpupointer) {
	}

	__device__
	operator Type*(){
		return gpupointer;
	}

	__device__
	operator const Type*() const {
		return gpupointer;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_KERNEL_REF_H_ */
