#ifndef LCG_CUDAUTILS_WRAPPERS_HOSTED_H
#define LCG_CUDAUTILS_WRAPPERS_HOSTED_H

#include <stdexcept>

#include <cutils/cuda_exception.h>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template<class Type>
class hosted {
	Type instance;

public:
	hosted() :
			instance() {
	}

	hosted(const Type &data) :
			instance(data) {
	}

	hosted(const hosted &other) :
			instance(other) {
	}

	hosted(const hosted_ref<Type> &other) :
			instance(other) {
	}

	hosted(const pinned<Type> &other) :
			instance(other) {
	}

	hosted(const device<Type> &other) :
			instance() {
		CUDA_CHECK( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted(const device_ref<Type> &other) :
			instance() {
		CUDA_CHECK( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted& operator= (const Type &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const hosted &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const hosted_ref<Type> &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const pinned<Type> &other) {
		instance = other;
		return *this;
	}

	hosted& operator= (const device<Type> &other) {
		CUDA_CHECK( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	hosted& operator= (const device_ref<Type> &other) {
		CUDA_CHECK( cudaMemcpy(&instance, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	//TODO: Experiment with these generic typecasts to other types
//	template <class OtherType>
//	operator OtherType() {
//		return instance;
//	}
//
//	template <class OtherType>
//	operator const OtherType() const {
//		return instance;
//	}

	operator Type&() {
		return instance;
	}

	operator const Type&() const {
		return instance;
	}

	Type& operator*() {
		return instance;
	}

	const Type& operator*() const {
		return instance;
	}

	Type* operator->() {
		return &instance;
	}

	const Type* operator->() const {
		return &instance;
	}

	Type* operator&() {
		return &instance;
	}

	const Type* operator&() const {
		return &instance;
	}
};

template <class Type>
class hosted<Type*> {
	friend class device<Type*>;
	friend class device_ref<Type*>;

	size_t length;
	Type *array;
public:
	hosted(size_t length) :
			length(length), array(new Type[length]) {
	}

	template <size_t N>
	hosted(const Type (&data)[N]) :
			length(N), array(new Type[N]) {
		memcpy(array, data, length * sizeof(Type));
	}

	//TODO: Missing constructor
//	hosted(const pinned<Type*> &other) :
//			length(other.length), array(new Type[other.length]) {
//		memcpy(array, other, length * sizeof(Type));
//	}

	hosted(const device<Type*> &other) :
			length(other.length), array(new Type[other.length]) {
		CUDA_CHECK( cudaMemcpy(array, other, length * sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	hosted(const device_ref<Type*> &other) :
			length(other.length), array(new Type[other.length]) {
		CUDA_CHECK( cudaMemcpy(array, other, length * sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	operator Type*() {
		return array;
	}

	operator const Type*() const {
		return array;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_HOSTED_H */
