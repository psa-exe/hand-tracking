#ifndef RECORDER_H_
#define RECORDER_H_

#include <msc/Module.h>
#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

class Recorder :
		public Module,
		public t3::SingletonClass<Recorder> {

	friend SingletonBase;

	Recorder();

	size_t frameCount;

public:
	virtual void run();
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* RECORDER_H_ */
