#ifndef VISUALIZER_H_
#define VISUALIZER_H_

#include <GL/glew.h> //TODO: Just here to prevent that "gl.h" included before "glew.h" bullshit
#include <cutils/wrappers.h>
#include <msc/types.h>
#include <msc/Module.h>
#include <opencv2/core/core.hpp>
#include <t3/SingletonClass.h>
#include <thrust/host_vector.h>
#include <tucano/framebuffer.hpp>

namespace lcg {
namespace msc {

class Visualizer :
		public Module,
		public t3::SingletonClass<Visualizer> {

	friend SingletonBase;

	Visualizer();
	void updateBuffers();

	int min_depth, max_depth;

	cutils::hosted<ObservedSkinImage> observedSkinInput;
	cutils::hosted<ObservedDepthImage> processedDepthInput;
	cutils::hosted<ObservedVideoImage> processedVideoInput;
	cutils::hosted<cuccl::full_label_image> blobLabelsInput; // TODO: Aren't this and observeSkinInput representing the same information?
	thrust::host_vector<RenderedDepthImage> renderedDepthInputs;

	Tucano::Framebuffer fbo;

	cv::Mat processedDepthImage;
	cv::Mat processedVideoImage;
	cv::Mat observedSkinImage;
	cv::Mat blobLabelsImage;
	cv::Mat bestHandImage;

	cv::Mat processedDepthImageRgb;
	cv::Mat blobLabelsImageRgb;

public:
	cv::Mat montageImageOutput;

	virtual void run();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* VISUALIZER_H_ */
