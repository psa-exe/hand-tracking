#ifndef STDX_VECTOR_H_
#define STDX_VECTOR_H_

#include <vector>

namespace stdx {

template <
	typename Value,
	class Alloc = std::allocator<Value>
> class vector : public std::vector<Value, Alloc> {
public:

	using std::vector<Value, Alloc>::vector;

	vector&
	append(const Value &value) {
		this->push_back(value);
		return *this;
	}

	template <typename Collection>
	vector&
	extend(const Collection &extra) {
		this->insert(this->begin(), extra.begin(), extra.end());
		return *this;
	}

	vector&
	operator<<(const Value &value) {
		this->push_back(value);
		return *this;
	}

	vector&
	operator<<(Value &&value) {
		this->push_back(value);
		return *this;
	}

	vector&
	operator,(const Value &value) {
		this->push_back(value);
		return *this;
	}

	vector&
	operator,(Value &&value) {
		this->push_back(value);
		return *this;
	}
};

} /* namespace stdx */

#endif /* STDX_VECTOR_H_ */
