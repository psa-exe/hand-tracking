#ifndef STDX_SEMAPHORE_H_
#define STDX_SEMAPHORE_H_

#include <condition_variable>
#include <mutex>

namespace stdx {

class semaphore {
public:
    semaphore (int count = 0);

    void signal();

    void wait();

private:
    std::mutex mtx;
    std::condition_variable cv;
    int count;
};

} /* namespace stdx */

#endif /* STDX_SEMAPHORE_H_ */
