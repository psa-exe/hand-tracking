#ifndef STDX_CUDASUPPORT_H_
#define STDX_CUDASUPPORT_H_

#ifdef __CUDA_ARCH__
	#define HOST_IF_CUDA        __host__
	#define DEVICE_IF_CUDA               __device__
	#define HOST_DEVICE_IF_CUDA __host__ __device__
#else
	#define HOST_IF_CUDA
	#define DEVICE_IF_CUDA
	#define HOST_DEVICE_IF_CUDA
#endif

#endif /* STDX_CUDASUPPORT_H_ */
