#ifndef STDX_DEFAULT_MAP_H_
#define STDX_DEFAULT_MAP_H_

#include <stdx/map.h>

namespace stdx {

template <
	typename Key,
	typename Value,
	class Compare = std::less<Key>,
	class Alloc = std::allocator<std::pair<const Key, Value>>>
class default_map : public map<Key, Value, Compare, Alloc> {
public:

	using map<Key, Value, Compare, Alloc>::map;

	default_map() :
			_default_value(Value()) {
	}

	default_map(const Value &default_value) :
			_default_value(default_value) {
	}

	Value default_value() const {
		return _default_value;
	}

	void default_value(const Value &new_default) {
		_default_value = new_default;
	}

	const Value& operator[](const Key& key) const {
	    auto search = this->find(key);

	    if (search == this->end())
	    	return _default_value;
	    else
	    	return search->second;
	}

	Value& operator[](const Key& key) {
		auto search = this->find(key);

		if (search == this->end())
			this->insert(std::pair<Key, Value>(key, _default_value));

		search = this->find(key);

		return search->second;
	}

private:
	Value _default_value;
};

}

#endif /* STDX_DEFAULT_MAP_H_ */
