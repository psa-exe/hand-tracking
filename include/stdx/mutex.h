#ifndef STDX_MUTEX_H_
#define STDX_MUTEX_H_

#include <functional>
#include <mutex>

namespace stdx {

class mutex : public std::mutex {
public:

	void critical_section(std::function<void(void)> operations);
};

} /* namespace stdx */

#endif /* STDX_MUTEX_H_ */
