#ifndef KINECTSPECS_H_
#define KINECTSPECS_H_

#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

struct KinectSpecs :
		t3::SingletonClass<KinectSpecs> {

	friend SingletonBase;

	const static int width = 640, height = 480;

	// TODO: Cannot be typed ObservedDepthType because would cause circular inclusion of msc/types.h
	const static int noDepth = 0;
	const static int minPossibleDepth = 1;
	const static int maxPossibleDepth = 8192;

	constexpr static int pixels() {
		return width * height;
	}

	// TODO: Cannot be typed ObservedDepthType because would cause circular inclusion of msc/types.h
	constexpr static int possibleDepthRange() {
		return maxPossibleDepth - minPossibleDepth;
	}

	float aspectRatio;
	float fovy;
	float near, far;

private:
	KinectSpecs();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* KINECTSPECS_H_ */
