#ifndef OBJECTIVEFUNCTION_H_
#define OBJECTIVEFUNCTION_H_

#include <cutils/types.h>
#include <msc/types.h>

namespace lcg {
namespace msc {

struct FunctionInputs {
	ObservedDepthType od;
	ObservedSkinType  os;
	RenderedDepthType rd;
};

struct FunctionOutputs {
	EnergyImage *depthDifferenceOutputs;
	EnergyImage *depthMatchesOutputs; //TODO: This is only here for debugging, so output could be optional
	EnergyImage *skinAndMatchOutputs;
	EnergyImage *skinOrMatchOutputs;
};

struct ObjectiveFunction {
	__device__
	virtual ~ObjectiveFunction();

	//TODO: Change this for auxiliary managemnt class with static instace to provide for automatic initialization/shutdown
	static void initialize();

	__device__
	virtual void operator()(int n, cutils::vec2i p,	FunctionInputs input, FunctionOutputs output, bool writeRenderMatches) = 0;
};

struct OikonomidisFunctionParams {
	ObservedDepthType depthThreshold1;
	ObservedDepthType depthThreshold2;
	ObservedDepthType nearDepthClip;
	ObservedDepthType farDepthClip;
};

struct ModifiedFunctionParams {
	ObservedDepthType depthThreshold1;
	ObservedDepthType depthThreshold2;
	ObservedDepthType maxDepthDifference;
	ObservedDepthType nearDepthClip;
	ObservedDepthType farDepthClip;
};

struct OikonomidisFunction {
	__device__
	void operator()(int n, cutils::vec2i p, FunctionInputs in, FunctionOutputs out, bool writeRenderMatches);
};

struct ModifiedFunction {
	__device__
	void operator()(int n, cutils::vec2i p, FunctionInputs in, FunctionOutputs out, bool writeRenderMatches);
};

}
}

#endif /* OBJECTIVEFUNCTION_H_ */
