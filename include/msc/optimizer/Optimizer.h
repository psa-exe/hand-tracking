/*
 * Optimizer.h
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#ifndef ABSTRACTPSO_H_
#define ABSTRACTPSO_H_

#include <msc/HandPose.h>
#include <msc/Module.h>
#include <msc/types.h>
#include <thrust/device_vector.h>

namespace lcg {
namespace msc {

struct Optimizer : Module {
	Optimizer();
	virtual ~Optimizer();
	virtual float constrictionFactor() const;
	virtual void advanceSwarm() = 0;
	virtual void disturbSwarm() = 0;
	virtual void evaluateSwarm() = 0;
	virtual void initializeSwarm();
	virtual void run();

	thrust::device_vector<EnergyImage> depthDifferenceOutputs; // min(|o_d - r_d|, d_M)
	thrust::device_vector<EnergyImage>   renderMatchesOutputs; // r_m
	thrust::device_vector<EnergyImage>   skinOrMatchesOutputs; // os_s || r_m
	thrust::device_vector<EnergyImage>  skinAndMatchesOutputs; // os_s && r_m

	thrust::host_vector<int>           indexByRankOutputs;
	thrust::host_vector<float>        scoresOutputs;
	thrust::host_vector<HandPose>  particlesOutputs;
	thrust::host_vector<HandPose> velocitiesOutputs;
	thrust::host_vector<HandPose>  bestCasesOutputs;

	float bestScoreEverOutput;
	HandPose bestCaseEverOutput;

private:
	bool firstRun;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* ABSTRACTPSO_H_ */
