#ifndef LCG_MSC_MODIFIEDPSO_H_
#define LCG_MSC_MODIFIEDPSO_H_

#include <cutils/wrappers.h>
#include <msc/optimizer/Optimizer.h>
#include <msc/types.h>

#include <random>

namespace lcg {
namespace msc {

struct ClassicOptimization : Optimizer {
	ClassicOptimization();

	virtual void advanceSwarm();
	virtual void disturbSwarm();
	virtual void evaluateSwarm();

private:
	EnergyImage *depthDifferenceOutputsPointer;
	EnergyImage *skinAndMatchesOutputsPointer;
	EnergyImage *skinOrMatchesOutputsPointer;

	std::default_random_engine randomGenerator;
	std::uniform_real_distribution<float> abductionDistribution;
	std::uniform_real_distribution<float> flexionDistribution;
	std::uniform_real_distribution<float> influenceDistribution;
	std::uniform_int_distribution<int> fingerJointDistribution;
	std::function<float(void)> randomInfluence;
	std::function<int(void)> randomFingerJoint;
	std::function<float(void)> randomAbduction;
	std::function<float(void)> randomFlexion;
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* LCG_MSC_MODIFIEDPSO_H_ */
