#ifndef PSODEBUGGER_H_
#define PSODEBUGGER_H_

#include <msc/types.h>
#include <opencv2/core/core.hpp>
#include <stdx/map.h>
#include <stdx/string.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

namespace lcg {
namespace msc {

class PSODebugger {
public:
	PSODebugger();
	void run();

private:
	void convertAndDisplayImage(thrust::host_vector<EnergyImage> &image, int index, const stdx::string &baseTitle, double alpha = 255);
	float depthDifferenceMultiplier() const;
	void displayImages();
	void downloadBuffers();

	int cols() const;
	int rows() const;
	cv::Size displaySize() const;
	cv::Size cellSize() const;

	stdx::map<stdx::string, cv::Mat> displays;
	thrust::host_vector<EnergyImage> depthDifferenceVector; // min(|o_d - r_d|, d_M)
	thrust::host_vector<EnergyImage> renderMatchesVector;   // r_m
	thrust::host_vector<EnergyImage> skinOrMatchesVector;   // os_s || r_m
	thrust::host_vector<EnergyImage> skinAndMatchesVector;  // os_s && r_m
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PSODEBUGGER_H_ */
