#ifndef ABSTRACTCAMERAMODULE_H_
#define ABSTRACTCAMERAMODULE_H_

#include <cutils/wrappers.h>
#include <msc/Module.h>
#include <msc/types.h>
#include <opencv2/core/core.hpp>

namespace lcg {
namespace msc {

class AbstractCamera :
		public Module {

	friend class CameraFactory;

public:
	cutils::hosted<ObservedVideoImage> observedVideoOutput;
	cutils::hosted<ObservedDepthImage> observedDepthOutput;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* ABSTRACTCAMERAMODULE_H_ */
