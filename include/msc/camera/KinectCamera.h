#ifndef KINECTCAMERA_H_
#define KINECTCAMERA_H_

#include "AbstractCamera.h"

namespace lcg {
namespace msc {

class KinectCamera:
		public AbstractCamera {

	friend class CameraFactory;

public:
	virtual ~KinectCamera();
	virtual void run();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* KINECTCAMERA_H_ */
