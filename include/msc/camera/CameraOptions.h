#ifndef CAMERAOPTIONS_H_
#define CAMERAOPTIONS_H_

#include <msc/OptionsSingleton.h>
#include <stdx/string.h>

namespace lcg {
namespace msc {

enum class CameraModeEnum { PLAYBACK, KINECT };

class CameraOptions :
		public OptionsSingleton<CameraOptions> {

	friend SingletonBase;

private:
	std::string cameraModeString;

public:

	CameraModeEnum cameraMode;

	virtual void registerOptions();
	virtual void validateOptions();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* CAMERAOPTIONS_H_ */
