#ifndef KINECTCAMERAOPTIONS_H_
#define KINECTCAMERAOPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

class PlaybackCameraOptions :
		public OptionsSingleton<PlaybackCameraOptions> {

	friend SingletonBase;

public:

	virtual void registerOptions();
	virtual void validateOptions();

	bool loop;
	bool startPaused;
	std::string depthDirectory;
	std::string videoDirectory;
	std::string depthWildcard;
	std::string videoWildcard;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* KINECTCAMERAOPTIONS_H_ */
