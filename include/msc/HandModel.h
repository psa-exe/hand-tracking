#ifndef LCG_MSC_HAND_MODEL_HAND_MODEL_H_
#define LCG_MSC_HAND_MODEL_HAND_MODEL_H_

#include <tucano/camera.hpp>
#include <tucano/mesh.hpp>
#include <tucano/effects/phongshader.hpp>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>

#include <stdx/io_error.h>
#include <stdx/map.h>
#include <stdx/string.h>
#include <stdx/vector.h>

#include <memory>

//TODO: Problem domain constants
const int SPHERES = 22;
const int CYLINDERS = 15;

namespace lcg {
namespace msc {

using namespace Eigen;

// Forward declaration of HandPose class to allow inclusion of this header in the handpose.h header file
class HandPose;

class HandModel {
public:
	enum Side {
		LEFT, RIGHT
	};

	enum Part {
		WRIST            = 0x00,
		PALM             = 0x01,
		PALM_CAP         = 0x02,

		THUMB_BASE       = 0x03, //! This dummy part represents the thumb metacarpal bone, since it should be rendered as a sphere but the rotation center is not the sphere's center.
		THUMB_METACARPAL = 0x04,
		THUMB_JOINT_P    = 0x05,
		THUMB_PHALANX_P  = 0x06,
		THUMB_JOINT_D    = 0x07,
		THUMB_PHALANX_D  = 0x08,
		THUMB_TIP        = 0x09,

		INDEX_JOINT_P    = 0x0A,
		INDEX_PHALANX_P  = 0x0B,
		INDEX_JOINT_M    = 0x0C,
		INDEX_PHALANX_M  = 0x0D,
		INDEX_JOINT_D    = 0x0E,
		INDEX_PHALANX_D  = 0x0F,
		INDEX_TIP        = 0x10,

		MIDDLE_JOINT_P   = 0x11,
		MIDDLE_PHALANX_P = 0x12,
		MIDDLE_JOINT_M   = 0x13,
		MIDDLE_PHALANX_M = 0x14,
		MIDDLE_JOINT_D   = 0x15,
		MIDDLE_PHALANX_D = 0x16,
		MIDDLE_TIP       = 0x17,

		RING_JOINT_P     = 0x18,
		RING_PHALANX_P   = 0x19,
		RING_JOINT_M     = 0x1A,
		RING_PHALANX_M   = 0x1B,
		RING_JOINT_D     = 0x1C,
		RING_PHALANX_D   = 0x1D,
		RING_TIP         = 0x1E,

		PINKY_JOINT_P    = 0x1F,
		PINKY_PHALANX_P  = 0x20,
		PINKY_JOINT_M    = 0x21,
		PINKY_PHALANX_M  = 0x22,
		PINKY_JOINT_D    = 0x23,
		PINKY_PHALANX_D  = 0x24,
		PINKY_TIP        = 0x25,
	};

	static const int MESHES = PINKY_TIP - WRIST + 1;

	static const stdx::vector<Part> cylinder_parts;
	static const stdx::vector<Part> sphere_parts;
	static stdx::map<Part, Eigen::Vector4f> part_colors;

	static const stdx::vector<stdx::vector<Part>> hierarchy;

	HandModel(
		Side side,
		const stdx::string &file_path = "data/hand-model.matrices"
	) throw (stdx::io_error);

	/**
	 * \note This constructor must be kept because of the current_pose attribute, which is a unique_ptr of an incomplete type (HandPose), according to \url{http://stackoverflow.com/questions/9954518/stdunique-ptr-with-an-incomplete-type-wont-compile}
	 */
	~HandModel();

	void apply(const HandPose &pose);
	void setTranslation(const Vector3f &vector);
	void render(const Tucano::Camera& camera, const Tucano::Camera& light, bool invertY = false);

private:

	std::unique_ptr<HandPose> current_pose;
	stdx::vector<Affine3f> local_matrices, pose_matrices, global_matrices;

	Tucano::Shapes::Cylinder cylinder_mesh;
	Tucano::Shapes::Sphere   sphere_mesh;
	Tucano::Shader phong_shader;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* LCG_MSC_HAND_MODEL_HAND_MODEL_H_ */
