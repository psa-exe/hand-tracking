#ifndef PREPROCESSINGSETTINGS_H_
#define PREPROCESSINGSETTINGS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

struct EnvironmentOptions :
		OptionsSingleton<EnvironmentOptions> {

	friend SingletonBase;

	float nearDepthClip, farDepthClip;

protected:
	virtual void registerOptions();
	virtual void validateOptions();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PREPROCESSINGSETTINGS_H_ */
