/*
 * CudaOptions.h
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#ifndef CUDASETTINGS_H_
#define CUDASETTINGS_H_

#include <t3/SingletonClass.h>
#include <vector_types.h>

namespace lcg {
namespace msc {

struct CudaOptions :
		t3::SingletonClass<CudaOptions> {
	friend SingletonBase;

	dim3 defaultBlock() const;
	dim3 defaultGrid() const;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* CUDASETTINGS_H_ */
