/*
 * OptionsSingleton.h
 *
 *  Created on: 14/02/2016
 *      Author: pedro
 */

#ifndef CONFIGURABLESINGLETON_H_
#define CONFIGURABLESINGLETON_H_

#include <t3/SingletonClass.h>

#include <iostream>

namespace lcg {
namespace msc {

struct OptionsSingletonBase {
	virtual ~OptionsSingletonBase();
	virtual void validateOptions() = 0;

protected:
	OptionsSingletonBase();
};

/**
 * \note OptionsSingleton instances should never be used in static
 * initialization contexts or before System::instance().init() has been
 * called, since the registration process takes place during static
 * initialization, in no particular class order.
 *
 * \note Given a subclass of this template, if no instance of this
 * subclass is ever used in some translation unit, then this
 * template's specialization for that subclass will not be created. Hence,
 * the subclass will not get registered or validated.
 */
template <class DerivedType>
struct OptionsSingleton :
		t3::SingletonClass<DerivedType>,
		OptionsSingletonBase {

	virtual void registerOptions() = 0;

protected:

	//TODO: Design would be simplified if this template class defined template methods for adding the options, hiding implementation details from subclasses. An alternative version could take a std::initializer_list of an object describing options
	//template <class ArgType>
//	void addOption(const stdx::string &name, ArgType &var, const ArgType &defaultValue, const stdx::string &desc = "") {
//	}

	OptionsSingleton() {
		&registered; // Blank statement to force specialization of registered static member, enforcing registration during static initialization phase
	}

private:
	static const bool registered;

	bool callRegisterOptions() {
		this->registerOptions();
		return true;
	}
};

template <class DT>
const bool
OptionsSingleton<DT>::registered =
	DT::instance().callRegisterOptions();

} /* namespace msc */
} /* namespace lcg*/

#endif /* CONFIGURABLESINGLETON_H_ */
