#ifndef OPTIMIZERPARAMETERS_H_
#define OPTIMIZERPARAMETERS_H_

#include <msc/HandPose.h>
#include <msc/OptionsSingleton.h>

#include <string>

namespace lcg {
namespace msc {

enum class ObjectiveFunctionEnum {
	CLASSIC, MODIFIED
};

struct PSOOptions :
		OptionsSingleton<PSOOptions> {

	friend SingletonBase;

	bool active;
	int particles, maxGenerations;
	int disturbingInterval;
	float disturbingRatio;
	float disturbanceVariance;
	float energyEpsilon;
	float socialComponent, cognitiveComponent;
	float energyComponent1, energyComponent2, energyComponent3;
	float abductionAmplitude; //TODO: How to determine this angular amplitude? The paper says it's "experimentally determined"
	float flexionAmplitude;   //TODO: How to determine this angular amplitude? The paper says it's "experimentally determined"
	float quaternionAmplitude;//TODO: How to determine this angular amplitude? The paper says it's "experimentally determined"
	float xyAmplitude;        //TODO: How to determine this spatial amplitude? The paper mentions nothing about it
	float zAmplitude;
	std::string initialPosesDir;
	ObjectiveFunctionEnum objectiveFunction;

	int disturbedParticles();
	HandPose lowerPoseBound();
	HandPose upperPoseBound();
	HandPose poseAmplitude();

protected:
	std::string objectiveFunctionName;

	virtual void registerOptions();
	virtual void validateOptions();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* OPTIMIZERPARAMETERS_H_ */
