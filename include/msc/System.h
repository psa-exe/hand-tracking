#ifndef HANDTRACKINGSYSTEM_H_
#define HANDTRACKINGSYSTEM_H_

#include <msc/optimizer/Optimizer.h>
#include <msc/Clock.h>
#include <msc/GlfwRenderTarget.h>
#include <msc/Module.h>
#include <msc/PSODebugger.h>
#include <msc/camera/AbstractCamera.h>
#include <msc/blobtracker/BlobTracker.h>
#include <msc/preprocessor/Preprocessor.h>
#include <msc/skindetector/SkinDetector.h>
#include <stdx/string.h>
#include <stdx/vector.h>
#include <t3/SingletonClass.h>

#include <memory>

namespace lcg {
namespace msc {

class HandPoseRenderer;

struct System :
		t3::SingletonClass<System> {

	friend SingletonBase;

	/**
	 * \brief Initializes the system and all subsystems for use.
	 *
	 * Causes configuration files to be parsed and initializes configurable entities.
	 *
	 * \note This method must be called before any other library component is
	 * used. Particularly, no OptionsSingleton or Factory subclasses should
	 * be used prior to it being called, since their members might not have been
	 * initialized yet.
	 */
	void init(int &argc, char **argv);
	virtual void run();

	/**
	 * Registers extra modules that are run after the core iteration pass
	 * @param module
	 */
	void addModule(Module *module);

	void shutdown();

	//TODO: Change all module variables to reference types and initialize them using the corresponding instance() static methods
	//TODO: Modules should be singleton classes with no deeper hierarchies, but their behavior should be composed using the Strategy or Template patterns
	AbstractCamera *camera;
	Preprocessor *preprocessor;
	SkinDetector *skinDetector;
	BlobTracker *blobTracker;
	Optimizer *pso;
	HandPoseRenderer *renderer;
	PSODebugger *psoDebugger;

	//TODO: This is only here temporarily
	GlfwRenderTarget *renderTarget;

private:
	stdx::vector<Module*> extraModules;
	bool running;

	System();
	~System();
};

/**
 * \brief Convenience shortcut for System::instance()
 * @return
 */
inline
System& system() {
	return System::instance();
}

} /* namespace msc */
} /* namespace lcg */

#include <msc/HandPoseRenderer.h>

#endif /* HANDTRACKINGSYSTEM_H_ */
