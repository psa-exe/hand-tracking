#ifndef LCG_MSC_BLOBTRACKING_H_
#define LCG_MSC_BLOBTRACKING_H_

namespace lcg {
namespace msc {

class BlobTracking {
public:
	static BlobTracking* make();

	virtual ~BlobTracking();
	virtual void track() = 0;
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* LCG_MSC_BLOBTRACKING_H_ */
