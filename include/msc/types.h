#ifndef MSCTYPES_H_
#define MSCTYPES_H_

#include <cuccl/cuccl.h>
#include <cuimg/colors.h>
#include <msc/KinectSpecs.h>
#include <msc/constants.h>

namespace lcg {
namespace msc {

struct blob {
	typedef cuccl::properties::    area_type     area_type;
	typedef cuccl::properties::centroid_type centroid_type;
	typedef cuccl::properties::variance_type variance_type;

	area_type         area;
	centroid_type centroid;
	variance_type variance;
};

template <int N = max_blobs>
struct some_properties {
	typedef cuccl::meta_cuccl<>::      area_type       area_type;
	typedef cuccl::meta_cuccl<>::  centroid_type   centroid_type;
	typedef cuccl::meta_cuccl<>::  variance_type   variance_type;

	cuimg::array_2d<cuccl::full_label, N> labels;
	cuimg::array_2d<area_type, N> areas;
	cuimg::array_2d<centroid_type, N> centroids;
	cuimg::array_2d<variance_type, N> variances;
};

//==============================================================================
// Typedefs
//==============================================================================

typedef cutils::fit_threads<KinectSpecs::width, KinectSpecs::height> kinect_config;

typedef cuimg::RGB24                      RGBVideoType;
typedef cuimg::YUV24                      YUVVideoType;

typedef uint16_t                          ObservedDepthType;
typedef ObservedDepthType                 RenderedDepthType;
typedef cuccl::meta_cuccl<>::segment_type ObservedSkinType;

typedef uint32_t                          EnergyType; //TODO: Write on why this datatype is needed (because of the values that may show up in the calculations

typedef cuimg::image<RGBVideoType, KinectSpecs::width, KinectSpecs::height> ObservedVideoImage;
typedef cuimg::image<YUVVideoType, KinectSpecs::width, KinectSpecs::height> YUVVideoImage;

typedef ObservedVideoImage ObservedVideoImage;

typedef cuimg::image<ObservedDepthType, KinectSpecs::width, KinectSpecs::height> ObservedDepthImage;
typedef cuimg::image<RenderedDepthType, KinectSpecs::width, KinectSpecs::height> RenderedDepthImage;
typedef cuccl::segment_image                                                      ObservedSkinImage;
typedef cuimg::image<EnergyType       , KinectSpecs::width, KinectSpecs::height> EnergyImage;

} /* namespace msc */
} /* namespace lcg */

#endif /* MSCTYPES_H_ */
