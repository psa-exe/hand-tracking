#ifndef PREPROCESSOR_H_
#define PREPROCESSOR_H_

#include <cutils/wrappers.h>
#include <msc/Module.h>
#include <msc/types.h>
#include <t3/SingletonClass.h>
#include <thrust/device_vector.h>

namespace lcg {
namespace msc {

class Preprocessor :
		public t3::SingletonClass<Preprocessor>,
		public Module {

	friend SingletonBase;

	Preprocessor();

	cutils::hosted<ObservedDepthImage> observedDepthInput;
	cutils::hosted<ObservedVideoImage> observedVideoInput;

public:
	virtual void run();

	cutils::device<ObservedDepthImage> processedDepthOutput;
	cutils::device<ObservedVideoImage> processedVideoOutput;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PREPROCESSOR_H_ */
