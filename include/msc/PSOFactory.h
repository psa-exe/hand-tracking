/*
 * PSOFactory.h
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#ifndef PSOFACTORY_H_
#define PSOFACTORY_H_

#include <msc/optimizer/Optimizer.h>

namespace lcg {
namespace msc {

struct PSOFactory {
	static Optimizer *make();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PSOFACTORY_H_ */
