#ifndef SKINMODEL_H_
#define SKINMODEL_H_

#include <cutils/wrappers.h>
#include <msc/types.h>

namespace lcg {
namespace msc {

class SkinModel {
public:
	virtual ~SkinModel();
	virtual void detect() = 0;
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* SKINMODEL_H_ */
