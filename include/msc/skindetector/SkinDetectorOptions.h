#ifndef SKINDETECTOROPTIONS_H_
#define SKINDETECTOROPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

class SkinDetectorOptions :
		public OptionsSingleton<SkinDetectorOptions> {

	friend SingletonBase;

protected:
	virtual void registerOptions();
	virtual void validateOptions();

public:
	float lowSkinThreshold, highSkinThreshold;
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* SKINDETECTOROPTIONS_H_ */
