#ifndef LCG_MSC_HISTOGRAMSKINMODEL_H_
#define LCG_MSC_HISTOGRAMSKINMODEL_H_

#include <cuccl/cuccl.h>
#include <cutils/types.h>
//#include <msc/HistogramModel.h>
#include <msc/types.h>
#include <msc/skindetector/SkinModel.h>
#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

//class HistogramSkinModel :
//		public SkinModel {
//
//	HistogramModel skinDist, nonSkinDist;
//	float prior;
//
//public:
//	static HistogramSkinModel* getDefault();
//
//	__host__ __device__
//	HistogramSkinModel(
//		const HistogramModel &skin_distribution,
//		const HistogramModel &non_skin_distribution,
//		float prior);
//
//	__host__ __device__
//	float evidence(const cutils::vec2f &point) const;
//
//	__host__ __device__
//	float likelihood(const cutils::vec2f &point) const;
//
//	__host__ __device__
//	float posterior(const cutils::vec2f &point) const;
//};

} /* namespace msc */
} /* namespace lcg */

#endif /* LCG_MSC_HISTOGRAMSKINMODEL_H_ */
