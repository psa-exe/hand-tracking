#include <msc/PSODebuggerOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void PSODebuggerOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("PSO.debugger.active",
			po::value<bool>(&active)->default_value(false),
			"[bool] Turns PSO debugging module On/Off\n\r"
		)
		("PSO.debugger.showDepthDifference",
			po::value<bool>(&showDepthDifference)->default_value(false),
			"[bool] display per-particle images for \"depth difference\" PSO outputs\n\r"
		)
		("PSO.debugger.shownDepthDifference.brightnessCorrectionFactor",
			po::value<float>(&depthDifferenceBrightnessCorrectionFactor)->default_value(10),
			"[float] brightness multiplier for \"depth difference\" PSO outputs\n\r"
		)
		("PSO.debugger.showRenderMatches",
			po::value<bool>(&showRenderMatches  )->default_value(false),
			"[bool] display per-particle images for \"render matches  \" PSO outputs\n\r"
		)
		("PSO.debugger.showSkinAndMatches",
			po::value<bool>(&showSkinAndMatches )->default_value(false),
			"[bool] display per-particle images for \"skin and render matches \" PSO outputs\n\r"
		)
		("PSO.debugger.showSkinOrMatches",
			po::value<bool>(&showSkinOrMatches  )->default_value(false),
			"[bool] display per-particle images for \"skin or matches  \" PSO outputs\n\r"
		)
		("PSO.debugger.shownParticles",
			po::value<int>(&shownParticles      )->default_value(1),
			"[int] number of particles to display\n\r"
		);
}

void PSODebuggerOptions::validateOptions() {
	if (depthDifferenceBrightnessCorrectionFactor <= 0) {
		std::cerr << "Fatal configuration error: PSO.debugger.depthDifferenceBrightnessCorrectionFactor cannot be <= 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (shownParticles < 0) {
		std::cerr << "Fatal configuration error: PSO.debugger.shownParticles cannot be < 0" << std::endl;
		exit(EXIT_FAILURE);
	}
}

} /* namespace msc */
} /* namespace lcg */
