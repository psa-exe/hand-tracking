/*
 * PSOFactory.cpp
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#include <msc/optimizer/ClassicOptimization.h>
#include <msc/PSOFactory.h>

namespace lcg {
namespace msc {

Optimizer* PSOFactory::make() {
	return new ClassicOptimization();
}

} /* namespace msc */
} /* namespace lcg */
