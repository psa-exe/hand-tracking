#include <msc/OptionsParser.h>
#include <msc/EnvironmentOptions.h>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <fstream>

#include <iostream>

using namespace std;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

namespace lcg {
namespace msc {

stdx::string OptionsParser::DEFAULT_CONFIG_FILE() {
	return "system.ini";
}

OptionsParser::OptionsParser() :
		configOptions("Allowed options", 80, 70) {

	configFiles << DEFAULT_CONFIG_FILE();
}

void OptionsParser::addConfigurable(OptionsSingletonBase *configurable) {
	configurables << configurable;
}

void OptionsParser::parse(int &argc, char **argv) {
	configOptions.add_options()
		("help,h", "Display this summary of available options");

	po::store(
		po::parse_command_line(argc, argv, configOptions),
		configVariables
	);
	po::notify(configVariables);

	if (configVariables.count("help") > 0) {
		//TODO: Make up a better help message
		cout <<
			"Usage:" << endl <<
			"\t" << argv[0] << " [options]\n\r" << endl;
		configOptions.print(cout);
		exit(EXIT_SUCCESS);
	}

	for (auto file : configFiles) {
		if (not fs::exists(file)) {
			//TODO: Make a Logger class
			cerr << "Configuration file \"" << file << "\" not found (is your CWD correct?)";
			return;
		}

		fs::ifstream configFile(file.string());

		po::store(po::parse_config_file(configFile, configOptions), configVariables);
		po::notify(configVariables);
	}

	for (auto cfg : configurables)
		cfg->validateOptions();
}

} /* namespace msc */
} /* namespace lcg*/
