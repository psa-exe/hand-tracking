#include <GL/glew.h> //TODO: Included here only to prevent "gl.h included before glew.h" bullshit
#include <msc/GlfwRenderTarget.h>>
#include <msc/KinectSpecs.h>

#include <iostream>

namespace lcg {
namespace msc {

GlfwRenderTarget::GlfwRenderTarget() {

    if (!glfwInit()) {
		std::cerr << "Failed to init glfw" << std::endl;
		exit(EXIT_FAILURE);
	}

	// double x dimension for splitview and add margin
	main_window = glfwCreateWindow(
		KinectSpecs::width ,
		KinectSpecs::height,
		"Swarm hypotheses",
		NULL, NULL
	);

	if (!main_window) {
		std::cerr << "Failed to create the GLFW window" << std::endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(main_window);

	//TODO: Will not work until I learn to bind methods to instances and get function pointers for them!
//	glfwSetKeyCallback(main_window, keyCallback);

	glfwSetInputMode(main_window, GLFW_STICKY_KEYS, true);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//TODO: Configure appearance of this guy
//	glfwHideWindow(main_window);

//	Tucano::Misc::initGlew();

//	std::cout << "initialized" << std::endl;
//
//	std::cout << std::endl << std::endl << " ************ usage ************** " << std::endl;
//	std::cout << " R : reset camera" << std::endl;
//	std::cout << "ASWD : move Left, Rigth, Forward, Back" << std::endl;
//	std::cout << "EC : move Up, Down" << std::endl;
//	std::cout << "K : add key point" << std::endl;
//	std::cout << "SPACE : start/stop camera animation" << std::endl;
//	std::cout << ",. : step forward/backward in animation" << std::endl;
//	std::cout << "0 : toggle draw control points" << std::endl;
//	std::cout << "1/2/3 : free camera / trackball camera / path camera" << std::endl;
//	//std::cout << "9 : toggle draw quaternions" << std::endl;
//	std::cout << " ***************** " << std::endl;
}

GlfwRenderTarget::~GlfwRenderTarget() {
	glfwDestroyWindow(main_window);
	glfwTerminate();
}

void GlfwRenderTarget::forceContext() {
	glfwMakeContextCurrent(main_window);
}

GLFWwindow* GlfwRenderTarget::getWindow() {
	return main_window;
}

void GlfwRenderTarget::swapBuffers() {
	glfwSwapBuffers(main_window);
}

void GlfwRenderTarget::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);
}

} /* namespace lcg::msc */
} /* namespace lcg */
