#include <msc/System.h>

#include <msc/GlfwRenderTarget.h>
#include <msc/OptionsParser.h>
#include <msc/PSOFactory.h>
#include <msc/camera/CameraFactory.h>
#include <msc/skindetector/GaussianSkinModel.h>

namespace lcg {
namespace msc {

System::System() :
		running(false) {
}

System::~System() {
	delete psoDebugger;
	delete renderer;
	delete camera;
	delete renderTarget;
}

void System::addModule(Module *module) {
	extraModules << module;
}

void System::init(int &argc, char **argv) {
	//TODO: Insert Welcome message?
	//	OptionsParser::instance().configOptions.print(std::cout);
	OptionsParser::instance().parse(argc, argv);

	camera = CameraFactory::instance().make();
	preprocessor = &Preprocessor::instance(); // TODO: Should this be produced by a factory?
	skinDetector = &SkinDetector::instance();
	blobTracker = &BlobTracker::instance();
	pso = PSOFactory::make();
	psoDebugger = new PSODebugger;

	// TODO: The mess below is because the renderer's HandModel requires glew, which in turn requires an actual window. It'll be gone once a window abstraction layer is implemented and required to system initialization.
	renderTarget = new GlfwRenderTarget();
	Tucano::Misc::initGlew();
	renderer = new HandPoseRenderer;
}

void System::run() {
	running = true;

	while (running) {
		camera->run();
		preprocessor->run();
		skinDetector->run();
		blobTracker->run();
		pso->run();

		//TODO: It seems to me like this should not be here, as it should be the Renderer's role to render to every attached destination
		renderer->renderPosesToWindow(
			pso->particlesOutputs.begin(),
			pso->particlesOutputs.end()
		);
		psoDebugger->run();

		for (auto m : extraModules)
			m->run();
	}
}

void System::shutdown() {
	running = false;
}

} /* namespace msc */
} /* namespace lcg */
