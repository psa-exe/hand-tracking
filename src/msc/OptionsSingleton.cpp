#include <msc/OptionsSingleton.h>
#include <msc/OptionsParser.h>

namespace lcg {
namespace msc {

OptionsSingletonBase::OptionsSingletonBase() {
	OptionsParser::instance().addConfigurable(this);
}

OptionsSingletonBase::~OptionsSingletonBase() {
}

} /* namespace msc */
} /* namespace lcg */
