#include <msc/preprocessor/PreprocessorOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void PreprocessorOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("Preprocessor.medianBlurRadius",
			po::value<int>(&medianBlurRadius)->default_value(2),
			"[int] Radius of median blur filter applied to RGB frames"
		);
}

void PreprocessorOptions::validateOptions() {

}

} /* namespace lcg::msc */
} /* namespace lcg */
