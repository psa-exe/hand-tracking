#include <msc/camera/PlaybackCameraOptions.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>
#include <msc/camera/CameraOptions.h>

namespace lcg {
namespace msc {

namespace fs = boost::filesystem;
namespace po = boost::program_options;

void PlaybackCameraOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("Camera.playback.loop",
			po::value<bool>(&loop)->default_value(false),
			"[bool] Loop frame sequence after last frame\n\r")

		("Camera.playback.depthDirectory",
			po::value<std::string>(&depthDirectory)->default_value(""),
			"[path] Directory to look for depth images\n\r")

		("Camera.playback.videoDirectory",
			po::value<std::string>(&videoDirectory)->default_value(""),
			"[path] Directory to look for video images\n\r")

		("Camera.playback.depthWildcard",
			po::value<std::string>(&depthWildcard)->default_value(""),
			"[string] Wildcard appended to depth directory to search for depth image files\n\r")

		("Camera.playback.startPaused",
			po::value<bool>(&startPaused)->default_value(false),
			"[bool] Pauses the playback camera on the first frame\n\r")

		("Camera.playback.videoWildcard",
			po::value<std::string>(&videoWildcard)->default_value(""),
			"[string] Wildcard appended to video directory to search for video image files\n\r")
		;
}

void PlaybackCameraOptions::validateOptions() {
	if (CameraOptions::instance().cameraMode == CameraModeEnum::PLAYBACK) {
		if (depthDirectory.size() == 0) {
			std::cerr << "Fatal error: Camera.playback.depthDirectory not set" << std::endl;
			exit(EXIT_FAILURE);
		} else if (not fs::exists(depthDirectory)) {
			std::cerr << "Fatal error: Camera.playback.depthDirectory \"" << depthDirectory << "\": no such file or directory" << std::endl;
			exit(EXIT_FAILURE);
		} else if (not fs::is_directory(depthDirectory)) {
			std::cerr << "Fatal error: Camera.playback.depthDirectory \"" << depthDirectory << "\" is not a directory" << std::endl;
			exit(EXIT_FAILURE);
		}

		if (videoDirectory.size() == 0) {
			std::cerr << "Fatal error: Camera.playback.videoDirectory not set" << std::endl;
			exit(EXIT_FAILURE);
		} else if (not fs::exists(videoDirectory)) {
			std::cerr << "Fatal error: Camera.playback.videoDirectory \"" << videoDirectory << "\": no such file or directory" << std::endl;
			exit(EXIT_FAILURE);
		} else if (not fs::is_directory(videoDirectory)) {
			std::cerr << "Fatal error: Camera.playback.videoDirectory \"" << videoDirectory << "\" is not a directory" << std::endl;
			exit(EXIT_FAILURE);
		}

		//TODO: Check for wildcards existence
	}
}

} /* namespace msc */
} /* namespace lcg */
