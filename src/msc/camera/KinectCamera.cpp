#include <msc/camera/KinectCamera.h>


#include <libfreenect_sync.h>
#include <msc/KinectSpecs.h>
#include <msc/types.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace msc {

const cv::Size kinect_size(KinectSpecs::width, KinectSpecs::height);

KinectCamera::~KinectCamera() {
	freenect_sync_stop();
}

void KinectCamera::run() {
	void *depth_data, *video_data;
	uint32_t depth_ts, video_ts;

	freenect_sync_get_depth(&depth_data, &depth_ts, 0, FREENECT_DEPTH_REGISTERED);
	freenect_sync_get_video(&video_data, &video_ts, 0, FREENECT_VIDEO_RGB);

	memcpy(observedDepthOutput->data, depth_data, sizeof(ObservedDepthImage));
	memcpy(observedVideoOutput->data, depth_data, sizeof(ObservedVideoImage));
}

} /* namespace msc */
} /* namespace lcg */
