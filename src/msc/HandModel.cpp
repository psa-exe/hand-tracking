#include <kinect.h>
#include <msc/HandModel.h>
#include <msc/HandPose.h>
#include <msc/KinectSpecs.h>
#include <msc/renderer/RendererOptions.h>
#include <stdx/fstream.h>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>
#include <tucano/effects/phongshader.hpp>

#include <algorithm>
#include <istream>

typedef stdx::string str;

namespace lcg {
namespace msc {

const stdx::vector<HandModel::Part>
HandModel::cylinder_parts = {
	PALM,
	THUMB_PHALANX_P,
	THUMB_PHALANX_D,
	INDEX_PHALANX_P,
	INDEX_PHALANX_M,
	INDEX_PHALANX_D,
	MIDDLE_PHALANX_P,
	MIDDLE_PHALANX_M,
	MIDDLE_PHALANX_D,
	RING_PHALANX_P,
	RING_PHALANX_M,
	RING_PHALANX_D,
	PINKY_PHALANX_P,
	PINKY_PHALANX_M,
	PINKY_PHALANX_D,
};

const stdx::vector<HandModel::Part>
HandModel::sphere_parts = {
	WRIST,
	PALM_CAP,
	THUMB_METACARPAL,
	THUMB_JOINT_P,
	THUMB_JOINT_D,
	THUMB_TIP,
	INDEX_JOINT_P,
	INDEX_JOINT_M,
	INDEX_JOINT_D,
	INDEX_TIP,
	MIDDLE_JOINT_P,
	MIDDLE_JOINT_M,
	MIDDLE_JOINT_D,
	MIDDLE_TIP,
	RING_JOINT_P,
	RING_JOINT_M,
	RING_JOINT_D,
	RING_TIP,
	PINKY_JOINT_P,
	PINKY_JOINT_M,
	PINKY_JOINT_D,
	PINKY_TIP,
};

stdx::map<HandModel::Part, Eigen::Vector4f>
HandModel::part_colors {
	{ WRIST,            Eigen::Vector4f(0.3, 0.3, 0.8, 1.0) },
	{ PALM,             Eigen::Vector4f(0.3, 0.3, 0.8, 1.0) },
	{ PALM_CAP,         Eigen::Vector4f(0.3, 0.3, 0.8, 1.0) },
	{ THUMB_JOINT_D,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ THUMB_JOINT_P,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ THUMB_METACARPAL, Eigen::Vector4f(0.8, 0.3, 0.8, 1.0) },
	{ THUMB_PHALANX_D,  Eigen::Vector4f(0.8, 0.3, 0.8, 1.0) },
	{ THUMB_PHALANX_P,  Eigen::Vector4f(0.8, 0.3, 0.8, 1.0) },
	{ THUMB_TIP,        Eigen::Vector4f(0.8, 0.3, 0.8, 1.0) },
	{ INDEX_JOINT_D,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ INDEX_JOINT_M,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ INDEX_JOINT_P,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ INDEX_PHALANX_D,  Eigen::Vector4f(0.8, 0.3, 0.3, 1.0) },
	{ INDEX_PHALANX_M,  Eigen::Vector4f(0.8, 0.3, 0.3, 1.0) },
	{ INDEX_PHALANX_P,  Eigen::Vector4f(0.8, 0.3, 0.3, 1.0) },
	{ INDEX_TIP,        Eigen::Vector4f(0.8, 0.3, 0.3, 1.0) },
	{ MIDDLE_JOINT_D,   Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ MIDDLE_JOINT_M,   Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ MIDDLE_JOINT_P,   Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ MIDDLE_PHALANX_D, Eigen::Vector4f(0.8, 0.8, 0.3, 1.0) },
	{ MIDDLE_PHALANX_M, Eigen::Vector4f(0.8, 0.8, 0.3, 1.0) },
	{ MIDDLE_PHALANX_P, Eigen::Vector4f(0.8, 0.8, 0.3, 1.0) },
	{ MIDDLE_TIP,       Eigen::Vector4f(0.8, 0.8, 0.3, 1.0) },
	{ RING_JOINT_D,     Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ RING_JOINT_M,     Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ RING_JOINT_P,     Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ RING_PHALANX_D,   Eigen::Vector4f(0.3, 0.8, 0.3, 1.0) },
	{ RING_PHALANX_M,   Eigen::Vector4f(0.3, 0.8, 0.3, 1.0) },
	{ RING_PHALANX_P,   Eigen::Vector4f(0.3, 0.8, 0.3, 1.0) },
	{ RING_TIP,         Eigen::Vector4f(0.3, 0.8, 0.3, 1.0) },
	{ PINKY_JOINT_D,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ PINKY_JOINT_M,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ PINKY_JOINT_P,    Eigen::Vector4f(0.7, 0.7, 0.7, 1.0) },
	{ PINKY_PHALANX_D,  Eigen::Vector4f(0.3, 0.8, 0.8, 1.0) },
	{ PINKY_PHALANX_M,  Eigen::Vector4f(0.3, 0.8, 0.8, 1.0) },
	{ PINKY_PHALANX_P,  Eigen::Vector4f(0.3, 0.8, 0.8, 1.0) },
	{ PINKY_TIP,        Eigen::Vector4f(0.3, 0.8, 0.8, 1.0) }
};

const stdx::vector<stdx::vector<HandModel::Part>>
HandModel::hierarchy = {
	{ PALM, PALM_CAP, THUMB_BASE, INDEX_JOINT_P, MIDDLE_JOINT_P, RING_JOINT_P, PINKY_JOINT_P },
	{                  },
	{                  },
	{ THUMB_METACARPAL, THUMB_JOINT_P },
	{                  },
	{ THUMB_PHALANX_P, THUMB_JOINT_D    },
	{                  },
	{ THUMB_PHALANX_D, THUMB_TIP        },
	{                  },
	{                  },
	{ INDEX_PHALANX_P, INDEX_JOINT_M    },
	{                  },
	{ INDEX_PHALANX_M, INDEX_JOINT_D    },
	{                  },
	{ INDEX_PHALANX_D, INDEX_TIP        },
	{                  },
	{                  },
	{ MIDDLE_PHALANX_P, MIDDLE_JOINT_M   },
	{                  },
	{ MIDDLE_PHALANX_M, MIDDLE_JOINT_D   },
	{                  },
	{ MIDDLE_PHALANX_D, MIDDLE_TIP       },
	{                  },
	{                  },
	{ RING_PHALANX_P, RING_JOINT_M     },
	{                  },
	{ RING_PHALANX_M, RING_JOINT_D     },
	{                  },
	{ RING_PHALANX_D, RING_TIP         },
	{                  },
	{                  },
	{ PINKY_PHALANX_P, PINKY_JOINT_M    },
	{                  },
	{ PINKY_PHALANX_M, PINKY_JOINT_D    },
	{                  },
	{ PINKY_PHALANX_D, PINKY_TIP        },
	{                  },
	{                  },
};

HandModel::HandModel(Side side, const str &file_path) throw(stdx::io_error) :
		current_pose(new HandPose),
		local_matrices (MESHES),
		pose_matrices  (MESHES),
		global_matrices(MESHES),
		cylinder_mesh(1, 2, 8),
		sphere_mesh(2) {

	phong_shader.setShaderName("kinectShader");
	phong_shader.initializeFromStrings(
		stdx::ifstream("shaders/kinect.vert").read(),
		stdx::ifstream("shaders/kinect.frag").read()
	);

	stdx::map<str, Part> parts = {
			{ "wrist",            WRIST            },
			{ "palm",             PALM             },
			{ "palm_cap",         PALM_CAP         },

			{ "thumb_base",       THUMB_BASE       },
			{ "thumb_metacarpal", THUMB_METACARPAL },
			{ "thumb_joint_p",    THUMB_JOINT_P    },
			{ "thumb_phalanx_p",  THUMB_PHALANX_P  },
			{ "thumb_joint_d",    THUMB_JOINT_D    },
			{ "thumb_phalanx_d",  THUMB_PHALANX_D  },
			{ "thumb_tip",        THUMB_TIP        },

			{ "index_joint_p",    INDEX_JOINT_P    },
			{ "index_phalanx_p",  INDEX_PHALANX_P  },
			{ "index_joint_m",    INDEX_JOINT_M    },
			{ "index_phalanx_m",  INDEX_PHALANX_M  },
			{ "index_joint_d",    INDEX_JOINT_D    },
			{ "index_phalanx_d",  INDEX_PHALANX_D  },
			{ "index_tip",        INDEX_TIP        },

			{ "middle_joint_p",   MIDDLE_JOINT_P   },
			{ "middle_phalanx_p", MIDDLE_PHALANX_P },
			{ "middle_joint_m",   MIDDLE_JOINT_M   },
			{ "middle_phalanx_m", MIDDLE_PHALANX_M },
			{ "middle_joint_d",   MIDDLE_JOINT_D   },
			{ "middle_phalanx_d", MIDDLE_PHALANX_D },
			{ "middle_tip",       MIDDLE_TIP       },

			{ "ring_joint_p",     RING_JOINT_P     },
			{ "ring_phalanx_p",   RING_PHALANX_P   },
			{ "ring_joint_m",     RING_JOINT_M     },
			{ "ring_phalanx_m",   RING_PHALANX_M   },
			{ "ring_joint_d",     RING_JOINT_D     },
			{ "ring_phalanx_d",   RING_PHALANX_D   },
			{ "ring_tip",         RING_TIP         },

			{ "pinky_joint_p",    PINKY_JOINT_P    },
			{ "pinky_phalanx_p",  PINKY_PHALANX_P  },
			{ "pinky_joint_m",    PINKY_JOINT_M    },
			{ "pinky_phalanx_m",  PINKY_PHALANX_M  },
			{ "pinky_joint_d",    PINKY_JOINT_D    },
			{ "pinky_phalanx_d",  PINKY_PHALANX_D  },
			{ "pinky_tip",        PINKY_TIP        },
	};

	int line = 1;

	std::ifstream file(file_path, std::ifstream::in);
	file.exceptions(std::ifstream::failbit);

	while (file.good()) {
		try {
			str object_name;
			Matrix4f matrix;

			file >> object_name;
			line++;

			for (int i = 0; i < 16; i++) {
				file >> matrix(i / 4, i % 4);
				if (i % 4 == 3)
					line++;
			}

			if (parts.has(object_name)) {
				local_matrices [parts[object_name]] = Affine3f(matrix);
				pose_matrices  [parts[object_name]] = Affine3f::Identity();
				global_matrices[parts[object_name]] = Affine3f::Identity();
			} else {
				std::cerr << object_name << ": name not found\n";
			}

			line++;

		} catch (std::ifstream::failure &ex) {
			if (file.eof()) {
				break;
			} else {
				std::cerr << "Syntax error in hand model file \"" << file_path << "\"" << ", line " << line << std::endl;
				exit(EXIT_FAILURE);
			}
		}
	}

	apply(HandPose());
}

HandModel::~HandModel() {}

void HandModel::apply(const HandPose &pose) {
	*current_pose = pose;

	//TODO: Better iterate through enumeration
	for (int part = WRIST; part <= PINKY_TIP; part++)
		pose_matrices[part] = pose.matrix(Part(part));

	global_matrices[WRIST] = pose_matrices[WRIST] * local_matrices[WRIST];

	for (int base = WRIST; base < PINKY_TIP; base++)
		for (const Part &child : hierarchy[base])
			global_matrices[child] = global_matrices[base] * local_matrices[child] * pose_matrices[child];
}

void HandModel::render(const Tucano::Camera &camera, const Tucano::Camera &light, bool invertY) {
	Vector4f viewport = camera.getViewport();
	Vector4f color = Vector4f(0.0, 0.48, 1.0, 1.0);

	glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

	glEnable(GL_DEPTH_TEST);

	phong_shader.bind();

	phong_shader.setUniform("yMultiplier", invertY ? 1 : -1);
	phong_shader.setUniform("kinectNear", KinectSpecs::instance().near);
	phong_shader.setUniform("kinectFar" , KinectSpecs::instance().far);
	phong_shader.setUniform("projectionMatrix", camera.getProjectionMatrix());
	phong_shader.setUniform("depthCorrection", RendererOptions::instance().depthCorrection);
	phong_shader.setUniform("lightViewMatrix", light.getViewMatrix());
	phong_shader.setUniform("in_Color", color);

	cylinder_mesh.bindBuffers();
	cylinder_mesh.setAttributeLocation(&phong_shader);
	for (auto cylinder : cylinder_parts) {
		phong_shader.setUniform("modelMatrix", global_matrices[cylinder]);
		phong_shader.setUniform("viewMatrix", camera.getViewMatrix());
		phong_shader.setUniform("in_Color", part_colors[cylinder]);
		cylinder_mesh.renderElements();
	}
	cylinder_mesh.unbindBuffers();

	sphere_mesh.bindBuffers();
	sphere_mesh.setAttributeLocation(&phong_shader);
	for (auto sphere : sphere_parts) {
		phong_shader.setUniform("modelMatrix", global_matrices[sphere]);
		phong_shader.setUniform("viewMatrix", camera.getViewMatrix());
		phong_shader.setUniform("in_Color", part_colors[sphere]);
		sphere_mesh.renderElements();
	}
	sphere_mesh.unbindBuffers();

	phong_shader.unbind();
}

void HandModel::setTranslation(const Vector3f &vector) {
	current_pose->hand_translation() = vector;

	pose_matrices  [WRIST].translation() = vector;
	global_matrices[WRIST].translation() = vector;
}

} /* namespace msc */
} /* namespace lcg */
