#include <msc/skindetector/SkinDetectorOptions.h>

#include <msc/OptionsParser.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void SkinDetectorOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("SkinDetector.lowSkinThreshold",
			po::value<float>(&lowSkinThreshold)->default_value(0.25),
			"[float] Low skin probability threshold"
		)
		("SkinDetector.highSkinThreshold",
			po::value<float>(&highSkinThreshold)->default_value(0.4),
			"[float] High skin probability threshold"
		)
		;
}

void SkinDetectorOptions::validateOptions() {

}

} /* namespace lcg::msc */
} /* namespace lcg */
