#include <msc/skindetector/HistogramSkinModel.h>

#include <cuimg/colors.h>
#include <cuimg/image.h>
#include <cutils/thread_index.h>
#include <cutils/types.h>

#include <msc/System.h>
#include <msc/skindetector/HistogramSkinModel.h>
#include <msc/skindetector/SkinDetectorOptions.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace msc {

//HistogramSkinModel* HistogramSkinModel::getDefault() {
//	return new HistogramSkinModel(
//		HistogramModel(...), HistogramModel(...), 0.1885f
//	);
//}
//
//__host__ __device__
//HistogramSkinModel::HistogramSkinModel(
//	const HistogramModel &skinDist,
//	const HistogramModel &nonSkinDist,
//	float prior) :
//		skinDist(skinDist),
//		nonSkinDist(nonSkinDist),
//		prior(prior) {
//}
//
//__host__ __device__
//float HistogramSkinModel::evidence(const vec2f &point) const {
//	return nonSkinDist.density(point);
//}
//
//__host__ __device__
//float HistogramSkinModel::likelihood(const vec2f &point) const {
//	return skinDist.density(point);
//}
//
//__host__ __device__
//float HistogramSkinModel::posterior(const vec2f &point) const {
//	return likelihood(point) * prior / evidence(point);
//	//TODO: Old code (copied below) should be more numerically robust
////		float prior = 0.1885f;
////
////		float scale = sqrt(
////			color_cov.determinant() /
////			 skin_cov.determinant()
////		);
////
////		float exponent =
////			(uv - color_mean).transposed() * color_cov_inv * (uv - color_mean) -
////			(uv -  skin_mean).transposed() *  skin_cov_inv * (uv -  skin_mean);
////
////		float score = prior * scale * exp(exponent / 2);
//}

} /* namespace lcg::msc */
} /* namespace lcg */
