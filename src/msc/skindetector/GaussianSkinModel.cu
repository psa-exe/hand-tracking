#include <msc/skindetector/GaussianSkinModel.h>

#include <cuimg/colors.h>
#include <cuimg/image.h>
#include <cutils/thread_index.h>
#include <cutils/types.h>

#include <msc/System.h>
#include <msc/skindetector/GaussianSkinModel.h>
#include <msc/skindetector/SkinDetectorOptions.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace msc {

GaussianSkinModel* GaussianSkinModel::getDefault() {
	return new GaussianSkinModel(
		GaussianModel(
			cutils::vec2f{ 149.29f, 112.35f },
			cutils::mat2f{
				{ 87.83f, -67.26f },
				{-67.26f, 108.86f}
			}
		)
	);
}

__host__ __device__
GaussianSkinModel::GaussianSkinModel(
	const GaussianModel &skinDist) :
		skinDist(skinDist) {
}

__host__ __device__
float GaussianSkinModel::posterior(const vec2f &point) const {
	return skinDist.density(point);
	//TODO: Old code (copied below) should be more numerically robust
//		float prior = 0.1885f;
//
//		float scale = sqrt(
//			color_cov.determinant() /
//			 skin_cov.determinant()
//		);
//
//		float exponent =
//			(uv - color_mean).transposed() * color_cov_inv * (uv - color_mean) -
//			(uv -  skin_mean).transposed() *  skin_cov_inv * (uv -  skin_mean);
//
//		float score = prior * scale * exp(exponent / 2);
}


__global__
void augment_skin_with_depth(cuccl::segment_image &skin, ObservedDepthImage &depth) {
	int i = cutils::thread_index().plain();

	//TODO: Insert pixel_segment and pixel_depth classes
	if (skin(i) == cuccl::segment_image::background() and depth(i) != 0)
		skin(i) = segment_depth;
	else if (depth(i) == 0)
		skin(i) = cuccl::segment_image::background();
}

__global__
void filter_skin(YUVVideoImage &yuv_frame, cuccl::segment_image &observed_skin_host_image, GaussianSkinModel model, float low_thr, float high_thr) {
	int i = cutils::thread_index().plain();

	cuimg::YUV24 yuv = yuv_frame(i);
	cutils::vec2f uv = yuv.uv().cast<float>();

	float score = model.posterior(uv);

	if (score >= high_thr)
		observed_skin_host_image(i) = segment_high;
	else if (score >= low_thr)
		observed_skin_host_image(i) = segment_low;
	else
		observed_skin_host_image(i) = cuccl::segment_image::background();
}

void GaussianSkinModel::detect() {
	static cutils::hosted<YUVVideoImage> temporaryYuvHost;
	static cutils::device<YUVVideoImage> temporaryYuvDevice;
	static cutils::hosted<ObservedDepthImage> processedDepthInput;
	static cutils::hosted<ObservedVideoImage> processedVideoInput;

//	processedDepthInput = system().preprocessor->processedDepthOutput;
	processedVideoInput = system().preprocessor->processedVideoOutput;

	Clock::instance().logTime(";detect_in:");

	//TODO: Fixing GPU RGB to YUV conversion will liberate the yuv buffer;
	cv::Mat video_mat_bgr(ObservedDepthImage::cvSize, CV_8UC3, processedVideoInput->data);
	cv::Mat video_mat_yuv(ObservedDepthImage::cvSize, CV_8UC3, temporaryYuvHost->data);
	cv::cvtColor(video_mat_bgr, video_mat_yuv, cv::COLOR_RGB2YUV);

	temporaryYuvDevice = temporaryYuvHost;

	//TODO: Hard-coded launch parameters
	filter_skin<<<kinect_config::grid, kinect_config::block>>>(
		temporaryYuvDevice,
		system().skinDetector->observedSkinOutput,
		*this,
		SkinDetectorOptions::instance().lowSkinThreshold,
		SkinDetectorOptions::instance().highSkinThreshold
	);

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";detect_out:");

//	augment_skin_with_depth<<<launch_config::grid, launch_config::block>>>(
//		system().skinDetector->observedSkinOutput, observedDepthDeviceOutput
//	);
}

} /* namespace lcg::msc */
} /* namespace lcg */
