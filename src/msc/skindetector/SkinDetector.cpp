#include <msc/skindetector/SkinDetector.h>

#include <msc/skindetector/GaussianSkinModel.h>

namespace lcg {
namespace msc {

SkinDetector::SkinDetector() :
		// TODO: There should be a factory that returns the proper SkinModel instance
		skinModel(GaussianSkinModel::getDefault()) {
}

void SkinDetector::run() {
	skinModel->detect();
}

} /* namespace lcg::msc */
} /* namespace lcg */
