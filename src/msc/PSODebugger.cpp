#include <msc/PSODebugger.h>

#include <msc/PSOOptions.h>
#include <msc/PSODebuggerOptions.h>
#include <msc/System.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdx/string.h>

#include <cmath>
#include <sstream>

namespace lcg {
namespace msc {

PSODebugger::PSODebugger():
		depthDifferenceVector(PSOOptions::instance().particles),
		renderMatchesVector  (PSOOptions::instance().particles),
		skinOrMatchesVector  (PSOOptions::instance().particles),
		skinAndMatchesVector (PSOOptions::instance().particles) {
}

void PSODebugger::convertAndDisplayImage(thrust::host_vector<EnergyImage> &images, int rank, const stdx::string &baseTitle, double alpha) {
	if (not displays.has(baseTitle))
		displays[baseTitle] = cv::Mat(displaySize(), CV_8UC3);

	//TODO: Draw best case for each particle in a small frame in the bottom right corner of corresponding cell?
	cv::Mat inputImage(EnergyImage::cvSize, CV_32SC1, &images[system().pso->indexByRankOutputs[rank]].data[0]);
	cv::Mat finalImage(EnergyImage::cvSize, CV_8UC1);
	inputImage.convertTo(finalImage, CV_8U, alpha);
	cv::cvtColor(finalImage, finalImage, cv::COLOR_GRAY2BGR);
	cv::resize(finalImage, finalImage, cellSize());

	float fontScale = 0.7 * (cellSize().height / float(displaySize().height));

	cv::Scalar textColor(0, 0, 255);
	cv::Point tlCorner(10, 0.05 * cellSize().height);
	cv::Point blCorner(10, 0.95 * cellSize().height);

	cv::putText(finalImage,
		stdx::string::collapse("Particle #", rank + 1),
		tlCorner,
		cv::FONT_HERSHEY_SIMPLEX,
		fontScale,
		textColor
	);

	cv::putText(finalImage,
		stdx::string::collapse("Energy: ", system().pso->scoresOutputs[rank]),
		blCorner,
		cv::FONT_HERSHEY_SIMPLEX,
		fontScale,
		textColor
	);

	cv::Rect slot(
		(rank % cols()) * cellSize().width ,
		(rank / cols()) * cellSize().height,
		cellSize().width,
		cellSize().height
	);
	finalImage.copyTo(displays[baseTitle](slot));

	cv::imshow(baseTitle, displays[baseTitle]);
}

float PSODebugger::depthDifferenceMultiplier() const {
	return PSODebuggerOptions::instance().depthDifferenceBrightnessCorrectionFactor * 255.0 / (1000 * KinectSpecs::instance().far);
}

void PSODebugger::displayImages() {
	for (int rank = 0; rank < PSODebuggerOptions::instance().shownParticles; rank++) {
		if (PSODebuggerOptions::instance().showDepthDifference)
			convertAndDisplayImage(depthDifferenceVector, rank, "Depth difference", depthDifferenceMultiplier());

		if (PSODebuggerOptions::instance().showRenderMatches)
			convertAndDisplayImage(renderMatchesVector, rank, "Render matches");

		if (PSODebuggerOptions::instance().showSkinAndMatches)
			convertAndDisplayImage(skinAndMatchesVector, rank, "Skin and render matches");

		if (PSODebuggerOptions::instance().showSkinOrMatches)
			convertAndDisplayImage(skinOrMatchesVector, rank, "Skin or render matches");
	}
}

void PSODebugger::downloadBuffers() {
	if (PSODebuggerOptions::instance().showDepthDifference)
		thrust::copy(
			System::instance().pso->depthDifferenceOutputs.begin(),
			System::instance().pso->depthDifferenceOutputs.end(),
			depthDifferenceVector.begin()
		);

	if (PSODebuggerOptions::instance().showRenderMatches)
		thrust::copy(
			System::instance().pso->renderMatchesOutputs.begin(),
			System::instance().pso->renderMatchesOutputs.end(),
			renderMatchesVector.begin()
		);

	if (PSODebuggerOptions::instance().showSkinAndMatches)
		thrust::copy(
			System::instance().pso->skinAndMatchesOutputs.begin(),
			System::instance().pso->skinAndMatchesOutputs.end(),
			skinAndMatchesVector.begin()
		);

	if (PSODebuggerOptions::instance().showSkinOrMatches)
		thrust::copy(
			System::instance().pso->skinOrMatchesOutputs.begin(),
			System::instance().pso-> skinOrMatchesOutputs.end(),
			skinOrMatchesVector.begin()
		);
}

void PSODebugger::run() {
	if (PSODebuggerOptions::instance().active) {
		downloadBuffers();
		CUDA_DEBUG( cudaDeviceSynchronize() );
		displayImages();
	}
}

int PSODebugger::cols() const {
	return ceil(PSODebuggerOptions::instance().shownParticles / float(rows()));
}

int PSODebugger::rows() const {
	return ceil(sqrt(PSODebuggerOptions::instance().shownParticles));
}

cv::Size PSODebugger::displaySize() const {
	return cv::Size(
		cols() * cellSize().width ,
		rows() * cellSize().height
	);
}

cv::Size PSODebugger::cellSize() const {
	return cv::Size(
		KinectSpecs::width  / max(rows(), cols()),
		KinectSpecs::height / max(rows(), cols())
	);
}

} /* namespace msc */
} /* namespace lcg */
