#include <msc/optimizer/Optimizer.h>

#include <msc/Clock.h>
#include <msc/PSOOptions.h>
#include <msc/optimizer/ObjectiveFunction.h>

namespace lcg {
namespace msc {

Optimizer::Optimizer() :
		    indexByRankOutputs(PSOOptions::instance().particles),
		depthDifferenceOutputs(PSOOptions::instance().particles),
		  renderMatchesOutputs(PSOOptions::instance().particles),
		  skinOrMatchesOutputs(PSOOptions::instance().particles),
		 skinAndMatchesOutputs(PSOOptions::instance().particles),
		         scoresOutputs(PSOOptions::instance().particles),
		      particlesOutputs(PSOOptions::instance().particles),
		     velocitiesOutputs(PSOOptions::instance().particles),
		      bestCasesOutputs(PSOOptions::instance().particles),
		      firstRun(true) {

	ObjectiveFunction::initialize();
}

Optimizer::~Optimizer() {
}

float Optimizer::constrictionFactor() const {
	float psi = PSOOptions::instance().cognitiveComponent + PSOOptions::instance().socialComponent;
	return 2 / abs(2 - psi - sqrt(psi * psi - 4 * psi));
}

void Optimizer::initializeSwarm() {
	if (firstRun) {
		thrust::host_vector<HandPose> initialParticles(PSOOptions::instance().particles);

		//TODO: Also check for available files in directory
		for (int i = 0; i < initialParticles.size(); i++) {
			std::stringstream path;
			path << PSOOptions::instance().initialPosesDir << "/" << std::setfill('0') << std::setw(2) << i << ".pose";
			std::ifstream file(path.str());
			file >> initialParticles[i];
			file.close();
		}

		particlesOutputs = initialParticles;
		firstRun = false;
	} else {
		for (int i = 1; i < particlesOutputs.size(); i++) {

			int p = indexByRankOutputs[i];
			particlesOutputs[p] = particlesOutputs[indexByRankOutputs[0]];

			//TODO: This process is probably taking too long. It is possible to store model matrices in uniform buffers and map them to cuda, which would allow us to make all calculation in GPU
			//TODO: Should insert some constraints here
			//TODO: We should take advantage of finger motion correlation
			for (int j = 0; j < HAND_DOFS; j++) {
				float r = PSOOptions::instance().disturbanceVariance * (2 * random() / float(RAND_MAX) - 1);

				if (HandPose::isAnAbductionAngle(HandDofs(j)))
					particlesOutputs[p](j) += PSOOptions::instance().abductionAmplitude * M_PI_4 * r;
				else if (HandPose::isAFlexionAngle(HandDofs(j)))
					particlesOutputs[p](j) += PSOOptions::instance().flexionAmplitude   * M_PI_2 * r;
				else if (j < 4)
					particlesOutputs[p](j) += PSOOptions::instance().quaternionAmplitude * r;
				else if (j < 7)
					particlesOutputs[p](j) += PSOOptions::instance().xyAmplitude * r;
				else
					particlesOutputs[p](j) += PSOOptions::instance().zAmplitude * r;
			}

			particlesOutputs[p].clamp(
				PSOOptions::instance().lowerPoseBound(),
				PSOOptions::instance().upperPoseBound()
			);
		}
	}

	for (HandPose &v : velocitiesOutputs)
		v.fill(0);

	thrust::fill(
		scoresOutputs.begin(), scoresOutputs.end(),
		std::numeric_limits<float>::infinity()
	);

	Clock::instance().logTime(";opt_init:");

	bestCaseEverOutput  = particlesOutputs[0];
	bestScoreEverOutput = std::numeric_limits<float>::infinity();

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";opt_init_out:");
}

void Optimizer::run() {
	Clock::instance().logTime(";opt_first:");

	if (PSOOptions::instance().active) {
		initializeSwarm();
		evaluateSwarm();

		for (int n = 1; n < PSOOptions::instance().maxGenerations + 1; n++) {
			if (n % PSOOptions::instance().disturbingInterval == 0)
				disturbSwarm();
			advanceSwarm();
			evaluateSwarm();
		}
	}

	Clock::instance().logTime(";opt_end:");
}

} /* namespace msc */
} /* namespace lcg */
