#include <cuccl/kernels.h>
#include <cuccl/label_equivalence.h>

namespace lcg {
namespace cuccl {

void label_equivalence::init() {
	labelTex.addressMode[0] = cudaAddressModeClamp;
	labelTex.addressMode[1] = cudaAddressModeClamp;
	labelTex.filterMode = cudaFilterModePoint;
	labelTex.normalized = false;

	binaryTex.addressMode[0] = cudaAddressModeClamp;
	binaryTex.addressMode[1] = cudaAddressModeClamp;
	binaryTex.filterMode = cudaFilterModePoint;
	binaryTex.normalized = false;
}

label_equivalence::label_equivalence(const dim3 &grid, const dim3 &block) :
		grid(grid), block(block) {
}

cutils::device_ref<full_label_image>
label_equivalence::cuccl(const cutils::device_ref<segment_image> &binary) {

	cudaBindTexture2D(NULL, binaryTex, binary->data,    segment_image::width,    segment_image::height,    segment_image::pitch);
	cudaBindTexture2D(NULL, labelTex,  labels->data, full_label_image::width, full_label_image::height, full_label_image::pitch);

	changed = false;

	prelabel<<<grid, block>>>(labels, changed);
	CUDA_DEBUG(cudaDeviceSynchronize());

	while (changed) {
		changed = false;
		flatten<<<grid, block>>>(labels);
		scan   <<<grid, block>>>(labels, changed);
		CUDA_DEBUG( cudaDeviceSynchronize() );
	}

	return labels;
}

label_equivalence::labels_and_props
label_equivalence::cucclprops(const cutils::device_ref<segment_image> &binary) {

	cudaBindTexture2D(NULL, binaryTex, binary->data,    segment_image::width,    segment_image::height,    segment_image::pitch);
	cudaBindTexture2D(NULL, labelTex,  labels->data, full_label_image::width, full_label_image::height, full_label_image::pitch);

	changed = false;

	prelabel<<<grid, block>>>(labels, props, changed);
	CUDA_DEBUG(cudaDeviceSynchronize());

	while (changed) {
		changed = false;
		flatten<<<grid, block>>>(labels, props);
		scan   <<<grid, block>>>(labels, changed);
		CUDA_DEBUG( cudaDeviceSynchronize() );
	}

	return labels_and_props(labels, props);
}

} /* namespace cuccl */
} /* namespace lcg */
