#include <cuccl/error_ellipse.h>
#include <cutils/functions.h>
#include <cutils/types/cuda_vector.h>

using lcg::cutils::sqr;

namespace lcg {
namespace cuccl {

__host__ __device__
error_ellipse::error_ellipse(const ulonglong3 &variances, const ulonglong2 centroid, const float area) {
	covmatrix[0][0] = variances.x / area - sqr(centroid.x / area);
	covmatrix[0][1] = variances.z / area - centroid.x / area * centroid.y / area;
	covmatrix[1][0] = variances.z / area - centroid.x / area * centroid.y / area;
	covmatrix[1][1] = variances.y / area - sqr(centroid.y / area);

	const float &a = covmatrix[0][0], &b = covmatrix[0][1], &c = covmatrix[1][0], &d = covmatrix[1][1];
	const float trace = a + d, determ = a * d - b * c;

	eigenval[0] = trace / 2 + sqrt(sqr(trace) / 4 - determ);
	eigenval[1] = trace / 2 - sqrt(sqr(trace) / 4 - determ);

	if (b) {
		eigenvec[0] = make_float2(b, eigenval[0] - a);
		eigenvec[1] = make_float2(b, eigenval[1] - a);

		eigenvec[0] = eigenvec[0] / sqrt(sqr(eigenvec[0].x) + sqr(eigenvec[0].y));
		eigenvec[1] = eigenvec[1] / sqrt(sqr(eigenvec[1].x) + sqr(eigenvec[1].y));
	} else if (c) {
		eigenvec[0] = make_float2(eigenval[0] - a, c);
		eigenvec[1] = make_float2(eigenval[1] - a, c);

		eigenvec[0] = eigenvec[0] / sqrt(sqr(eigenvec[0].x) + sqr(eigenvec[0].y));
		eigenvec[1] = eigenvec[1] / sqrt(sqr(eigenvec[1].x) + sqr(eigenvec[1].y));
	} else {
		eigenvec[0] = make_float2(1, 0);
		eigenvec[1] = make_float2(0, 1);
	}
}

//TODO: Change for the usual error ellipse angle.
__host__ __device__
float error_ellipse::angle() const {
	const float2 &u = eigenvec[0], v = make_float2(1, 0);
	return acos(u * v / sqrt((u * u) * (v * v)));
}

__host__ __device__
float error_ellipse::area() const {
	return 4 * M_PI * sqrt(eigenval[0] * eigenval[1]);
}

} /* namespace cuccl */
} /* namespace lcg */
