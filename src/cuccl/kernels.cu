
#include <cuccl/kernels.h>
#include <cuccl/label_equivalence.h>
#include <cuimg/neigh.h>
#include <cutils/functions.h>
#include <cutils/thread_index.h>
#include <cutils/types/vector.h>

#include <stdio.h>

namespace lcg {
namespace cuccl {

using cutils::length;
using cutils::sqr;
using cutils::thread_index;
using cutils::vec2i;
using cutils::vec3i;
using cuimg::neigh8;

texture<segment_image::element_type, cudaTextureType2D> binaryTex;
texture<   full_label::   base_type, cudaTextureType2D>  labelTex;

__device__
full_label find_root(const full_label_image &table, full_label label) {

	full_label root = table(label.value());

	for (int j = 0; label != root and j < LCG_CUCCL_MAX_INNER_ITERATIONS; j++) {
		label = root;
		root  = table(label.value());
	}

	return label;
}

__global__
void prelabel(full_label_image &labels, properties &props, bool &changed) {
	//TODO: Fix vector types so we can get rid of int2
	const int2 p = thread_index().xy();
	const int  i = thread_index().plain();

	segment_image::element_type segment = tex2D(binaryTex, p.x, p.y);

	if (segment != segment_image::background()) {
		full_label root(segment, i);
		for (auto &q : neigh_scan(p)) {
			segment_image::element_type new_segment = tex2D(binaryTex, q.x, q.y);
			if (full_label_image::inside(q) and new_segment < segment) {
				root = full_label(new_segment, thread_index(q).plain());
				changed = true;
				break;
			}
		}

		labels(i) = root;

		props.area    (i) = 1;
		props.centroid(i) = properties::centroid_type{ p.x, p.y };
		props.variance(i) = properties::variance_type{ sqr(p.x), sqr(p.y), p.x * p.y };
	} else {
		labels(i) = full_label::background();

		props.area    (i) = 0;
		props.centroid(i) = properties::centroid_type{ 0, 0 };
		props.variance(i) = properties::variance_type{ 0, 0, 0 };
	}
}

__global__
void prelabel(full_label_image &labels, bool &changed) {
	const int2 p = thread_index().xy();
	const int  i = thread_index().plain();

	segment_image::element_type segment = tex2D(binaryTex, p.x, p.y);

	if (segment != segment_image::background()) {
		full_label root(segment, i);
		for (auto &q : neigh_scan(p)) {
			segment_image::element_type new_segment = tex2D(binaryTex, q.x, q.y);
			if (full_label_image::inside(q) and new_segment < segment) {
				root = full_label(new_segment, thread_index(q).plain());
				changed = true;
				break;
			}
		}

		labels(i) = root;
	} else {
		labels(i) = full_label::background();
	}
}

__global__
void scan(full_label_image &labels, bool &changed) {
	const int2 p = thread_index().xy();
	const int  i = thread_index().plain();

	const full_label label = tex2D(labelTex, p.x, p.y);

	if (label.is_foreground()) {
		const full_label minimal = smallest(neigh8(), p);

		if (minimal < label) {
			changed = true;
			atomicMin((full_label::base_type*)(labels.data + label.value()), minimal);
		}
	}
}

__global__
void flatten(full_label_image &labels, properties &props) {
	const int2 p = thread_index().xy();
	const int  i = thread_index().plain();

	const full_label old_label = labels(i);

	if (old_label.is_background())
		return;

	if (old_label.value() != i) {
		const full_label new_label = find_root(labels, old_label);

		if (new_label == old_label)
			return;

		labels(i) = new_label;

		const properties::area_type area = props.area(i);
		if (area > 0) {
			const properties::centroid_type centroid = props.centroid(i);
			const properties::variance_type variance = props.variance(i);

			props.area    (i) = 0;
			props.centroid(i) = properties::centroid_type{ 0, 0 };
			props.variance(i) = properties::variance_type{ 0, 0, 0 };

			atomicAdd(&props.area    (new_label.value())  , area      );
			atomicAdd(&props.centroid(new_label.value()).x, centroid.x);
			atomicAdd(&props.centroid(new_label.value()).y, centroid.y);
			atomicAdd(&props.variance(new_label.value()).x, variance.x);
			atomicAdd(&props.variance(new_label.value()).y, variance.y);
			atomicAdd(&props.variance(new_label.value()).z, variance.z);
		}
	}
}

__global__
void flatten(full_label_image &labels) {
	const int2 p = thread_index().xy();
	const int  i = thread_index().plain();

	const full_label old_label = labels(i);

	if (old_label.is_foreground() and old_label.value() != i) {
		const full_label new_label = find_root(labels, old_label);

		if (new_label != old_label)
			labels(i) = new_label;
	}
}

} /* namespace cuccl */
} /* namespace lcg */
