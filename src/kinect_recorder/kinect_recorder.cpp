//TODO Improve this program by employing libfreenect's asynchronous functions, to reduce delay between frames.

#include <libfreenect.h>
#include <libfreenect_sync.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdx/semaphore.h>
#include <stdx/string.h>

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>

typedef char      byte_type;
typedef uint16_t depth_type;
typedef uint8_t  video_type;

typedef byte_type *  byte_ptr_type;
typedef depth_type* depth_ptr_type;
typedef video_type* video_ptr_type;

const stdx::string depth_window_name = "Depth";
const stdx::string video_window_name = "Video";

freenect_resolution resolution = FREENECT_RESOLUTION_MEDIUM;

freenect_depth_format depth_format = FREENECT_DEPTH_REGISTERED;
freenect_video_format video_format = FREENECT_VIDEO_RGB;

stdx::string prefix;
stdx::string sufix = ".png";
stdx::string temp_sufix = ".dump";
bool running;

size_t depth_frames = 0;
size_t video_frames = 0;

size_t buffer_length = 16;

std::fstream depth_file;
std::fstream video_file;

stdx::semaphore depth_write_sem(buffer_length);
stdx::semaphore video_write_sem(buffer_length);

stdx::semaphore depth_read_sem(0);
stdx::semaphore video_read_sem(0);

depth_ptr_type depth_buffer;
video_ptr_type video_buffer;

freenect_frame_mode depth_mode;
freenect_frame_mode video_mode;

depth_ptr_type addressDepth(size_t i);
video_ptr_type addressVideo(size_t i);

stdx::string nameDepthFile(size_t i);
stdx::string nameVideoFile(size_t i);

stdx::string tempDepthFile();
stdx::string tempVideoFile();

void displayDepth(depth_ptr_type depth_data);
void displayVideo(video_ptr_type video_data);

void captureDepthThread();
void captureVideoThread();
void processDepthThread();
void processVideoThread();

void encodeFrames();
void init();
void openFiles(std::ios::openmode mode);
void parseArgs(int argc, char **argv);
void shutdown();
void shutdownInterface();

int main(int argc, char *argv[]) {

	parseArgs(argc, argv);
	init();

	std::thread t1(processDepthThread);
	std::thread t2(processVideoThread);
	std::thread t3(captureDepthThread);
	std::thread t4(captureVideoThread);

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	shutdownInterface();

	//TODO: Multithreaded buffered encoding would be advantageous for really long recording sessions
	encodeFrames();
	shutdown();

	return EXIT_SUCCESS;
}

depth_ptr_type addressDepth(size_t i) {
	return depth_ptr_type(byte_ptr_type(depth_buffer) + i * depth_mode.bytes);
}

video_ptr_type addressVideo(size_t i) {
	return video_ptr_type(byte_ptr_type(video_buffer) + i * video_mode.bytes);
}

stdx::string nameDepthFile(size_t i) {
	return stdx::string::collapse(prefix, "/depth", std::setw(4), std::setfill('0'), i, sufix);
}

stdx::string nameVideoFile(size_t i) {
	return stdx::string::collapse(prefix, "/video", std::setw(4), std::setfill('0'), i, sufix);
}

stdx::string tempDepthFile() {
	return stdx::string::collapse(prefix, "/depth", temp_sufix);
}

stdx::string tempVideoFile() {
	return stdx::string::collapse(prefix, "/video", temp_sufix);
}

void displayDepth(depth_ptr_type depth_data) {
	cv::Mat depth_frame(
		cv::Size(depth_mode.width, depth_mode.height),
		CV_16UC1,
		depth_data
	);

	cv::imshow(depth_window_name, depth_frame * 32);

	if ((cv::waitKey(1000.0 / (1.2 * depth_mode.framerate)) & 255) == 27)
		running = false;
}

void displayVideo(video_ptr_type video_data) {
	cv::Mat video_frame(
		cv::Size(video_mode.width, video_mode.height),
		CV_8UC3,
		video_data
	);

	cv::cvtColor(video_frame, video_frame, cv::COLOR_RGB2BGR);
	cv::imshow(video_window_name, video_frame);

	if ((cv::waitKey(1000.0 / (1.2 * depth_mode.framerate)) & 255) == 27)
		running = false;
}

void captureDepthThread() {
	void *depth_data;
	uint32_t timestamp;

	for (int i = 0; running; i = (i + 1) % buffer_length) {
		depth_write_sem.wait();

		freenect_sync_get_depth(&depth_data, &timestamp, 0, depth_format);
		memcpy(addressDepth(i), depth_data, depth_mode.bytes);
		depth_frames++;

		depth_read_sem.signal();
	}
}

void captureVideoThread() {
	void *video_data;
	uint32_t timestamp;

	for (int i = 0; running; i = (i + 1) % buffer_length) {
		video_write_sem.wait();

		freenect_sync_get_video(&video_data, &timestamp, 0, video_format);
		memcpy(addressVideo(i), video_data, video_mode.bytes);
		video_frames++;

		video_read_sem.signal();
	}
}

void processDepthThread() {

	for (int i = 0; running; i = (i + 1) % buffer_length) {
		depth_read_sem.wait();

		depth_file.write(byte_ptr_type(addressDepth(i)), depth_mode.bytes);
		displayDepth(addressDepth(i));

		depth_write_sem.signal();
	}

	depth_file.close();
}

void processVideoThread() {

	for (int i = 0; running; i = (i + 1) % buffer_length) {
		video_read_sem.wait();

		video_file.write(byte_ptr_type(addressVideo(i % buffer_length)), video_mode.bytes);
		displayVideo(addressVideo(i % buffer_length));

		video_write_sem.signal();
	}

	video_file.close();
}

void encodeFrames() {
	size_t max_frames = std::min(depth_frames, video_frames);

	cv::Mat depth_frame(
		cv::Size(depth_mode.width, depth_mode.height),
		CV_16UC1
	);

	cv::Mat video_frame(
		cv::Size(video_mode.width, video_mode.height),
		CV_8UC3
	);

	openFiles(std::ios::in);

	printf("Converting %d frames to %s format\n", max_frames, sufix.c_str());

	for (int i = 0; i < max_frames and not depth_file.eof(); i++) {
		depth_file.read(byte_ptr_type(depth_frame.data), depth_mode.bytes);
		cv::imwrite(nameDepthFile(i), depth_frame);
		printf("\t%5d / %5d depth frames (%3.0f%%)\r", (i + 1), max_frames, 100.0 * (i + 1) / max_frames);
	}

	puts("");

	for (int i = 0; i < max_frames and not video_file.eof(); i++) {
		video_file.read(byte_ptr_type(video_frame.data), video_mode.bytes);
		cv::cvtColor(video_frame, video_frame, cv::COLOR_RGB2BGR);
		cv::imwrite(nameVideoFile(i), video_frame);
		printf("\t%5d / %5d video frames (%3.0f%%)\r", (i + 1), max_frames, 100.0 * (i + 1) / max_frames);
	}

	puts("\nDone");

	depth_file.close();
	video_file.close();

	remove(tempDepthFile().c_str());
	remove(tempVideoFile().c_str());
}

void init() {
	cv::namedWindow(depth_window_name, CV_WINDOW_AUTOSIZE);
	cv::namedWindow(video_window_name, CV_WINDOW_AUTOSIZE);

	depth_mode = freenect_find_depth_mode(resolution, depth_format);
	video_mode = freenect_find_video_mode(resolution, video_format);

	depth_buffer = new depth_type[buffer_length * depth_mode.bytes];
	video_buffer = new video_type[buffer_length * video_mode.bytes];

	openFiles(std::ios::out);

	running = true;
}

void openFiles(std::ios::openmode mode) {
	depth_file.open(
		tempDepthFile(),
		std::ios::binary | mode
	);
	video_file.open(
		tempVideoFile(),
		std::ios::binary | mode
	);
}

void parseArgs(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " <output_prefix>" << std::endl;
		exit(EXIT_FAILURE);
	}

	prefix = argv[1];
}

void shutdown() {
	delete []depth_buffer;
	delete []video_buffer;
}

void shutdownInterface() {
	cv::destroyAllWindows();
	freenect_sync_stop();
}
