#include <kinect_viewer/CameraDisplay.h>
#include <msc/OptionsParser.h>
#include <msc/System.h>
#include <msc_view/Recorder.h>
#include <msc_view/Visualizer.h>

#include <sys/ioctl.h>

using namespace lcg;
using namespace msc;
using namespace boost::filesystem;

winsize terminal_size;
char *status_line_format;

void init_terminal();

int main(int argc, char *argv[]);

void welcome();

void init_terminal() {
	//TODO: Terminal module is not portable
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &terminal_size);
	terminal_size.ws_col = max(80, terminal_size.ws_col);

	status_line_format = new char[7];
	sprintf(status_line_format, "%%-%ds", terminal_size.ws_col - 6);
}

int main(int argc, char *argv[]) {
	OptionsParser::instance().configFiles << "viewer.ini";
	system().init(argc, argv);

	init_terminal();
	welcome();

	//TODO: Once Observer architecture is implemented, manual registration won't be required and module dependencies will become implicit.
	system().addModule(&Visualizer::instance());
	system().addModule(&Recorder::instance());
	system().addModule(&CameraDisplay::instance());

	system().run();

	exit(EXIT_SUCCESS);
}

void welcome() {
	stdx::string title_base = " Hand tracking test application ";
	stdx::string full_title =
			stdx::string("=") * ((terminal_size.ws_col - title_base.size()) / 2) +
			title_base +
			stdx::string("=") * ( terminal_size.ws_col - title_base.size() - (terminal_size.ws_col - title_base.size()) / 2);

	puts(full_title.c_str());
	printf("Built on %s, at %s\n\n", __DATE__, __TIME__);
}
