#include <cuimg/image.h>
#include <cutils/functions.h>

#include <iostream>

namespace lcg {
namespace cuimg {

__host__ __device__
rectangle::rectangle(size_t width, size_t height, int x, int y) :
	width(width), height(height), x(x), y(y) {
}

__host__ __device__
rectangle::rectangle(size_t width, size_t height, const int2 &center) :
	width(width), height(height), x(center.x), y(center.y) {
}

__host__ __device__
int rectangle::area() const {
	return width * height;
}

__host__ __device__
ulonglong2 rectangle::centroid() const {
	if (area())
		return make_ulonglong2(
				height * cutils::sum(width , x) / area(),
				width  * cutils::sum(height, y) / area());
	else
		return make_ulonglong2(0, 0);
}

__host__ __device__
ulonglong2 rectangle::variance() const {
	if (area())
		return make_ulonglong2(
				height * cutils::sum_sqr(width , x) / area(),
				width  * cutils::sum_sqr(height, y) / area());
	else
		return make_ulonglong2(0, 0);
}

__host__ __device__
float rectangle::covariance() const {
	return 0;
}

__host__ __device__
bool rectangle::contains(const int2 &point) const {
	return
		0 <= point.x and point.x < width and
		0 <= point.y and point.y < height;
}

std::ostream& operator<<(std::ostream &os, const rectangle &rect) {
	return os << "rectangle " << rect.width << " X " << rect.height << " at (" << rect.x << ", " << rect.y << ")";
}

} /* namespace cuimg */
} /* namespace lcg */
