#include <cutils/cuda_exception.h>

#include <sstream>

namespace lcg {
namespace cutils {

cuda_exception::cuda_exception(std::string file, int line, cudaError_t status) :
		_file(file), _line(line), _status(status), _call("") {
	std::stringstream temp;
	temp << _file << ":" << _line << ":" << cudaGetErrorString(status) << " ("
			<< status << ')';
	_what = temp.str();
}

cuda_exception::cuda_exception(std::string call, cudaError_t status) :
		_file(""), _line(0), _status(status), _call(call) {
	std::stringstream temp;
	temp << _call << ": " << cudaGetErrorString(status) << " (" << status
			<< ')';
	_what = temp.str();
}

const char* cuda_exception::what() const throw () {
	return _what.c_str();
}

std::string cuda_exception::getFile() const {
	return _file;
}

int cuda_exception::getLine() const {
	return _line;
}

std::string cuda_exception::getCall() const {
	return _call;
}

cudaError_t cuda_exception::getStatus() const {
	return _status;
}

cuda_exception::~cuda_exception() {
}


} /* namespace cutils */
} /* namespace lcg */
