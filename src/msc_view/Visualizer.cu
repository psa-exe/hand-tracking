#include <msc_view/Visualizer.h>

#include <cuimg/functions/apply.h>
#include <msc/EnvironmentOptions.h>
#include <msc/KinectSpecs.h>
#include <msc/PSOOptions.h>
#include <msc/System.h>
#include <msc/camera/CameraOptions.h>
#include <msc/camera/PlaybackCamera.h>
#include <msc_view/Visualizer.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdx/string.h>
#include <tucano/framebuffer.hpp>

namespace lcg {
namespace msc {

const cv::Size kinect_size(KinectSpecs::width, KinectSpecs::height);

Visualizer::Visualizer() :

		min_depth(1000 * EnvironmentOptions::instance().nearDepthClip - KinectSpecs::minPossibleDepth),
		max_depth(1000 * EnvironmentOptions::instance(). farDepthClip - KinectSpecs::minPossibleDepth),

		processedDepthImage   (kinect_size, CV_16UC1, processedDepthInput->data),
		processedVideoImage   (kinect_size, CV_8UC3 , processedVideoInput->data),
		processedDepthImageRgb(kinect_size, CV_8UC3),
		observedSkinImage     (kinect_size, CV_8UC1 , observedSkinInput->data),
		blobLabelsImage       (kinect_size, CV_8UC4 , blobLabelsInput->data),
		bestHandImage         (kinect_size, CV_8UC3),
		montageImageOutput(cv::Size(2 * KinectSpecs::width, KinectSpecs::height), CV_8UC3),

		fbo(Tucano::Framebuffer(KinectSpecs::width, KinectSpecs::height, 1)),

		renderedDepthInputs(PSOOptions::instance().particles) {
}

void Visualizer::run() {
	updateBuffers();
	CUDA_DEBUG( cudaDeviceSynchronize(); )

	cv::Mat  leftSide(montageImageOutput(cv::Rect(0, 0, KinectSpecs::width, KinectSpecs::height)));
	cv::Mat rightSide(montageImageOutput(cv::Rect(KinectSpecs::width, 0, KinectSpecs::width, KinectSpecs::height)));

	processedDepthImage.convertTo(processedDepthImageRgb, CV_8U, 255.0 / (1000 * KinectSpecs::instance().far));
	cv::cvtColor(processedDepthImageRgb, processedDepthImageRgb, cv::COLOR_GRAY2BGR);
	processedDepthImageRgb.copyTo(rightSide);

	cv::cvtColor(processedVideoImage, processedVideoImage, cv::COLOR_RGB2BGR);

	cv::cvtColor(blobLabelsImage, blobLabelsImageRgb, cv::COLOR_RGBA2RGB);
	blobLabelsImageRgb.copyTo(rightSide, ~observedSkinImage);

	cv::Mat bestHandMask;
	cv::cvtColor(bestHandImage, bestHandMask, cv::COLOR_BGR2GRAY);
	cv::threshold(bestHandMask, bestHandMask, 254, 255, cv::THRESH_BINARY);
	cv::addWeighted(processedVideoImage, 0.5, bestHandImage, 0.5, 0, leftSide);
	processedVideoImage.copyTo(leftSide, bestHandMask);

	cv::putText(leftSide,
		stdx::string::collapse("Best fit: ", system().pso->bestScoreEverOutput),
		cv::Point(10, 0.95 * KinectSpecs::height),
		cv::FONT_HERSHEY_SIMPLEX,
		1.0,
		cv::Scalar(0, 0, 255),
		1,
		true
	);
}

void Visualizer::updateBuffers() {
	processedDepthInput = system().preprocessor->processedDepthOutput;
	processedVideoInput = system().preprocessor->processedVideoOutput;
	observedSkinInput   = system().skinDetector->observedSkinOutput;
	blobLabelsInput     = system().blobTracker ->blobLabelsOutput;

	system().renderTarget->forceContext();
	fbo.bind();
	system().renderer->renderStuff({
		//TODO: Was drawing the best case of every generation, now the best case ever
//		system().pso->particlesOutputs[system().pso->indexByRankOutputs[0]]
		system().pso->bestCaseEverOutput
	});
	std::vector<unsigned char> pixels;
	fbo.readBuffer(0, pixels);
	cv::Mat bestHandRender(cv::Size(KinectSpecs::width, KinectSpecs::height), CV_8UC4, &pixels[0]);
	cv::cvtColor(bestHandRender, bestHandImage, cv::COLOR_RGBA2BGR);
	fbo.unbind();

	//TODO: Doesn't need to copy all of them
	thrust::copy(
		System::instance().renderer->renderedDepthOutputs.begin(),
		System::instance().renderer->renderedDepthOutputs.end(),
		renderedDepthInputs.begin()
	);
}

} /* namespace msc */
} /* namespace lcg */
