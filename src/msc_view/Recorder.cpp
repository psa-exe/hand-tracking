#include <msc_view/Recorder.h>

#include <msc_view/RecorderOptions.h>
#include <msc_view/Visualizer.h>
#include <opencv2/highgui/highgui.hpp>
#include <stdx/string.h>

#include <iomanip>

namespace lcg {
namespace msc {

Recorder::Recorder() :
		frameCount(0) {
}

void Recorder::run() {
	if (RecorderOptions::instance().active) {
		stdx::string filename = stdx::string::collapse(
			"output/montage",
			std::setw(4), std::setfill('0'), frameCount,
			".png"
		);

		cv::imwrite(filename, Visualizer::instance().montageImageOutput);

		frameCount++;
	}
}

} /* namespace lcg::msc */
} /* namespace lcg */
