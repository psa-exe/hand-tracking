#include <stdx/io_error.h>

namespace stdx {

io_error io_error::open_error(const stdx::string &file_path) {
	return io_error(stdx::string::collapse("Could not open file: ", file_path));
}

io_error::io_error(const stdx::string &message) : message(message) {
}

const char* io_error::what() const noexcept {
	return message;
}

} /* namespace stdx */
