#include <stdx/mutex.h>

namespace stdx {

void mutex::critical_section(std::function<void(void)> operations) {
	lock();
	operations();
	unlock();
}

} /* namespace stdx */
