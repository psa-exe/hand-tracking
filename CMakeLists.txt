
project(HandTracking)

cmake_minimum_required(VERSION 3.1)

# Set CMake module path in order to include our custom Find<...>.cmake modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

# The project version number.
#set(VERSION_MAJOR   0   CACHE STRING "Project major version number.")
#set(VERSION_MINOR   0   CACHE STRING "Project minor version number.")
#set(VERSION_PATCH   1   CACHE STRING "Project patch version number.")
#mark_as_advanced(VERSION_MAJOR VERSION_MINOR VERSION_PATCH)

#configure_file(
#    include/hand-tracking/version.h.in 
#    include/hand-tracking/version.h
#)

# TODO: If not including the system component, linking against the filesystem component yelds an undefined reference. File a bug in the FindBoost package upstream
find_package(Boost 1.50 REQUIRED COMPONENTS filesystem program_options regex system)
find_package(CUDA 7.0 REQUIRED)
find_package(Freenect 0.2 REQUIRED)
find_package(GLEW REQUIRED) 
find_package(OpenGL REQUIRED)
find_package(OpenCV 2.3 REQUIRED COMPONENTS core highgui imgproc)
find_package(PkgConfig REQUIRED)
    
pkg_check_modules(EIGEN3 REQUIRED eigen3)
pkg_search_module(GLFW REQUIRED glfw3)

option(BUILD_PROFILE "Build profiling programs" ON)

set(SOURCE_DIR src)
set(PROFILE_DIR profile)
# TODO: Create CMake module for tucano in order to import it
#set(TUCANO_EFFECTS_DIR "${PROJECT_SOURCE_DIR}/libs/tucano/tucano/effects")
set(TUCANO_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/libs/tucano/")
set(TUCANO_SHADERS_DIR "${TUCANO_INCLUDE_DIR}/effects/shaders")  
set(OSTYPE "$ENV{OSTYPE}")

#TODO: these flags seem not to be working
#set(CXX_STANDARD_REQUIRED ON)
#set(CXX_STANDARD          11)

# These two flags below are a workaround on a FindCUDA.cmake bug in function 
# _cuda_get_important_host_flags that produces an error when either of these 
# variables is empty and passed as argument to this function.
#list(APPEND CMAKE_C_FLAGS   -Wall)
list(APPEND CMAKE_CXX_FLAGS "-w --std=c++11 -fPIC")

set(CUDA_SEPARABLE_COMPILATION ON)
list(APPEND CUDA_NVCC_FLAGS -w;--std=c++11;-gencode=arch=compute_30,code=sm_30)

include_directories(
    include
    ${BOOST_INCLUDE_DIRS}
    ${EIGEN3_INCLUDE_DIRS}
    ${GLEW_INCLUDE_DIRS}
    ${FREENECT_INCLUDE_DIR}
    ${OPENGL_INCLUDE_DIR}
    ${TUCANO_INCLUDE_DIR}
)

#===============================================================================
# stdx - C++ standard library enhancements
#===============================================================================
set(stdx_DIR "${SOURCE_DIR}/stdx")
file(GLOB_RECURSE stdx_SOURCES
    "${stdx_DIR}/*.cpp"
    "${stdx_DIR}/*.c"
)

add_library(stdx SHARED
    ${stdx_SOURCES}
)

#===============================================================================
# cutils - CUDA utilities
#===============================================================================
set(cutils_DIR "${SOURCE_DIR}/cutils")
file(GLOB_RECURSE cutils_SOURCES
    "${cutils_DIR}/*.cpp"
    "${cutils_DIR}/*.cu"
    "${cutils_DIR}/*.c"
)

cuda_add_library(cutils SHARED
    ${cutils_SOURCES}
)

target_link_libraries(cutils
    ${CUDA_LIBRARIES}
)

#===============================================================================
# cuimg - CUDA image processing
#===============================================================================
add_definitions(-DCUIMG_OPENCV_SUPPORT)

set(cuimg_DIR "${SOURCE_DIR}/cuimg")
file(GLOB_RECURSE cuimg_SOURCES
    "${cuimg_DIR}/*.cpp"
    "${cuimg_DIR}/*.cu"
    "${cuimg_DIR}/*.c"
)

cuda_add_library(cuimg SHARED
    ${cuimg_SOURCES}
)

target_link_libraries(cuimg
    cutils
    ${CUDA_LIBRARIES}
)

#===============================================================================
# cuccl - Connected Component Labeling
#===============================================================================
set(cuccl_DIR "${SOURCE_DIR}/cuccl")
file(GLOB_RECURSE cuccl_SOURCES
    "${cuccl_DIR}/*.cpp"
    "${cuccl_DIR}/*.cu"
    "${cuccl_DIR}/*.c"
)

cuda_add_library(cuccl SHARED
    ${cuccl_SOURCES}
)

target_link_libraries(cuccl
    cuimg
    cutils
	${CUDA_LIBRARIES}
)

#===============================================================================
# freenect_extra - libfreenect ammendments
#===============================================================================
set(freenect_extra_DIR "${SOURCE_DIR}/freenect_extra")
file(GLOB_RECURSE freenect_extra_SOURCES
    "${freenect_extra_DIR}/*.cpp"
    "${freenect_extra_DIR}/*.c"
)

add_library(freenect_extra SHARED
    ${freenect_extra_SOURCES}
)

target_link_libraries(freenect_extra
    ${FREENECT_LIBRARIES}
)

#===============================================================================
# kinect_recorder - Utility to record Kinect frames as png images 
#===============================================================================
set(kinect_recorder_DIR "${SOURCE_DIR}/kinect_recorder")
file(GLOB_RECURSE kinect_recorder_SOURCES
    "${kinect_recorder_DIR}/*.cpp"
    "${kinect_recorder_DIR}/*.c"
)

add_executable(kinect_recorder
	${kinect_recorder_SOURCES}
)

target_link_libraries(kinect_recorder
    stdx
    pthread
	${FREENECT_sync_LIBRARY}
	${FREENECT_LIBRARIES}
	${OpenCV_LIBRARIES}
)

#===============================================================================
# msc - Main library implementing Oikonomidis' method and variations  
#===============================================================================
set(msc_DIR "${SOURCE_DIR}/msc")
file(GLOB_RECURSE msc_SOURCES
    "${msc_DIR}/*.cpp"
    "${msc_DIR}/*.cu"
    "${msc_DIR}/*.c"
)

cuda_add_library(msc SHARED
	${msc_SOURCES}
)

set_target_properties(msc PROPERTIES COMPILE_FLAGS
    "-DTUCANOSHADERDIR=${TUCANO_SHADERS_DIR} -Wall"
)

target_link_libraries(msc
    cuccl
    pthread
    stdx
    ${Boost_LIBRARIES}
	${FREENECT_sync_LIBRARY}
	${OpenCV_LIBRARIES}
	${GLFW_LIBRARIES}
	${GLEW_LIBRARIES}
    ${OPENGL_LIBRARIES}
)

#===============================================================================
# msc_view - Auxiliary library with viewing facilities  
#===============================================================================
set(msc_view_DIR "${SOURCE_DIR}/msc_view")
file(GLOB_RECURSE msc_view_SOURCES
    "${msc_view_DIR}/*.cpp"
    "${msc_view_DIR}/*.cu"
    "${msc_view_DIR}/*.c"
)

cuda_add_library(msc_view SHARED
	${msc_view_SOURCES}
)

set_target_properties(msc_view PROPERTIES COMPILE_FLAGS
    "-DTUCANOSHADERDIR=${TUCANO_SHADERS_DIR} -Wall"
)

target_link_libraries(msc_view
    msc
)

#===============================================================================
# msc_eval - Auxiliary application to asess performance  
#===============================================================================
set(msc_eval_DIR "${SOURCE_DIR}/msc_eval")
file(GLOB_RECURSE msc_eval_SOURCES
    "${msc_eval_DIR}/*.cpp"
    "${msc_eval_DIR}/*.cu"
    "${msc_eval_DIR}/*.c"
)

cuda_add_executable(msc_eval
	${msc_eval_SOURCES}
)

set_target_properties(msc_eval PROPERTIES COMPILE_FLAGS
    "-DTUCANOSHADERDIR=${TUCANO_SHADERS_DIR} -Wall"
)

target_link_libraries(msc_eval
    msc_view
)

#===============================================================================
# kinect_viewer - Hand segmentation and cuccl development application 
#===============================================================================
set(kinect_viewer_DIR "${SOURCE_DIR}/kinect_viewer")
file(GLOB_RECURSE kinect_viewer_SOURCES
    "${kinect_viewer_DIR}/*.cpp"
    "${kinect_viewer_DIR}/*.cu"
    "${kinect_viewer_DIR}/*.c"
)

cuda_add_executable(kinect_viewer
	${kinect_viewer_SOURCES}
)

set_property(TARGET kinect_viewer PROPERTY COMPILE_FLAGS
    "-DTUCANOSHADERDIR=${TUCANO_SHADERS_DIR} -Wall"
)

target_link_libraries(kinect_viewer
    msc_view
)
